                <table id="restrict_class_list">
                  <thead>
                      <tr>
                          <th>Sl. no.</th>
                          <th>Category</th>
                          <th>Vendor Name</th>
                          <th>Class Name</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                        include_once('elements/db_connection.php');
                        $cat = $_REQUEST['cat'];
                        if($cat == '') {
                          $sql = "SELECT user_detail.company_name, user_detail.category, restrict_class.id, restrict_class.name FROM restrict_class LEFT JOIN user_detail ON user_detail.user_id = restrict_class.vendor_id WHERE restrict_class.isDeleted = 0 ORDER BY `id` DESC";
                        } else if(isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
                          $user_id = $_REQUEST['id'];
                          $sql = "SELECT user_detail.company_name, user_detail.category, restrict_class.id, restrict_class.name FROM restrict_class LEFT JOIN user_detail ON user_detail.user_id = restrict_class.vendor_id WHERE restrict_class.isDeleted = 0 AND user_detail.category='$cat' AND user_detail.user_id = '$user_id' ORDER BY `id` DESC";
                        } else {
                          $sql = "SELECT user_detail.company_name, user_detail.category, restrict_class.id, restrict_class.name FROM restrict_class LEFT JOIN user_detail ON user_detail.user_id = restrict_class.vendor_id WHERE restrict_class.isDeleted = 0 AND user_detail.category='$cat' ORDER BY `id` DESC";
                        }
                        $result = mysqli_query($conn, $sql);
                        $slNo = 1;
                        if (mysqli_num_rows($result) > 0) {
                            // output data of each row
                            while($row = mysqli_fetch_assoc($result)) {
                        ?>
                              <tr>
                                <td><?php echo $slNo;?></td>
                                <td><?php echo $row['category'];?></td>
                                <td><?php echo $row['company_name'];?></td>
                                <td><?php echo $row['name'];?></td>
                                <td>
                                <a class='btn btn-danger' onclick="delete_class(<?php echo $row['id'];?>)" style="cursor:pointer;">Delete</a>
                                <a class='btn btn-primary' onclick="edit_class(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
                                <a class='btn btn-primary' onclick="show_detail(<?php echo $row['id'];?>)" style="cursor:pointer;">View</a>
                                </td>
                              </tr>
                        <?php
                              $slNo++;
                            }
                        } else {
                          $error_message = 'Wrong username or password';
                        }
                      ?>
                  </tbody>
                </table>