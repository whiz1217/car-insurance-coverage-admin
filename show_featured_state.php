<?php
include_once('elements/db_connection.php');
$vendor_id = $_REQUEST['vendor_id'];
?>
<?php 
$arSelectedState = array();
$sql = "SELECT * FROM vendor_selected_state WHERE vendor_id=$vendor_id AND is_featured = 'yes'";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
	$states = $row['states'];
	$arSelectedState = explode(',', $states);
}
$sql = "SELECT * FROM states WHERE country_id=231 ORDER BY name ASC";
$result = mysqli_query($conn, $sql);
?>
<select name="state[]" class="state_list_select" multiple="multiple" required>
<?php 
	while($row = mysqli_fetch_assoc($result)) {
		if(in_array($row['name'], $arSelectedState)) {
	?>
			<option value="<?php echo $row['name'];?>" selected><?php echo $row['name'];?></option>
	<?php       
		} else {
	?>
			<option value="<?php echo $row['name'];?>"><?php echo $row['name'];?></option>
	<?php       
		}
	}
	?>
</select>