<?php 
	include_once('elements/db_connection.php');
	include_once('elements/left_menu.php');
	include_once('elements/header.php');
?>
        <!-- /top navigation -->
<style>
#showStaffDetails {    
    margin-right: 15px;
    background-color: #fff;
    bottom: 0;
    clear: both;
    display: block;
    padding: 5px 0 0;
    position: fixed;
    right: 0;
    width: 80%;
    text-align: center;
    z-index: 10;
}
#closeStafeDetails {
    cursor: pointer;
    float: right;
    margin-right: -10px;
    margin-top: -20px;
    border: 1px solid #9e9e9e;
    padding: 0px 8px;
    font-weight: bold;
    font-size: 18px;
    border-radius: 20px;
    background-color: #b16565;
    color: #fff;
}
#closeStafeDetails:hover {
    box-shadow: 3px 4px 20px black;
    background-color: #d43f3f;
}
}
</style>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_title">
                <h2>Dealer List</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
			    <div class="item form-group pull-right">
                    <select onchange="show_dealer_by_approveDisapprove(event)" class="form-control" style="width: 177px;">
                        <option value="">All Dealers</option>
                        <option value="yes">Approve Dealers</option>
                        <option value="no">Disapprove Dealers</option>
                    </select>
                </div> 
                <div class="clearfix"></div>
				<div class="dealerClass">	
                <table id="restrict_class_list">
                  <thead>
                      <tr>
                          <th>Sl. no.</th>
                          <th>Name</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody class="vendor_list_body">
                      <?php 
                        $sql = "SELECT users.id, user_business_details.businessName, user_business_details.isApproved FROM users LEFT JOIN user_business_details ON users.id = user_business_details.userId WHERE users.user_role_id=3 AND users.isDeleted=0 ORDER BY users.id DESC";
                        $result = mysqli_query($conn, $sql);
                        $slNo = 1;
                        if (mysqli_num_rows($result) > 0) {
                            // output data of each row
                            while($row = mysqli_fetch_assoc($result)) {
                        ?>

                              <tr>
                                <td><?php echo $slNo;?></td>
                                <td><?php echo $row['businessName'];?></td>
                                <td>
                                <a class='btn btn-danger' onclick="delete_dealer(<?php echo $row['id'];?>)" style="cursor:pointer;">Delete</a>
                                <a class='btn btn-primary' onclick="edit_detail(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
                                  <a class='btn btn-primary' onclick="show_detail_dealer(<?php echo $row['id'];?>)" style="cursor:pointer;">View</a>
                                  <a class='btn btn-primary' onclick="approve_disapprove_dealer(<?php echo $row['id'];?>, '<?php echo $row['isApproved'];?>')" style="cursor:pointer;">
                                  <?php
                                  if($row['isApproved'] == 'yes') {
                                      echo ' Click to disapprove';
                                  } else {
                                    echo ' Click to approve';
                                  }
                                  ?>
                                  </a>
                                  <a class='btn btn-success' onclick="show_staff_incentive(<?php echo $row['id'];?>)" style="cursor:pointer;">Staff Incentive</a>
                                </td>
                              </tr>
                        <?php
                        $slNo++;
                            }
                        } else {
                          $error_message = 'Wrong username or password';
                        }
                      ?>
                  </tbody>
              </table>
			  </div>
              </div>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content vendor-modal">
                Loading...
            </div>
        </div>
        <div id="showStaffDetails" style="display:none">
            <div id="closeStafeDetails" onclick="closeDiv('showStaffDetails', 'result-staff')">x</div>
            <div id="result-staff" style="box-shadow: 0 0 20px black;border-radius: 5px;">
                <span>Loding...</span>
            </div>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
        
<script>
var table;
$(document).ready(function(){
    table = $('#restrict_class_list').DataTable();
});
function show_detail_dealer(user_id) {
    $.ajax({
        method: "POST",
        url: "dealer-detail.php",
        data: { user_id : user_id }
    }).done(function(data) {
        $('.vendor-modal').html(data);
    });
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}
function fnSaveIncentive (id) {
    var incentiveData = $('#Incentive_'+id).val();
    $.ajax({
        method: "POST",
        url: "dealer_staff_list.php",
        data: { id : id, action:'saveIncentive',incentive: incentiveData }
    }).done(function(data) {
        var message = $('#showMessage');
        message.text(data);
        setTimeout(function() {
            message.text('');
        }, 1000);
    });
}
function closeDiv(id, addLoding) {
    var getDiv = document.getElementById(id);
    if(getDiv) getDiv.style.display = "none";
    var load = '<span>Loding...</span>';
    var lodingDiv = $('#'+addLoding);
    if(lodingDiv) lodingDiv.html(load);
}
function show_staff_incentive(dealer_id){
    var modal = document.getElementById('showStaffDetails');
    modal.style.display = "block";
    $.ajax({
        method: "GET",
        url: "dealer_staff_list.php",
        data: { dealer_id : dealer_id }
    }).done(function(data) {
        $('#result-staff').html(data);
    });
}
function delete_dealer(id) {
    $.ajax({
        method: "POST",
        url: "dealer-delete.php",
        data: { id : id }
    }).done(function(data) {
        $('.vendor-modal').html(data);
    });
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}
function edit_detail(user_id) {
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
    $.ajax({
        method: "POST",
        url: "dealer-edit.php",
        data: { user_id : user_id }
    }).done(function(data) {
        $('.vendor-modal').html(data);
    });
}
		
function approve_disapprove_dealer(user_id, is_approve) {
    $.ajax({
        method: "POST",
        url: "dealer-approve.php",
        data: { user_id : user_id, is_approve : is_approve }
    }).done(function(data) {
        window.location.reload();
    });
}
		
function show_dealer_by_approveDisapprove(e) {
    var val = $(e.target).val();
    $.ajax({
        method: "POST",
        url: "dealer_list_body_of_ApproveDisapprove.php",
        data: { cat : val }
    }).done(function(data) {
      //  console.log(data);
        table.clear();
        table.destroy();
        $('.dealerClass').html(data);
        table = $("#restrict_class_list").DataTable();
    });
}
function close_popup() {
    $('#myModal').html('<div class="modal-content vendor-modal" >Loading...</div>');
    var modal = document.getElementById('myModal');
    modal.style.display = "none";
}
function get_city(e) {
	var zip = $(e.target).val();
	$.ajax({
		method: "POST",
		url: "get_city_state_by_zip.php",
		data: { zip: zip }
	}).done(function(data) {
		if(data != '') {
			var obj = JSON.parse(data);
			$('.city').val(obj.CITY);
			$('.state').val(obj.STATE);
		}
	});
}
function get_city_business(e) {
	var zip = $(e.target).val();
	$.ajax({
		method: "POST",
		url: "get_city_state_by_zip.php",
		data: { zip: zip }
	}).done(function(data) {
		if(data != '') {
			var obj = JSON.parse(data);
			$('.city').val(obj.CITY);
			$('.state').val(obj.STATE);
		}
	});
}
function get_city_personal(e) {
	var zip = $(e.target).val();
	$.ajax({
		method: "POST",
		url: "get_city_state_by_zip.php",
		data: { zip: zip }
	}).done(function(data) {
		if(data != '') {
			var obj = JSON.parse(data);
			$('.city_personal').val(obj.CITY);
			$('.state_personal').val(obj.STATE);
		}
	});
}
function compIsType(t, s) { 
  for(z = 0; z < t.length; ++z) 
	  if(t[z] == s)
		return true;

  return false;
}
function showIncentive(e) {
  var val = $(e.target).prop( "checked" );
  if(val) {
      $('.incentive').prop("disabled", false);
    } else {
      $('.incentive').prop("disabled", true);
    }
}
</script>