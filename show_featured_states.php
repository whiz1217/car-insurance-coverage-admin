<?php
include_once('elements/db_connection.php');
if($_REQUEST['state'] && $_REQUEST['vendor_id']) {
$states = $_REQUEST['state'];
$vendor_id = $_REQUEST['vendor_id'];
?>
<div class="checkbox">
  <label><input onclick="select_all_state()" class="selected_states_all" type="checkbox">Select All</label>
</div>
<?php
$arSelectedState = array();
$sql = "SELECT * FROM vendor_state_mapping WHERE vendor_id=$vendor_id AND is_featured = 'yes'";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
	$arSelectedState[] = $row['states'];
}
if(!empty($states)) {
	foreach($states as $state) {
		if(in_array($state, $arSelectedState)) {
?>
	<div class="checkbox">
	  <label><input onclick="show_dealer()" class="selected_states" type="checkbox" name="states[]" value="<?php echo $state;?>" checked><?php echo $state;?></label>
	</div>
<?php } else { ?>
	<div class="checkbox">
	  <label><input onclick="show_dealer()" class="selected_states" type="checkbox" name="states[]" value="<?php echo $state;?>"><?php echo $state;?></label>
	</div>
<?php } } } } ?>