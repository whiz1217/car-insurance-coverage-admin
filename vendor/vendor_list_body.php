<table id="vendor_list">
  <thead>
      <tr>
          <th>Sl. no.</th>
          <th>Name</th>
          <th>Category</th>
          <th>Action</th>
      </tr>
  </thead>
  <tbody class"vendor_list_body">
<?php 
include_once('../elements/db_connection.php');
$cat = $_REQUEST['cat'];
if($cat == '') {
  $sql = "SELECT users.id, user_detail.company_name, user_detail.category,users.isEnable FROM users LEFT JOIN user_detail ON users.id = user_detail.user_id WHERE users.user_role_id=2 AND users.isDeleted = 0  ORDER BY users.id DESC";
} else {
  $sql = "SELECT users.id, user_detail.company_name, user_detail.category,users.isEnable FROM users LEFT JOIN user_detail ON users.id = user_detail.user_id WHERE users.user_role_id=2 AND users.isDeleted = 0 AND user_detail.category='$cat' ORDER BY users.id DESC";
}
$result = mysqli_query($conn, $sql);
$slNo = 1;
if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
?>
      <tr>
        <td><?php echo $slNo;?></td>
        <td><?php echo $row['company_name'];?></td>
        <td><?php echo $row['category'];?></td>
        <td>
          <a class='btn btn-danger' onclick="delete_vendor(<?php echo $row['id'];?>)" style="cursor:pointer;">Delete</a>
          <a class='btn btn-primary' onclick="edit_detail(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
          <a class='btn btn-primary' onclick="show_detail(<?php echo $row['id'];?>)" style="cursor:pointer;">View</a>
          <a class='btn btn-primary' onclick="approve_disapprove_vendor(<?php echo $row['id'];?>, '<?php echo $row['isEnable'];?>')" style="cursor:pointer;">
          <?php
          if($row['isEnable'] == 'yes') {
              echo ' Click to disapprove';
          } else {
            echo ' Click to approve';
          }
          ?>
          </a>
        </td>
      </tr>
<?php
$slNo++;
    }
} else {
  $error_message = 'Wrong username or password';
}
?>
</tbody>
</table>