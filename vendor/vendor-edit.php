<!-- Modal content -->
<div class="modal-content">
<?php
include_once('../elements/db_connection.php');
$user_id = $_REQUEST['user_id'];
$sql = "SELECT users.id, users.email, user_detail.id as user_detail_id, user_detail.category, user_detail.adminNotes, user_detail.city_name, user_detail.state_name, user_detail.company_name, user_detail.website_url, user_detail.fax_number, user_detail.street_address_1, user_detail.street_address_2, user_detail.country_id, user_detail.state_id, user_detail.city_id, user_detail.zip_code, user_detail.telephone, user_detail.user_document, claim_info.name, claim_info.email as claim_email, claim_info.telephone as claim_telephone, claim_info.id as claim_info_id, claim_info.fax as claim_fax, vendor_cancellation_info.id as cancellation_id, vendor_cancellation_info.name as cancellation_name, vendor_cancellation_info.emailId as cancellation_email, vendor_cancellation_info.telephone as cancellation_telephone, vendor_cancellation_info.fax as cancellation_fax, vendor_other_detail.intro, vendor_other_detail.news, vendor_other_detail.promoCodes, vendor_other_detail.pushToNews, vendor_other_detail.pushToAdvertisngPage, vendor_other_detail.advertisment
FROM users 
LEFT JOIN user_detail ON users.id = user_detail.user_id 
LEFT JOIN claim_info ON users.id = claim_info.user_id 
LEFT JOIN vendor_other_detail ON users.id = vendor_other_detail.vendorId 
LEFT JOIN vendor_cancellation_info ON users.id = vendor_cancellation_info.userId 
WHERE users.id=$user_id ORDER BY users.id DESC";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
?>
<div class="x_panel">
    <div class="x_title">
        <h2>Vendor Info</h2>
        <span class="close" onclick="close_popup()">&times;</span>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <form class="form-horizontal form-label-left" action="vendor/update_vendor.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="user_id" value="<?php echo $row['id']?>">
		<input type="hidden" name="user_detail_id" value="<?php echo $row['user_detail_id']?>">
		<input type="hidden" name="claim_info_id" value="<?php echo $row['claim_info_id']?>">
		<input type="hidden" name="cancellation_id" value="<?php echo $row['cancellation_id']?>">
        <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select required name="category" class="form-control">
                <option value="">Choose Category</option>
                <option value="VSC" <?php if($row['category'] == 'VSC') { ?> selected <?php } ?>>VSC</option>
                <option value="GAP" <?php if($row['category'] == 'GAP') { ?> selected <?php } ?>>GAP</option>
                <option value="ANCILLARY" <?php if($row['category'] == 'ANCILLARY') { ?> selected <?php } ?>>ANCILLARY</option>
                <option value="INSURANCE" <?php if($row['category'] == 'INSURANCE') { ?> selected <?php } ?>>INSURANCE</option>
              </select>
            </div>
		</div>
		<div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input class="form-control col-md-7 col-xs-12" value="<?php echo $row['company_name']?>" name="company_name" placeholder="" type="text">
            </div>
		</div>

		<div class="item form-group col-md-6">
			<label class="control-label col-md-3 col-sm-3 col-xs-12">Website URL</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input type="text" name="website_url"  value="<?php echo $row['website_url']?>" placeholder="http://www.website.com" class="form-control col-md-7 col-xs-12">
			</div>
		</div>

		<div class="item form-group col-md-6">
			<label class="control-label col-md-3 col-sm-3 col-xs-12">Email Address</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input type="text" name="email" style="text-transform: none;" value="<?php echo $row['email']?>" class="form-control col-md-7 col-xs-12">
			</div>
		</div>
          
		<div class="item form-group col-md-6">
			<label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 1</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input type="text" name="street_address_1" value="<?php echo $row['street_address_1']?>" class="form-control col-md-7 col-xs-12">
			</div>
		</div>

		<div class="item form-group col-md-6">
			<label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 2</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<input type="text" name="street_address_2" value="<?php echo $row['street_address_2']?>" class="form-control col-md-7 col-xs-12">
			</div>
		</div>
        
          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Zip/Postal Code</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="zip_code" value="<?php echo $row['zip_code']?>" onkeyup="get_city(event)" class="zip_code form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
            <div class="col-md-6 col-sm-6 col-xs-12 city_vendor_add">
              <input type="text" name="city" placeholder="City" value="<?php echo $row['city_name']?>" class="city form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group col-md-6">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">State/Province</label>
            <div class="col-md-6 col-sm-6 col-xs-12 state_vendor_add">
              <input type="text" name="state" placeholder="State" value="<?php echo $row['state_name']?>" class="state form-control col-md-7 col-xs-12"> 
            </div>
          </div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="telephone" data-format="ddd-ddd-dddd" value="<?php echo $row['telephone']?>" class="form-control col-md-7 col-xs-12 bfh-phone">
            </div>
          </div>   

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="fax_number" value="<?php echo $row['fax_number']?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

		<div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Logo</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
				<input type="file" name="logoDocument" value="" class="form-control col-md-7 col-xs-12 vendorLogo">
				<img id="showLogo" src="<?php echo $row['user_document']?>" alt="Logo" style="width: 225px;"/>
            </div>
		</div>
          
		  <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" data-max-size="2048">Notes</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea name="adminNotes" class="form-control col-md-7 col-xs-12"><?php echo $row['adminNotes']?></textarea>
            <!-- Max upload size 10 MB -->
            </div>
          </div>
          
		  <div class="clearfix"></div>
          <label class="col-md-12" for="name">Claim info</label>
          <div class="ln_solid"></div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="claim_name" value="<?php echo $row['name']?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Address">Email Id</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="claim_email" style="text-transform: none;" value="<?php echo $row['claim_email']?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="claim_telephone" data-format="ddd-ddd-dddd" value="<?php echo $row['claim_telephone']?>" class="form-control col-md-7 col-xs-12 bfh-phone">
            </div>
          </div>
          
		  <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="claim_fax" value="<?php echo $row['claim_fax']?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="clearfix"></div>
            <label class="col-md-12">Cancellation Info</label>
            <div class="ln_solid"></div>

            <div class="item form-group col-md-6">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="cancellation_name" value="<?php echo $row['cancellation_name']?>" class="form-control col-md-7 col-xs-12">
              </div>
            </div>

            <div class="item form-group col-md-6">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Id</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="cancellation_email" style="text-transform: none;" value="<?php echo $row['cancellation_email']?>" class="form-control col-md-7 col-xs-12">
              </div>
            </div>

            <div class="item form-group col-md-6">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="cancellation_telephone" data-format="ddd-ddd-dddd" value="<?php echo $row['cancellation_telephone']?>" class="form-control col-md-7 col-xs-12 bfh-phone">
              </div>
            </div>
            
			<div class="item form-group col-md-6">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="cancellation_fax_number" value="<?php echo $row['cancellation_fax']?>" class="form-control col-md-7 col-xs-12">
              </div>
            </div>
			 
			<div class="clearfix"></div>
			  <label class="col-md-12"></label>
			  <div class="ln_solid"></div>
			 
			 <div class="item form-group col-md-6">
				<label class="control-label col-md-3 col-sm-3 col-xs-12">Intro</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
				  <textarea name="intro" class="form-control col-md-7 col-xs-12" placeholder="Intro"><?php echo $row['intro'];?></textarea>
				</div>
			  </div>
			  
			  <div class="checkbox col-md-12" style="margin-bottom:20px">
				<label class="checkbox-inline"><input type="checkbox" name="pushToNews" <?php if($row['pushToNews'] == 'yes') { echo 'checked'; }?>>Push intro to news</label>
			  </div>
            
			<div class="clearfix"></div>
              <label class="col-md-6">Master Agreement</label>
              <div class="ln_solid"></div>
              
			  <div class="item form-group col-md-6">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Upload Master Agreement</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="file" name="masterAgrementDocument[]" id="masterAgrementDocument" class="form-control col-md-7 col-xs-12" multiple>
				  <a class="form-control btn btn-primary" type="button" value="Preview" onclick="PreviewImage('masterAgrementDocument');">Preview</a>
                  <!-- Max upload size 10 MB -->
                </div>
            </div>
			
			<div class="clearfix"></div>
			
			<div class="item form-group col-md-12" style="display:none;" id="pdf-view">
				<span class="close" onclick="close_pdf_view()">×</span>
				
				<div style="clear:both">
				   <iframe id="viewer" frameborder="0" scrolling="no" style="width: 100%; height:1000px;"></iframe>
				</div>
			</div>
			
		  <div class="clearfix"></div>
          <div class="ln_solid"></div>
          
		  <div class="form-group row">
            <div class="col-md-6 col-md-offset-3">
              <a class="btn btn-danger" onclick="close_popup()">Cancel</a>
              <button id="send" type="submit" class="btn btn-success">Update</button>
            </div>
          </div>
	</form>
  </div>
</div>
<?php
}
?>
</div>
<script>
function PreviewImage(prId) {
	var modal = document.getElementById('pdf-view');
	modal.style.display = "block";
	var modal = document.getElementById('myModal');
	modal.style.display = "block";
	pdffile=document.getElementById(prId).files[0];
	pdffile_url=URL.createObjectURL(pdffile);
	$('#viewer').attr('src',pdffile_url);
}
function close_pdf_view() {
	var modal = document.getElementById('pdf-view');
	modal.style.display = "none";
}
function readURL(input) {

  if (input.files && input.files[0]) {
	var reader = new FileReader();

	reader.onload = function(e) {
	  $('#showLogo').attr('src', e.target.result);
	  $('#showLogo').show();
	}

	reader.readAsDataURL(input.files[0]);
  }
}

$(".vendorLogo").change(function() {
  readURL(this);
});
</script>