<?php
include_once('../elements/db_connection.php');
//echo '<pre>';
//print_r($_REQUEST);

//print_r($_FILES);
//exit;
if(isset($_REQUEST['category']) && !empty($_REQUEST['category'])) {
	$arCategory = $_REQUEST['category'];
	foreach($arCategory as $category) {
		$email = $_REQUEST['email'];
		$user_role_id = 2;
		$created_at = date('Y-m-d H:i:s');
		$sql = "INSERT INTO users (email, user_role_id, created_at) VALUES ('$email', '$user_role_id', '$created_at')";
		$result = mysqli_query($conn, $sql);
		$resultId = mysqli_insert_id($conn);

		$company_name = mysqli_real_escape_string($conn,strtoupper($_REQUEST['company_name']));
		$website_url = mysqli_real_escape_string($conn,strtoupper($_REQUEST['website_url']));
		$street_address_1 = mysqli_real_escape_string($conn,strtoupper($_REQUEST['street_address_1']));
		$street_address_2 = mysqli_real_escape_string($conn,strtoupper($_REQUEST['street_address_2']));
		if($street_address_2 == '') {
			$street_address_2 = null;
		}
		//$country_id = $_REQUEST['country_id'];
		$state_name = mysqli_real_escape_string($conn,strtoupper($_REQUEST['state']));
		$city_name = mysqli_real_escape_string($conn,strtoupper($_REQUEST['city']));
		$zip_code = mysqli_real_escape_string($conn,$_REQUEST['zip_code']);
		$telephone = mysqli_real_escape_string($conn,$_REQUEST['telephone']);
		$fax_number = mysqli_real_escape_string($conn,$_REQUEST['fax_number']);
		$adminNotes = mysqli_real_escape_string($conn,strtoupper($_REQUEST['adminNotes']));
		if($fax_number == '') {
			$fax_number = null;
		}
		$user_document = null;
		$uploaddir = 'user_documents/logo/';
		// for logo
		// ==============
		$check = file_exists($_FILES["logoDocument"]["tmp_name"]);
		if($check !== false) {
			$temp = explode(".", $_FILES["logoDocument"]["name"]);
			$uploaddir .= date('m-d-Y_hia').'.'. end($temp);
			//print_r($uploaddir);exit;
			move_uploaded_file($_FILES["logoDocument"]["tmp_name"], '../'.$uploaddir);
		}

		$sqlUserDetail = "INSERT INTO user_detail (user_id, category, company_name, website_url, email, street_address_1, street_address_2, state_name, city_name, zip_code, telephone, fax_number, user_document, adminNotes) 
		VALUES ('$resultId', '$category', '$company_name', '$website_url', '$email', '$street_address_1', '$street_address_2', '$state_name', '$city_name', '$zip_code', '$telephone', '$fax_number', '$uploaddir', '$adminNotes')";
		mysqli_query($conn, $sqlUserDetail);

		// for users documents
		// ==============
		$uploaddir = 'user_documents/user_doc/user_';
		$check = file_exists($_FILES["broacherDocument"]["tmp_name"][0]);
		if($check !== false) {
			foreach($_FILES["broacherDocument"]["name"] as $key=>$val) {
				$temp = explode(".", $_FILES["broacherDocument"]["name"][$key]);
				$uploaddir .= date('m-d-Y_hia').'.'. end($temp);
				//print_r($uploaddir);exit;
				move_uploaded_file($_FILES["broacherDocument"]["tmp_name"][$key], '../'.$uploaddir);
				$sqlUserDetail = "INSERT INTO vendor_brouchure_info (userId, documentPath, isDeleted) VALUES ('$resultId', '$uploaddir', 0)";
				mysqli_query($conn, $sqlUserDetail);
			}
		}

		$uploaddir = 'user_documents/user_doc/master_agrement_';
		$check = file_exists($_FILES["masterAgrementDocument"]["tmp_name"][0]);
		if($check !== false) {
			foreach($_FILES["masterAgrementDocument"]["name"] as $key=>$val) {
				$temp = explode(".", $_FILES["masterAgrementDocument"]["name"][$key]);
				$uploaddir .= date('m-d-Y_hia').'.'. end($temp);
				//print_r($uploaddir);exit;
				move_uploaded_file($_FILES["masterAgrementDocument"]["tmp_name"][$key], '../'.$uploaddir);
				$sqlUserDetail = "INSERT INTO vendor_master_agreement_info (userId, documentPath, isDeleted) VALUES ('$resultId', '$uploaddir', 0)";
				mysqli_query($conn, $sqlUserDetail);
			}
		}

		$claim_name = mysqli_real_escape_string($conn,strtoupper($_REQUEST['claim_name']));
		$claim_email = mysqli_real_escape_string($conn,$_REQUEST['claim_email']);
		$claim_telephone = $_REQUEST['claim_telephone'];
		$claim_fax = $_REQUEST['claim_fax'];
		$sqlClaim = "INSERT INTO claim_info (user_id, name, email, telephone, fax) VALUES ('$resultId', '$claim_name', '$claim_email', '$claim_telephone', '$claim_fax')";
		mysqli_query($conn, $sqlClaim);

		$cancellation_name = mysqli_real_escape_string($conn,strtoupper($_REQUEST['cancellation_name']));
		$cancellation_email = $_REQUEST['cancellation_email'];
		$cancellation_telephone = isset($_REQUEST['cancellation_telephone'])?$_REQUEST['cancellation_telephone']:NULL;
		$cancellation_fax_number = isset($_REQUEST['cancellation_fax_number'])?$_REQUEST['cancellation_fax_number']:NULL;

		$sqlCancellation = "INSERT INTO vendor_cancellation_info (userId, name, emailId, telephone, fax) 
		VALUES ('$resultId', '$cancellation_name', '$cancellation_email', '$cancellation_telephone', '$cancellation_fax_number')";

		mysqli_query($conn, $sqlCancellation);

		$intro = mysqli_real_escape_string($conn,strtoupper($_REQUEST['intro']));
		$pushToNews = 'no'; 
		if(isset($_REQUEST['pushToNews'])) {
			$pushToNews = 'yes'; 
		}
		$sqlOther = "INSERT INTO vendor_other_detail (vendorId, intro, pushToNews) 
		VALUES ('$resultId', '$intro', '$pushToNews')";

		mysqli_query($conn, $sqlOther);
	}
}
header('Location: ../vendor_list.php');
?>