<!-- Modal content -->
<div class="modal-content">
<?php

include_once('../elements/db_connection.php');
$user_id = $_REQUEST['user_id'];

$sql = "SELECT users.id, users.created_at, users.email, 
user_detail.category, user_detail.company_name, user_detail.website_url, user_detail.fax_number, user_detail.street_address_1, user_detail.street_address_2, user_detail.state_name, user_detail.city_name, user_detail.zip_code, user_detail.telephone, user_detail.user_document, 
claim_info.name, claim_info.email as claim_email, claim_info.telephone as claim_telephone, claim_info.fax as claim_fax, vendor_cancellation_info.id as cancellation_id, vendor_cancellation_info.name as cancellation_name, vendor_cancellation_info.emailId as cancellation_email, vendor_cancellation_info.telephone as cancellation_telephone, vendor_cancellation_info.fax as cancellation_fax,
vendor_other_detail.intro, vendor_other_detail.news, vendor_other_detail.promoCodes, vendor_other_detail.pushToNews, vendor_other_detail.pushToAdvertisngPage, vendor_other_detail.advertisment
FROM users 
LEFT JOIN user_detail ON users.id = user_detail.user_id 
LEFT JOIN claim_info ON users.id = claim_info.user_id 
LEFT JOIN vendor_cancellation_info ON users.id = vendor_cancellation_info.userId 
LEFT JOIN vendor_other_detail ON users.id = vendor_other_detail.vendorId 
WHERE users.id=$user_id 
ORDER BY users.id DESC";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
	$category = $row['category'];
?>
<div class="x_panel">
    <span class="close" onclick="close_popup()">&times;</span>  
    <a style="float: right" class="btn btn-primary print_pdf" target="_blank" href="vendor/vendor-detail_pdf_preview.php?user_id=<?php echo $user_id;?>">PDF Preview</a>
    <div class="x_content vendor_detail" style="background:white;">
        <div class="container invoice">
            <div class="invoice-header">
                <div class="row">
                <div class="col-xs-8">
                    <h1>Vendor Detail</h1>
                    <h4 class="text-muted">ID: <?php echo $row['id']?>/<?php echo $row['category']?> | Date: <?php echo Date("M  d, y", strtotime($row['created_at']));?></h4>
                </div>
                <div class="col-xs-4">
                    <div class="media">
                        <div class="media-left">
                            <img class="media-object logo" style=" -webkit-print-color-adjust: exact;" src="<?php echo $row['user_document']?>" width="100"/>
                        </div>
                        <ul class="media-body list-unstyled">
                            <li><strong><?php echo $row['company_name']?></strong></li>
                            <li><?php echo $row['website_url']?></li>
                            <li><?php echo $row['street_address_1']?>, <?php echo $row['street_address_2']?></li>
                            <li>
                            <?php echo $row['city_name'];?>, <?php echo $row['state_name'];?> - <?php echo $row['zip_code']?>
                            </li>
                            <li>Telephone: <?php echo $row['telephone']?>, Fax: <?php echo $row['fax_number']?></li>
                            <li><?php echo $row['email']?></li>
                        </ul>
                    </div>
                </div>
                </div>
            </div>
            <div class="invoice-body">
                <div class="row">
					<div class="col-xs-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Claim Details</h3>
							</div>
							<div class="panel-body">
								<dl class="dl-horizontal">
								<dt>Name</dt>
								<dd><strong><?php echo $row['name']?></strong></dd>
								<dt>Email Id</dt>
								<dd><?php echo $row['claim_email']?></dd>
								<dt>Telephone</dt>
								<dd><?php echo $row['claim_telephone']?></dd>
								<dt>Fax</dt>
								<dd><?php echo $row['claim_fax']?></dd>
							</div>
						</div>
					</div>
					
					<div class="col-xs-6">
						<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Cancellation Details</h3>
						</div>
						<div class="panel-body">
						<dl class="dl-horizontal">
						<dt>Name</dt>
						<dd><strong><?php echo $row['cancellation_name']?></strong></dd>
						<dt>Email Id</dt>
						<dd><?php echo $row['cancellation_email']?></dd>
						<dt>Telephone</dt>
						<dd><?php echo $row['cancellation_telephone']?></dd>
						<dt>Fax</dt>
						<dd><?php echo $row['cancellation_fax']?></dd>
						</div>
						</div>
					</div>
					
					<div class="col-xs-12">
						<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Other Details</h3>
						</div>
						<div class="panel-body">
						<dl class="dl-horizontal">
						<dt style="width: 190px;">Notes</dt>
						<dd style="margin-left: 200px;"><strong><?php echo $row['cancellation_name']?></strong></dd>
						<dt style="width: 190px;">Intro</dt>
						<dd style="margin-left: 200px;"><?php echo $row['intro']?></dd>
						<dt style="width: 190px;">Push Into To News</dt>
						<dd style="margin-left: 200px;"><?php echo $row['pushToNews']?></dd>
						</div>
						</div>
					</div>
                </div>
                <?php 
                $sqlClass = "SELECT name, general_range, id FROM restrict_class WHERE vendor_id = $user_id AND isDeleted=0";
                $resultClass = mysqli_query($conn, $sqlClass);
                $arClassId = array();
                while($rowClass = mysqli_fetch_assoc($resultClass)) {
                    $arClassId[] = $rowClass['id'];
                    $classId = $rowClass['id'];
                    $sqlCMap = "SELECT restrict_class_mapping_for_car_make.make_id, master_car_make.name, restrict_class_mapping_for_car_make.make_range FROM restrict_class_mapping_for_car_make LEFT JOIN master_car_make ON restrict_class_mapping_for_car_make.make_id = master_car_make.id WHERE restrict_class_mapping_for_car_make.restrict_class_id = $classId";
                    $resultCMap = mysqli_query($conn, $sqlCMap);
                    $arData = array();
                    while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
                        $make_id = $rowCMap['make_id'];
                        $arData[$make_id] = array();
                        $arData[$make_id]['name'] = $rowCMap['name'];
                        $arData[$make_id]['range'] = $rowCMap['make_range'];
                        $arData[$make_id]['model'] = array();
                    }
                    $sqlCMap = "SELECT restrict_class_mapping_for_car_model.model_id, master_car_model.model_name, master_car_model.model_make_id, restrict_class_mapping_for_car_model.model_range FROM restrict_class_mapping_for_car_model LEFT JOIN master_car_model ON restrict_class_mapping_for_car_model.model_id = master_car_model.id WHERE restrict_class_mapping_for_car_model.restrict_class_id = $classId";
                    $resultCMap = mysqli_query($conn, $sqlCMap);
                    while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
                        $model_make_id = $rowCMap['model_make_id'];
                        $sqlMake = "SELECT * FROM master_car_make WHERE name = '$model_make_id'";
                        $resultMak = mysqli_query($conn, $sqlMake);
                        while($rowMak = mysqli_fetch_assoc($resultMak)) {
                            $make_id = $rowMak['id'];
                            $model_id = $rowCMap['model_id'];
                            if(!isset($arData[$make_id])) {
                                $arData[$make_id] = array();
                                $arData[$make_id]['name'] = $rowMak['name'];
                                $arData[$make_id]['model'] = array();
                            }
                            $arData[$make_id]['model'][$model_id] = array();
                            $arData[$make_id]['model'][$model_id]['name'] = $rowCMap['model_name'];
                            $arData[$make_id]['model'][$model_id]['range'] = $rowCMap['model_range'];
                            $arData[$make_id]['model'][$model_id]['trim'] = array();
                        }
                    }
                    $sqlCMap = "SELECT restrict_class_mapping_for_car_trim.trim_id, master_car_trim.model_trim, master_car_trim.model_make_id, master_car_trim.model_name, restrict_class_mapping_for_car_trim.trim_range FROM restrict_class_mapping_for_car_trim LEFT JOIN master_car_trim ON restrict_class_mapping_for_car_trim.trim_id = master_car_trim.id WHERE restrict_class_mapping_for_car_trim.restrict_class_id = $classId";
                    $resultCMap = mysqli_query($conn, $sqlCMap);
                    while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
                        $model_make_id = $rowCMap['model_make_id'];
                        $sqlMake = "SELECT * FROM master_car_make WHERE name = '$model_make_id'";
                        $resultMak = mysqli_query($conn, $sqlMake);
                        while($rowMak = mysqli_fetch_assoc($resultMak)) {
                            $make_id = $rowMak['id'];
                            $model_name = $rowCMap['model_name'];
                            $make_name = $rowCMap['model_make_id'];
                            $trim_id = $rowCMap['trim_id'];
                            $sqlModel = "SELECT * FROM master_car_model WHERE model_name = '$model_name' AND model_make_id = '$make_name'";
                            $resultModel = mysqli_query($conn, $sqlModel);
                            while($rowModel = mysqli_fetch_assoc($resultModel)) {
                                $model_id = $rowModel['id'];
                                if(!isset($arData[$make_id])) {
                                    $arData[$make_id] = array();
                                    $arData[$make_id]['name'] = $rowMak['name'];
                                    $arData[$make_id]['model'] = array();
                                }
                                if(!isset($arData[$make_id]['model'][$model_id])) {
                                    $arData[$make_id]['model'][$model_id] = array();
                                    $arData[$make_id]['model'][$model_id]['name'] = $rowModel['model_name'];
                                    $arData[$make_id]['model'][$model_id]['trim'] = array();
                                }
                                $arData[$make_id]['model'][$model_id]['trim'][$trim_id] = array();
                                $arData[$make_id]['model'][$model_id]['trim'][$trim_id]['name'] = $rowCMap['model_trim'];
                                $arData[$make_id]['model'][$model_id]['trim'][$trim_id]['range'] = $rowCMap['trim_range'];
                            }
                        }
                    }
                    //echo '<pre>';
                    //print_r($arData);
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title restrict_class" style="cursor: pointer">Restrict Class: <?php echo $rowClass['name'];?><span style="float: right;"><?php if($rowClass['general_range'] != null) { echo 'General Range: '.$rowClass['general_range'];}?></span></h3>
                    </div>
                    <table class="table table-bordered table-condensed toggle_restrict_class" style="display: none;">
                        <thead>
                            <tr>
                                <th class="text-center colfix">Make</th>
                                <th class="text-center colfix">Model</th>
                                <th class="text-center colfix">Trim</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $arAllowedVehicals = array();
                            foreach($arData as $key=>$val) {
                                $make_count = count($val['model']);
                                $make_name = $val['name'];
                                $sql = "SELECT * FROM master_car_model WHERE model_make_id = '$make_name'";
                                $result = mysqli_query($conn, $sql);
                                while($rowModel = mysqli_fetch_assoc($result)) {
                                    $modelId = $rowModel['id'];
                                    $model_name = $rowModel['model_name'];
                                    if(isset($val['model'][$modelId])) {
                                        $sql = "SELECT * FROM master_car_trim WHERE model_make_id = '$make_name' AND model_name='$model_name'";
                                        $resultTrim = mysqli_query($conn, $sql);
                                        while($rowTrim = mysqli_fetch_assoc($resultTrim)) {
                                            $trimId = $rowTrim['id'];
                                            if(!isset($val['model'][$modelId]['trim'][$trimId])) {
                                                $arAllowedVehicals[$key]['name'] = $make_name;
                                                $arAllowedVehicals[$key]['model'][$modelId]['name'] = $model_name;
                                                $model_trim = $rowTrim['model_trim'];
                                                $arAllowedVehicals[$key]['model'][$modelId]['trim'][$trimId]['name'] = $model_trim;
                                            }
                                        }
                                    } else {
                                        $arAllowedVehicals[$key]['name'] = $make_name;
                                        $arAllowedVehicals[$key]['model'][$modelId]['name'] = $model_name;
                                        $sql = "SELECT * FROM master_car_trim WHERE model_make_id = '$make_name' AND model_name='$model_name'";
                                        $resultTrim = mysqli_query($conn, $sql);
                                        while($rowTrim = mysqli_fetch_assoc($resultTrim)) {
                                            $trimId = $rowTrim['id'];
                                            $model_trim = $rowTrim['model_trim'];
                                            $arAllowedVehicals[$key]['model'][$modelId]['trim'][$trimId]['name'] = $model_trim;
                                        }
                                    }
                                }
                                $i = 0;
                                if(!empty($val['model'])) {
                                foreach($val['model'] as $md) {
                                    if($i != 0) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo $md['name'];?><?php if(isset($md['range']) && $md['range'] != '') echo ': '.$md['range'];?>
                                </td>
                                <td>
                                <?php
                                    foreach($md['trim'] as $tr) {
                                ?>
                                    <?php echo $tr['name'];?><?php if(isset($tr['range']) && $tr['range'] != '') echo ': '.$tr['range'];?>, 
                                <?php } ?>
                                </td>
                            </tr>
                            <?php } else { ?>
                            <tr>
                                <td rowspan="<?php echo $make_count;?>">
                                    <?php echo $val['name'];?><?php if(isset($val['range']) && $val['range'] != '') echo ': '.$val['range'];?> 
                                    (Restricted)
                                </td>
                                <td>
                                    <?php echo $md['name'];?><?php if(isset($md['range']) && $md['range'] != '') echo ': '.$md['range'];?>
                                </td>
                                <td>
                                <?php
                                if(!empty($md['trim'])) {
                                    foreach($md['trim'] as $tr) {
                                ?>
                                    <?php echo $tr['name'];?><?php if(isset($tr['range']) && $tr['range'] != '') echo ': '.$tr['range'];?>, 
                                <?php } } ?>
                                </td>
                            </tr>
                            <?php } $i++; } } else { ?>
                            <tr>
                                <td rowspan="<?php echo $make_count;?>">
                                    <?php echo $val['name'];?><?php if(isset($val['range']) && $val['range'] != '') echo ': '.$val['range'];?> 
                                    (Restricted)
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php } } ?>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
                <?php
                $sql = "SELECT * FROM master_car_make";
                $result = mysqli_query($conn, $sql);
                while($rowMake = mysqli_fetch_assoc($result)) {
                    $makeId = $rowMake['id'];
                    $make_name = $rowMake['name'];
                    if(!isset($arData[$makeId])) {
                        $arAllowedVehicals[$makeId]['name'] = $make_name;
                        $sql = "SELECT * FROM master_car_model WHERE model_make_id = '$make_name'";
                        $resultModel = mysqli_query($conn, $sql);
                        while($rowModel = mysqli_fetch_assoc($resultModel)) {
                            $modelId = $rowModel['id'];
                            $model_name = $rowModel['model_name'];
                            $arAllowedVehicals[$makeId]['name'] = $make_name;
                            $arAllowedVehicals[$makeId]['model'][$modelId]['name'] = $model_name;
                            $sql = "SELECT * FROM master_car_trim WHERE model_make_id = '$make_name' AND model_name='$model_name'";
                            $resultTrim = mysqli_query($conn, $sql);
                            while($rowTrim = mysqli_fetch_assoc($resultTrim)) {
                                $trimId = $rowTrim['id'];
                                $model_trim = $rowTrim['model_trim'];
                                $arAllowedVehicals[$makeId]['model'][$modelId]['trim'][$trimId]['name'] = $model_trim;
                            }
                        }
                    }
                }
                //echo '<pre>';
                //print_r($arAllowedVehicals);
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title allowed_vehicals" style="cursor: pointer">Allowed Vehicals</h3>
                    </div>
                    <table class="table table-bordered table-condensed toggle_allowed_vehicals" style="display: none;">
                        <thead>
                            <tr>
                                <th class="text-center colfix">Make</th>
                                <th class="text-center colfix">Model</th>
                                <th class="text-center colfix">Trim</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            //echo '<pre>';
                            //print_r($arAllowedVehicals); exit;
                            foreach($arAllowedVehicals as $val) {
                                $make_count = count($val['model']);
                            ?>
                            <?php
                            $i = 0;
                                if(!empty($val['model'])) {
                                foreach($val['model'] as $md) {
                                    if($i != 0) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo $md['name'];?><?php if(isset($md['range']) && $md['range'] != '') echo ': '.$md['range'];?>
                                </td>
                                <td>
                                <?php
                                if(!empty($md['trim'])) {
                                    foreach($md['trim'] as $tr) {
                                ?>
                                    <?php echo $tr['name'];?><?php if(isset($tr['range']) && $tr['range'] != '') echo ': '.$tr['range'];?>, 
                                <?php } } ?>
                                </td>
                            </tr>
                            <?php } else { ?>
                            <tr>
                                <td rowspan="<?php echo $make_count;?>">
                                    <?php echo $val['name'];?><?php if(isset($val['range']) && $val['range'] != '') echo ': '.$val['range'];?> 
                                     (Allowed)
                                </td>
                                <td>
                                    <?php echo $md['name'];?><?php if(isset($md['range']) && $md['range'] != '') echo ': '.$md['range'];?>
                                </td>
                                <td>
                                <?php
                                if(!empty($md['trim'])) {
                                    foreach($md['trim'] as $tr) {
                                ?>
                                    <?php echo $tr['name'];?><?php if(isset($tr['range']) && $tr['range'] != '') echo ': '.$tr['range'];?>, 
                                <?php } } ?>
                                </td>
                            </tr>
                            <?php } $i++; } } else { ?>
                            <tr>
                                <td rowspan="<?php echo $make_count;?>">
                                    <?php echo $val['name'];?><?php if(isset($val['range']) && $val['range'] != '') echo ': '.$val['range'];?> 
                                    (Allowed)
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php } } ?>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
                <div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="engine_type panel-title" onclick="toggleEngine('commercialRestriction')" style="cursor: pointer">Commercial Restriction</h3>
					</div>
					<div class="commercialRestriction" style="display: none;">
					<?php
					$sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_class_id = $classId AND master_engine_type.type='Commercial Restriction'";
					$resultE = mysqli_query($conn, $sqlE);
					// output data of each row
					$dataFound = false;
					while($rowE = mysqli_fetch_assoc($resultE)) {
					?>
					<?php 
						if($rowE['engine_type_restricted'] == 'yes') {
						$dataFound = true;
					?>
						<?php echo $rowE['name'];?> ,
					<?php 
						} } 
						if(!$dataFound) {
							echo 'Nothing Found.';
						}
					?>
					</div>
				</div>
					
				<div class="clearfix"></div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="engine_type panel-title" onclick="toggleEngine('engineTypeRestriction')" style="cursor: pointer">Engine Type Restriction</h3>
					</div>
					<div class="engineTypeRestriction" style="display: none;">
					<?php
					$sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_class_id = $classId AND master_engine_type.type='Engine Type Restriction'";
					$resultE = mysqli_query($conn, $sqlE);
					// output data of each row
					$dataFound = false;
					while($rowE = mysqli_fetch_assoc($resultE)) {
						if($rowE['engine_type_restricted'] == 'yes') {
						$dataFound = true;
					?>
						<?php echo $rowE['name'];?> ,
					<?php 
						} } 
						if(!$dataFound) {
							echo 'Nothing Found.';
						}
					?>
					</div>
				</div>
				
				<div class="clearfix"></div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="engine_type panel-title" onclick="toggleEngine('driveTrein')" style="cursor: pointer">DriveTrain</h3>
					</div>
					<div class="driveTrein" style="display: none;">
					<?php
					$sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_class_id = $classId AND master_engine_type.type='Drive Trein'";
					$resultE = mysqli_query($conn, $sqlE);
					// output data of each row
					$dataFound = false;
					while($rowE = mysqli_fetch_assoc($resultE)) { 
						if($rowE['engine_type_restricted'] == 'yes') {
						$dataFound = true;
					?>
						<?php echo $rowE['name'];?> ,
					<?php 
						} } 
						if(!$dataFound) {
							echo 'Nothing Found.';
						}
					?>
					</div>
				</div>
				
				<div class="clearfix"></div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="engine_type panel-title" onclick="toggleEngine('packagesRestriction')" style="cursor: pointer">Millage & Packages Restriction</h3>
					</div>
					<div class="packagesRestriction" style="display: none;">
					<?php
					$sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_class_id = $classId AND master_engine_type.type='Millage & Packages Restriction'";
					$resultE = mysqli_query($conn, $sqlE);
					// output data of each row
					$dataFound = false;
					while($rowE = mysqli_fetch_assoc($resultE)) {
					if($rowE['engine_type_restricted'] == 'yes') {
						
					?>
						<?php echo $rowE['name'];?> ,
					<?php 
						} } 
						if(!$dataFound) {
							echo 'Nothing Found.';
						}
					?>
					</div>
				</div>
				
				<div class="clearfix"></div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 style="cursor: pointer" class="additional_restriction panel-title">Additional Restriction</h3>
					</div>
					<table class="toggle_additional_restriction table table-bordered table-condensed" style="display: none;">
						<thead>
						<tr>
							<th class="text-center colfix">Engine type (Allowed)</th>
							<th class="text-center colfix">Surcharge($)</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_class_id = $classId AND master_engine_type.type = 'main'";
						$resultE = mysqli_query($conn, $sqlE);
						// output data of each row
						while($rowE = mysqli_fetch_assoc($resultE)) {
						?>
						<tr>
						<?php if($rowE['engine_type_restricted'] != 'yes') {?>
							<td><?php echo $rowE['name'];?></td>
							<td><?php echo $rowE['surcharge'];?></td>
						<?php } ?>
						</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
				
				<div class="clearfix"></div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 style="cursor: pointer" class="additional_surcharge panel-title">Additional Surcharge</h3>
					</div>
					<table class="toggle_additional_surcharge table table-bordered table-condensed" style="display: none;">
						<thead>
						<tr>
							<th class="text-center colfix"></th>
							<th class="text-center colfix">Surcharge($)</th>
						</tr>
						</thead>
						<tbody>
						<?php
						$sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_engine_type_mapping_for_vendor.restrict_class_id = $classId AND master_engine_type.type = 'additional'";
						$resultE = mysqli_query($conn, $sqlE);
						// output data of each row
						while($rowE = mysqli_fetch_assoc($resultE)) {
						?>
						<tr>
						<?php if($rowE['engine_type_restricted'] != 'yes') {?>
							<td><?php echo $rowE['name'];?></td>
							<td><?php echo $rowE['surcharge'];?></td>
						<?php } ?>
						</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
								
					<div class="clearfix"></div>
                    <?php
                    foreach($arClassId as $cId) {
                        $sqlPP = "SELECT plan_terms.plansId, plans.name FROM plan_terms LEFT JOIN plans ON plan_terms.plansId = plans.id WHERE plan_terms.vendorId = $user_id AND plan_terms.classId = $cId GROUP BY plan_terms.vendorId, plan_terms.classId, plan_terms.plansId";
                        $resultPP = mysqli_query($conn, $sqlPP);
                        while($rowPP = mysqli_fetch_assoc($resultPP)) {
                            $planID = $rowPP['plansId'];
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 style="cursor: pointer" onclick="fnShowHidePlan('plan_<?php echo $planID;?>_toggle')" class="panel-title plan_<?php echo $planID;?>">Plan Name: <?php echo $rowPP['name']?></h3>
                            </div>
                            <?php
                            $sqlTT = "SELECT user_detail.user_id FROM plan_terms LEFT JOIN user_detail ON plan_terms.vendorId = user_detail.user_id WHERE plansId = $planID GROUP BY plan_terms.vendorId";
                            $resultTT = mysqli_query($conn, $sqlTT);
                            while($rowTT = mysqli_fetch_assoc($resultTT)) {
                                $sqlCMap = "SELECT restrict_class.name, plan_terms.classId FROM plan_terms LEFT JOIN restrict_class ON plan_terms.classId = restrict_class.id WHERE plan_terms.plansId = $planID AND plan_terms.vendorId = $user_id GROUP BY plan_terms.classId";
                                $resultCMap = mysqli_query($conn, $sqlCMap);
                                while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
                                    $classId = $rowCMap['classId'];
                            ?>
                                
								<?php if($category == 'VSC') {?>
								<table style="width:100%; border:1px solid #eee; display:none;" class="plan_<?php echo $planID;?>_toggle">
									<thead>
										<tr style="border:1px solid #eee;">
											<th>Sl. No.</th>
											<th>Term</th>
											<th>Cover Mileage</th>
											<th>Up To Mileage</th>
											<th>Manufacturer Warranty</th>
											<th>Mileage From</th>
											<th>Mileage To</th>
											<th>Price</th>
											<th>Deductible</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $planID AND  vendorId = $user_id AND classId = $classId";
										$resultPlan = mysqli_query($conn, $sqlPlan);
										$slNo = 1;
										if (mysqli_num_rows($resultPlan) > 0) {
											// output data of each row
											while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
										?>
												<tr>
													<td><?php echo $slNo;?></td>
													<td><?php echo $rowPlan['termNo'];?> <?php echo $rowPlan['TermSelect'];?></td>
													<td><?php echo $rowPlan['coverMailage'];?></td>
													<td><?php echo $rowPlan['upToMailage'];?></td>
													<td><?php echo $rowPlan['manufacturerWarranty'];?></td>
													<td><?php echo $rowPlan['milage_from'];?></td>
													<td><?php echo $rowPlan['milage_to'];?></td>
													<td><?php echo $rowPlan['price_unit'];?> <?php echo $rowPlan['price'];?></td>
													<td><?php echo $rowPlan['deductable'];?></td>
												</tr>
										<?php
												$slNo++;
											}
										}
										?>
									</tbody>
								</table>
							<?php
							} else  if($category == 'INSURANCE') {
							?>
								<table style="width:100%; border:1px solid #eee; display:none;" class="plan_<?php echo $planID;?>_toggle">
									<thead>
										<tr style="border:1px solid #eee;">
											<th>Sl. No.</th>
											<th>Term</th>
											<th>Store Type</th>
											<th>Price</th>
											<th>Deductible</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $planID AND  vendorId = $user_id AND classId = $classId";
										$resultPlan = mysqli_query($conn, $sqlPlan);
										$slNo = 1;
										if (mysqli_num_rows($resultPlan) > 0) {
											// output data of each row
											while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
										?>
												<tr>
													<td><?php echo $slNo;?></td>
													<td><?php echo $rowPlan['termNo'];?> <?php echo $rowPlan['TermSelectInsurance'];?></td>
													<td><?php echo $rowPlan['storeType'];?></td>
													<td><?php echo $rowPlan['price_unit'];?> <?php echo $rowPlan['price'];?></td>
													<td><?php echo $rowPlan['deductable'];?></td>
												</tr>
										<?php
												$slNo++;
											}
										}
										?>
									</tbody>
								</table>
							<?php
							} else  if($category == 'GAP') {
							?>
								<table style="width:100%; border:1px solid #eee; display:none;" class="plan_<?php echo $planID;?>_toggle">
									<thead>
										<tr style="border:1px solid #eee;">
											<th>Sl. No.</th>
											<th>Term From</th>
											<th>Term To</th>
											<th>Coverage Limit</th>
											<th>Store Type</th>
											<th>Payment</th>
											<th>Price</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $planID AND  vendorId = $user_id AND classId = $classId";
										$resultPlan = mysqli_query($conn, $sqlPlan);
										$slNo = 1;
										if (mysqli_num_rows($resultPlan) > 0) {
											// output data of each row
											while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
										?>
												<tr>
													<td><?php echo $slNo;?></td>
													<td><?php echo $rowPlan['termFrom'];?> <?php echo $rowPlan['TermFromOption'];?></td>
													<td><?php echo $rowPlan['termTo'];?> <?php echo $rowPlan['TermToOption'];?></td>
													<td><?php if($rowPlan['coverageLimitNone'] == 'on') { echo 'None'; } else { echo $rowPlan['coverageLimit']; } ?></td>
													<td><?php echo $rowPlan['storeType'];?></td>
													<td><?php echo $rowPlan['payment'];?></td>
													<td><?php echo $rowPlan['price_unit'];?> <?php echo $rowPlan['price'];?></td>
												</tr>
										<?php
												$slNo++;
											}
										}
										?>
									</tbody>
								</table>
							<?php
							} else  if($category == 'ANCILLARY') {
							?>
								<table style="width:100%; border:1px solid #eee; display:none;" class="plan_<?php echo $planID;?>_toggle">
									<thead>
										<tr style="border:1px solid #eee;">
											<th>Sl. No.</th>
											<th>Term</th>
											<th>Coverage Limit</th>
											<th>Store Type</th>
											<th>Options</th>
											<th>Payment</th>
											<th>Price</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										echo $sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $planID AND  vendorId = $user_id AND classId = $classId";
										$resultPlan = mysqli_query($conn, $sqlPlan);
										$slNo = 1;
										if (mysqli_num_rows($resultPlan) > 0) {
											// output data of each row
											while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
										?>
												<tr>
													<td><?php echo $slNo;?></td>
													<td><?php echo $rowPlan['termNo'];?> <?php echo $rowPlan['TermSelectAnciallary'];?></td>
													<td><?php if($rowPlan['coverageLimitNone'] == 'on') { echo 'None'; } else { echo $rowPlan['coverageLimit']; } ?></td>
													<td><?php echo $rowPlan['storeType'];?></td>
													<td><?php echo $rowPlan['options'];?></td>
													<td><?php echo $rowPlan['payment'];?></td>
													<td><?php echo $rowPlan['price_unit'];?> <?php echo $rowPlan['price'];?></td>
												</tr>
										<?php
												$slNo++;
											}
										}
										?>
									</tbody>
								</table>
							<?php
							}
							?>
                            <?php
                                }
                            }
                            ?>
                        </div>
                    <?php
                        }
                    }
                    ?>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </div>
    <div class="invoice-footer">
        Thanks,
        <br/>
        <strong>~Car Insurance Coverage~</strong>
    </div>
</div>
<?php
}
?>
</div>
<div id="editor"></div>