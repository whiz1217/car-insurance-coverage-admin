<?php
//print_r($_COOKIE['carDealerZoneAdmin']);
if(isset($_SESSION['carDealerZoneAdmin'])) {
	$carDealerZoneAdmin = json_decode($_SESSION['carDealerZoneAdmin']);
	if(!isset($carDealerZoneAdmin->id) || $carDealerZoneAdmin->id == '' || $carDealerZoneAdmin->id == null) {
		//header( "Location: admin_login.php" );
		echo "<script type='text/javascript'>  window.location='admin_login.php'; </script>";
		exit;
	}
} else {
	//header( "Location: admin_login.php" );
	echo "<script type='text/javascript'>  window.location='admin_login.php'; </script>";
	exit;
}
?>
<style>
.header {
  overflow: hidden;
  background-color: #fff;
  padding: 20px 10px;
  height: 55px;
}

@media screen and (max-width: 500px) {
  .header-right {
    float: right;
  }  
	.toggle a{
		padding: 15px 12px 0;
	}
}
.website-logo {
	width: 15%;
}
</style>
<div class="header">
	<div style="border: 0; position: absolute; width: 100%; height: 90px;">
		<img class="website-logo" src="images/logo.svg" alt="logo">
	</div>
	<div class="header-right">
		<nav>
		  <ul class="nav navbar-nav navbar-right" style="margin-top: -20px;">
			<li class="">
			  <a href="javascript:void(0);" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				<img src="images/profile.png" alt="">Admin
				<span class=" fa fa-angle-down"></span>
			  </a>
			  <ul class="dropdown-menu dropdown-usermenu pull-right">
				<li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
			  </ul>
			</li>
		  </ul>
		</nav>
	</div>
</div>
<div class="top_nav">
  <div class="nav_menu">
	<nav>
	  <div class="nav toggle" style="padding-top: 3px; position: relative;">
		<a id="menu_toggle"><i class="fa fa-bars"></i></a>
	  </div>
	</nav>
  </div>
</div>
        