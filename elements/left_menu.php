<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Admin Panel</title>

		<!-- Bootstrap -->
		<link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<!-- NProgress -->
		<link href="vendors/nprogress/nprogress.css" rel="stylesheet">
		<!-- iCheck -->
		<link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
		
		<!-- bootstrap-progressbar -->
		<link href="vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
		<!-- JQVMap -->
		<link href="vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
		<!-- bootstrap-daterangepicker -->
		<link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
		  <link rel="stylesheet" href="vendors/multiple-select-master/multiple-select.css" />

		<!-- Custom Theme Style -->
		<link href="build/css/custom.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="vendors/DataTables/datatables.min.css"/>
		<link rel="stylesheet" type="text/css" href="css/style.css"/>
	</head>
	<style>
	.left_col{
		background: #286090;
		top: 110px;
	}
	.nav-sm ul.nav.child_menu {
		background: #286090;
	}
	.profile_info span {
		color: #fea52d;
	}
	.nav-sm ul.nav.child_menu .current-page:hover {
		background-color: rgb(220, 67, 18);
	}
	.nav_title{
		background: #fff;
	}
	.sidebar-footer{
		background: #286090;
	}
	.toggle a i{
		color: #286090;
	}
	.toggle a{
	   padding: 15px 21px 0;
	}
	.main_container{
		background: #286090;
	}
	.nav.side-menu>li.active>a {
		background: linear-gradient(#286090,#2C4257),#2A3F54;
		font-weight: bold;
	}
	.nav-sm .nav.side-menu>li {
		min-height: 80px;
	}
	.nav-md .nav.side-menu>li>a {
		font-weight: bold;
	}
	.nav-sm .nav.side-menu>li>a {
		font-weight: bold;
		min-height: 80px;
	}
	.nav.side-menu>li.active, .nav.side-menu>li.current-page {
		border-right: 5px solid #ec9221;
	}
	.nav-sm .nav.child_menu li.active, .nav-sm .nav.side-menu li.active-sm {
		border-right: 5px solid #ec9221;
	}
	.nav.child_menu li:hover {
		background-color: rgb(220, 67, 18);
	}
	.nav-md ul.nav.child_menu li:after {
		border-left: 3px solid #ffffff;
		bottom: 0;
		content: "";
		left: 25.5px;
		position: absolute;
		top: 0;
	}
	.nav-md ul.nav.child_menu li:before {
		background: #ffffff;
		bottom: auto;
		content: "";
		height: 8px;
		left: 23px;
		margin-top: 15px;
		position: absolute;
		right: auto;
		width: 8px;
		z-index: 1;
		border-radius: 50%;
	}
	.btn-danger {
		background-color: #fea52d;
		border-color: #fea52d;
	}
	.btn-danger:hover {
		background-color: rgb(220, 67, 18);
		border-color: rgb(220, 67, 18);
	}
	.nav_menu {
		background: #fea52d;
		/*border-bottom: 54px solid #fea52d;*/
	}
	.right_col {
		background: #f7f7f7;
	}
	footer {
		background: #286090;
	}
	.side-menu a span.fa {
		margin-right: 0px!important;
	}
	body {
		background: #286090;
	}
	</style>
	<body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/profile.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2>Admin</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                 <center><a href="index.php" style="color: #fea52d;"><i class="fa fa-home"></i></h3></a></center>
				          <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i> Vendors<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="vendor_list.php">List</a></li>
                        <li><a href="add_vendor.php">Add Vendor</a></li>
                      </ul>
                    </li>
                  </ul>
				  <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i>Restrict Class <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="restrict_class_list.php">List</a></li>
                        <li><a href="add_restrict_class.php">Add Class</a></li>
                      </ul>
                    </li>
                  </ul>
				          <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i>Plan <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="plan_list.php">List</a></li>
                        <li><a href="add_plan.php">Add Plan</a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i>State Mapping <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="state_mapping_list.php">List</a></li>
                        <li><a href="state_mapping.php">Add State Mapping</a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i>Feature State Mapping <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="feature_state_mapping_list.php">List</a></li>
                        <li><a href="feature_state_mapping.php">Add State Mapping</a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i>Dealer <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="dealer_list.php">List</a></li>
                        <li><a href="add_dealer.php">Add Dealer</a></li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i>Apples To Apples<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="add_apple_to_apple_list.php">List</a></li>
                        <li><a href="add_apple_to_apple.php">Add Mapping</a></li>
						<li><a href="range_settings.php">Range Setting</a></li>
                        <li><a href="apple_to_apple_category.php">Category</a></li>
                        <li><a href="add_apple_to_apple_category.php">Add Category</a></li>
                        <li><a href="apple_to_apple_sub_category.php">Sub Category</a></li>
                        <li><a href="add_apple_to_apple_sub_category.php">Add Sub Category</a></li>
                      </ul>
                    </li>
                  </ul>
				  <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i>Advertisement<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="advertisement_list.php">List</a></li>
                        <li><a href="add_advertisement.php">Add Advertisement</a></li>
                      </ul>
                    </li>
                  </ul>
				  <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i>News<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="news_list.php">List</a></li>
                        <li><a href="add_news.php">Add News</a></li>
                      </ul>
                    </li>
                  </ul>
				  <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i>Dealer Portal<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="address_list.php">Address</a></li>
                        <li><a href="logo_list.php">Logo</a></li>
                        <li><a href="company_number_list.php">Company Number</a></li>
                        <li><a href="faq_list.php">FAQ</a></li>
                        <li><a href="add_faq.php">Add FAQ</a></li>
                        <li><a href="video_category_list.php">Video Category List</a></li>
                        <li><a href="add_video_category.php">Add Video Category</a></li>
                        <li><a href="video_list.php">Video List</a></li>
                        <li><a href="add_video.php">Add Video</a></li>
                      </ul>
                    </li>
                  </ul>
				  <ul class="nav side-menu">
                    <li><a><i class="fa fa-edit"></i>Comunication<span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="email_template_list.php">Email Template List</a></li>
                      </ul>
                    </li>
                  </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="logout.php">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>