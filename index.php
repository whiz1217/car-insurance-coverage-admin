<?php 
include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <!-- /top tiles -->
          <!-- /page content -->
          <div class="x_panel">
            <div class="x_content">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-child"></i>
                    </div>
                    <div class="count">&nbsp; </div>

                    <h3><a href="vendor_list.php">Vendor</a></h3>
                    <p>&nbsp;</p>
                  </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-files-o"></i>
                    </div>
                    <div class="count">&nbsp;</div>

                    <h3><a href="restrict_class_list.php">Restrict Classes</a></h3>
                    <p>&nbsp; </p>
                  </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-sort-amount-desc"></i>
                    </div>
                    <div class="count">&nbsp;</div>

                    <h3><a href="plan_list.php">Plans</a></h3>
                    <p>&nbsp;</p>
                  </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                  <div class="tile-stats">
                    <div class="icon"><i class="fa fa-sitemap"></i>
                    </div>
                    <div class="count">&nbsp;</div>

                    <h3><a href="add_apple_to_apple_list.php">Apple To Apple</a></h3>
                    <p>&nbsp;</p>
                  </div>
                </div>
              </div>
            </div>
            </div>
            </div>

        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->