<!-- Modal content -->
<div class="modal-content">
<?php
include_once('elements/db_connection.php');
$id = $_REQUEST['id'];
$sql = "SELECT * FROM dealer_faq WHERE id = $id";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
?>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="update_faq.php" method="post">
				
				<div class="item form-group">
					<label class="col-md-2">Question</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<?php echo $row['question'];?>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="item form-group">
					<label class="col-md-2">Answer</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<?php echo nl2br($row['answer']);?>
					</div>
				</div> 
				<div class="clearfix"></div>
				
				<div class="item form-group">
					<label class="col-md-2">Created At</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<?php echo $row['created_at'];?>
					</div>
				</div>      				
            </form>
        </div>
    </div>
<?php } ?>   
</div>
