<table id="restrict_class_list">
    <thead>
        <tr>
            <th>Sl. no.</th>
            <th>Category</th>
            <th>Vendor Name</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        include_once('elements/db_connection.php');
        
        if($_POST['cat'] == '') {
            $cat = $_POST['cat'];
            $sql = "SELECT user_detail.company_name, user_detail.category, vendor_state_mapping.id, vendor_state_mapping.vendor_id, vendor_state_mapping.states FROM vendor_state_mapping LEFT JOIN user_detail ON vendor_state_mapping.vendor_id = user_detail.user_id WHERE vendor_state_mapping.isDeleted = 0 AND vendor_state_mapping.is_featured = 'yes' GROUP BY vendor_id";  
        } else if(isset($_POST['id']) && $_POST['id'] != '') {
            $cat = $_POST['cat'];
            $user_id = $_POST['id'];
            $sql = "SELECT user_detail.company_name, user_detail.category, vendor_state_mapping.id, vendor_state_mapping.vendor_id, vendor_state_mapping.states FROM vendor_state_mapping LEFT JOIN user_detail ON vendor_state_mapping.vendor_id = user_detail.user_id WHERE vendor_state_mapping.isDeleted = 0 AND user_detail.category='$cat' AND user_detail.user_id = '$user_id' AND vendor_state_mapping.is_featured = 'yes' GROUP BY vendor_id";            
            //$sql = "SELECT user_detail.company_name, user_detail.category, restrict_class.id, restrict_class.name FROM restrict_class LEFT JOIN user_detail ON user_detail.user_id = restrict_class.vendor_id WHERE restrict_class.isDeleted = 0 AND user_detail.category='$cat' AND user_detail.user_id = '$user_id' ORDER BY `id` DESC";
        } else {
            $cat = $_POST['cat'];
            //$sql = "SELECT user_detail.company_name, user_detail.category, restrict_class.id, restrict_class.name FROM restrict_class LEFT JOIN user_detail ON user_detail.user_id = restrict_class.vendor_id WHERE restrict_class.isDeleted = 0 AND user_detail.category='$cat' ORDER BY `id` DESC";
            $sql = "SELECT user_detail.company_name, user_detail.category, vendor_state_mapping.id, vendor_state_mapping.vendor_id, vendor_state_mapping.states FROM vendor_state_mapping LEFT JOIN user_detail ON vendor_state_mapping.vendor_id = user_detail.user_id WHERE vendor_state_mapping.isDeleted = 0 AND user_detail.category='$cat' AND vendor_state_mapping.is_featured = 'yes' GROUP BY vendor_id";
        }
        $result = mysqli_query($conn, $sql);
        $slNo = 1;
        if (mysqli_num_rows($result) > 0) {
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {
        ?>
                 <tr>
                 <td><?php echo $slNo;?></td>
                 <td><?php echo $row['category'];?></td>
                 <td><?php echo $row['company_name'];?></td>
                 <td>
                 <a class='btn btn-danger' onclick="delete_sate_mapping(<?php echo $row['id'];?>)" style="cursor:pointer;">Delete</a>
                 <a class='btn btn-primary' onclick="edit_class(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
                 <a class='btn btn-primary' onclick="show_detail('<?php echo $row['vendor_id'];?>')" style="cursor:pointer;">View</a>
                 </td>
               </tr>
        <?php
                $slNo++;
            }
        } else {
            $error_message = 'Wrong username or password';
        }
        ?>
    </tbody>
</table>