<?php 

include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
?>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
	<div class="x_title">
		<h2>Add Video Category</h2>
		<ul class="nav navbar-right panel_toolbox">
		  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		  </li>
		</ul>
		<div class="clearfix"></div>
	</div>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="save_video_category.php" method="post">
				
				<div class="item form-group">
					<label class="col-md-2">Name</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" name="name" class="form-control" value="" placeholder="Name">
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="form-group">
				  <div class="col-md-6 col-md-offset-3">
					<a href="video_category_list.php" class="btn btn-danger">Cancel</a>
					<button id="send" type="submit" class="btn btn-success">Save</button>
				  </div>
				</div>             				
            </form>
        </div>
    </div>
</div>
<!-- footer content -->
<?php 
include_once('elements/footer.php');
?>
<!-- /footer content -->
