<?php
include_once('elements/db_connection.php');
$id = $_REQUEST['id'];
$sql = "SELECT * FROM plans WHERE id = $id";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
?>
<div class="x_panel">
    <span class="close" onclick="close_popup()">&times;</span>
    <div class="x_content">
        <div class="item form-group col-md-6">
            <label class="control-label col-md-5 col-sm-5 col-xs-12">Plan Name:</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $row['name']?>
            </div>
        </div>
<?php 
} 
$sql = "SELECT user_detail.user_id FROM plan_terms LEFT JOIN user_detail ON plan_terms.vendorId = user_detail.user_id WHERE plansId = $id GROUP BY plan_terms.vendorId";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
    $vendor_id =  $row['user_id'];
    $sqlUser = "SELECT * FROM user_detail WHERE user_id = $vendor_id";
    $resultUser = mysqli_query($conn, $sqlUser);
    while($rowUser = mysqli_fetch_assoc($resultUser)) {
		$category = $rowUser['category'];
    ?>
		<div class="item form-group col-md-6">
			<label class="control-label col-md-5 col-sm-5 col-xs-12">Vendor Company Name:</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<img class="media-object logo" style=" -webkit-print-color-adjust: exact;" src="<?php echo $rowUser['user_document']?>" width="100"/><br />
				<?php echo $rowUser['company_name'];?><br />
				<?php echo $rowUser['website_url'];?><br />
				<?php echo $rowUser['street_address_1']?>, <?php echo $rowUser['street_address_2']?><br />
				<?php echo $rowUser['city_name'];?>, <?php echo $rowUser['state_name'];?> - <?php echo $rowUser['zip_code']?><br />
				Telephone: <?php echo $rowUser['telephone']?>, Fax: <?php echo $rowUser['fax_number']?><br />
				<?php echo $rowUser['email'];?><br />
			</div>
		</div>
	</div>
    <?php } ?>
    <div class="x_title">
        <h2>Plan Terms info:</h2>
        <div class="clearfix"></div>
    </div>
    <?php 
    $sqlCMap = "SELECT restrict_class.name, plan_terms.classId FROM plan_terms LEFT JOIN restrict_class ON plan_terms.classId = restrict_class.id WHERE plan_terms.plansId = $id AND plan_terms.vendorId = $vendor_id GROUP BY plan_terms.classId";
    $resultCMap = mysqli_query($conn, $sqlCMap);
    while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
        $classId = $rowCMap['classId'];
    ?>

        <div class="item form-group col-md-12">
            <div class="col-md-12">
                <center>Class Name: <a style="cursor: pointer" href="class_detail.php?id=<?php echo $classId;?>"><?php echo $rowCMap['name']?></a></center>
            </div>
        </div>
		<?php if($category == 'VSC') {?>
			<table id="restrict_class_list" style="width:100%; border:1px solid #eee;">
				<thead>
					<tr style="border:1px solid #eee;">
						<th>Sl. No.</th>
						<th>Term</th>
						<th>Cover Mileage</th>
						<th>Up To Mileage</th>
						<th>Manufacturer Warranty</th>
						<th>Mileage From</th>
						<th>Mileage To</th>
						<th>Price</th>
						<th>Deductible</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $id AND  vendorId = $vendor_id AND classId = $classId";
					$resultPlan = mysqli_query($conn, $sqlPlan);
					$slNo = 1;
					if (mysqli_num_rows($resultPlan) > 0) {
						// output data of each row
						while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
					?>
							<tr>
								<td><?php echo $slNo;?></td>
								<td><?php echo $rowPlan['termNo'];?> <?php echo $rowPlan['TermSelect'];?></td>
								<td><?php echo $rowPlan['coverMailage'];?></td>
								<td><?php echo $rowPlan['upToMailage'];?></td>
								<td><?php echo $rowPlan['manufacturerWarranty'];?></td>
								<td><?php echo $rowPlan['milage_from'];?></td>
								<td><?php echo $rowPlan['milage_to'];?></td>
								<td><?php echo $rowPlan['price_unit'];?> <?php echo $rowPlan['price'];?></td>
								<td><?php echo $rowPlan['deductable'];?></td>
							</tr>
					<?php
							$slNo++;
						}
					}
					?>
				</tbody>
			</table>
		<?php
		} else  if($category == 'INSURANCE') {
		?>
			<table id="restrict_class_list" style="width:100%; border:1px solid #eee;">
				<thead>
					<tr style="border:1px solid #eee;">
						<th>Sl. No.</th>
						<th>Term</th>
						<th>Store Type</th>
						<th>Price</th>
						<th>Deductible</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $id AND  vendorId = $vendor_id AND classId = $classId";
					$resultPlan = mysqli_query($conn, $sqlPlan);
					$slNo = 1;
					if (mysqli_num_rows($resultPlan) > 0) {
						// output data of each row
						while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
					?>
							<tr>
								<td><?php echo $slNo;?></td>
								<td><?php echo $rowPlan['termNo'];?> <?php echo $rowPlan['TermSelectInsurance'];?></td>
								<td><?php echo $rowPlan['storeType'];?></td>
								<td><?php echo $rowPlan['price_unit'];?> <?php echo $rowPlan['price'];?></td>
								<td><?php echo $rowPlan['deductable'];?></td>
							</tr>
					<?php
							$slNo++;
						}
					}
					?>
				</tbody>
			</table>
		<?php
		} else  if($category == 'GAP') {
		?>
			<table id="restrict_class_list" style="width:100%; border:1px solid #eee;">
				<thead>
					<tr style="border:1px solid #eee;">
						<th>Sl. No.</th>
						<th>Term From</th>
						<th>Term To</th>
						<th>Coverage Limit</th>
						<th>Store Type</th>
						<th>Payment</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $id AND  vendorId = $vendor_id AND classId = $classId";
					$resultPlan = mysqli_query($conn, $sqlPlan);
					$slNo = 1;
					if (mysqli_num_rows($resultPlan) > 0) {
						// output data of each row
						while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
					?>
							<tr>
								<td><?php echo $slNo;?></td>
								<td><?php echo $rowPlan['termFrom'];?> <?php echo $rowPlan['TermFromOption'];?></td>
								<td><?php echo $rowPlan['termTo'];?> <?php echo $rowPlan['TermToOption'];?></td>
								<td><?php if($rowPlan['coverageLimitNone'] == 'on') { echo 'None'; } else { echo $rowPlan['coverageLimit']; } ?></td>
								<td><?php echo $rowPlan['storeType'];?></td>
								<td><?php echo $rowPlan['payment'];?></td>
								<td><?php echo $rowPlan['price_unit'];?> <?php echo $rowPlan['price'];?></td>
							</tr>
					<?php
							$slNo++;
						}
					}
					?>
				</tbody>
			</table>
		<?php
		} else  if($category == 'ANCILLARY') {
		?>
			<table id="restrict_class_list" style="width:100%; border:1px solid #eee;">
				<thead>
					<tr style="border:1px solid #eee;">
						<th>Sl. No.</th>
						<th>Term</th>
						<th>Coverage Limit</th>
						<th>Store Type</th>
						<th>Options</th>
						<th>Payment</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					echo $sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $id AND  vendorId = $vendor_id AND classId = $classId";
					$resultPlan = mysqli_query($conn, $sqlPlan);
					$slNo = 1;
					if (mysqli_num_rows($resultPlan) > 0) {
						// output data of each row
						while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
					?>
							<tr>
								<td><?php echo $slNo;?></td>
								<td><?php echo $rowPlan['termNo'];?> <?php echo $rowPlan['TermSelectAnciallary'];?></td>
								<td><?php if($rowPlan['coverageLimitNone'] == 'on') { echo 'None'; } else { echo $rowPlan['coverageLimit']; } ?></td>
								<td><?php echo $rowPlan['storeType'];?></td>
								<td><?php echo $rowPlan['options'];?></td>
								<td><?php echo $rowPlan['payment'];?></td>
								<td><?php echo $rowPlan['price_unit'];?> <?php echo $rowPlan['price'];?></td>
							</tr>
					<?php
							$slNo++;
						}
					}
					?>
				</tbody>
			</table>
		<?php
		}
		?>
	<?php
	}
	?>
    </div>
<?php 
} 
?>
</div>