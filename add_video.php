<?php 
include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
?>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
	<div class="x_title">
		<h2>Add Video</h2>
		<ul class="nav navbar-right panel_toolbox">
		  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		  </li>
		</ul>
		<div class="clearfix"></div>
	</div>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="save_video.php" method="post" enctype="multipart/form-data">
				
				<div class="item form-group">
					<label class="col-md-2">Category</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<select name="video_category_id" class="form-control">
						<?php 
                            $sql = "SELECT * FROM master_video_category WHERE is_deleted='0'";
                            $result = mysqli_query($conn, $sql);
                            $slNo = 1;
                            if (mysqli_num_rows($result) > 0) {
                                // output data of each row
                                while($row = mysqli_fetch_assoc($result)) {
                            ?>
							<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
						<?php } }?>
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="item form-group">
					<label class="col-md-2">Sub Category</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" name="sub_category" class="form-control" value="" placeholder="Sub Category">
					</div>
				</div> 
				<div class="clearfix"></div>
				
				<div class="item form-group">
					<label class="col-md-2">Upload Video</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="file" name="video">
					</div>
				</div> 
				
				<div class="clearfix"></div>
				<div class="form-group">
				  <div class="col-md-6 col-md-offset-3">
					<a href="video_list.php" class="btn btn-danger">Cancel</a>
					<button id="send" type="submit" class="btn btn-success">Save</button>
				  </div>
				</div>             				
            </form>
        </div>
    </div>
</div>
<!-- footer content -->
<?php 
include_once('elements/footer.php');
?>
<!-- /footer content -->
