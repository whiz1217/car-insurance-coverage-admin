<?php 
include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
?>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
	<div class="x_title">
		<h2>Add FAQ</h2>
		<ul class="nav navbar-right panel_toolbox">
		  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		  </li>
		</ul>
		<div class="clearfix"></div>
	</div>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="save_faq.php" method="post">
				
				<div class="item form-group">
					<label class="col-md-2">Question</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" name="question" class="form-control" value="" placeholder="Question">
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="item form-group">
					<label class="col-md-2">Answer</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<textarea type="text" name="answer" class="form-control" placeholder="Answer"></textarea>
					</div>
				</div> 
				
				<div class="clearfix"></div>
				<div class="form-group">
				  <div class="col-md-6 col-md-offset-3">
					<a href="faq_list.php" class="btn btn-danger">Cancel</a>
					<button id="send" type="submit" class="btn btn-success">Save</button>
				  </div>
				</div>             				
            </form>
        </div>
    </div>
</div>
<!-- footer content -->
<?php 
include_once('elements/footer.php');
?>
<!-- /footer content -->
