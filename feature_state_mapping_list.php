<?php 
	include_once('elements/db_connection.php');
	include_once('elements/left_menu.php');
    include_once('elements/header.php');
?>
<!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_title">
                <h2>Featured State Mapping List</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
              <div class="item form-group pull-right">
                    <select onchange="show_class_by_category(event)" class="form-control cat_id" style="width: 177px;">
                        <option value="">All Category</option>
                        <option value="VSC">VSC</option>
                        <option value="GAP">GAP</option>
                        <option value="ANCILLARY">ANCILLARY</option>
                        <option value="INSURANCE">INSURANCE</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="item form-group vendor_list_filter pull-right">
                    <select class="form-control" disabled style="width: 177px;">
                        <option value="">All Vendor</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="class_list_table">
                <table id="restrict_class_list">
                    <thead>
                        <tr>
                            <th>Sl. no.</th>
                            <th>Category</th>
                            <th>Vendor Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT user_detail.company_name, user_detail.category, vendor_state_mapping.id, vendor_state_mapping.vendor_id, vendor_state_mapping.states FROM vendor_state_mapping LEFT JOIN user_detail ON vendor_state_mapping.vendor_id = user_detail.user_id WHERE vendor_state_mapping.isDeleted = 0 AND vendor_state_mapping.is_featured = 'yes' GROUP BY vendor_id";
                            $result = mysqli_query($conn, $sql);
                            $slNo = 1;
                            if (mysqli_num_rows($result) > 0) {
                                // output data of each row
                                while($row = mysqli_fetch_assoc($result)) {
                            ?>
                                <tr>
                                    <td><?php echo $slNo;?></td>
                                    <td><?php echo $row['category'];?></td>
                                    <td><?php echo $row['company_name'];?></td>
                                    <td>
                                    <a class='btn btn-danger' onclick="delete_sate_mapping(<?php echo $row['id'];?>)" style="cursor:pointer;">Delete</a>
                                    <a class='btn btn-primary' onclick="edit_class(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
                                    <a class='btn btn-primary' onclick="show_detail('<?php echo $row['vendor_id'];?>')" style="cursor:pointer;">View</a>
                                    </td>
                                </tr>
                            <?php
                            $slNo++;
                                }
                            } else {
                            $error_message = 'Wrong username or password';
                            }
                        ?>
                    </tbody>
                    </table>
                </div>
                    
              </div>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content class-modal" >
                Loading...
            </div>

        </div>
        <!-- /page content -->
        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
<script>
var table;
$(document).ready(function(){
    table = $('#restrict_class_list').DataTable();
});  
function edit_class(id) {
    $.ajax({
        method: "POST",
        url: "featured_state_mapping_list_edit.php",
        data: { id : id }
    }).done(function(data) {
        $('#myModal').html(data);
    });
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}
function delete_sate_mapping(id) {
    $.ajax({
        method: "POST",
        url: "state-mapping-delete.php",
        data: { id : id }
    }).done(function(data) {
        $('#myModal').html(data);
    });
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}
function show_detail(id) {
    $.ajax({
        method: "POST",
        url: "featured_state-map-detail.php",
        data: { id : id }
    }).done(function(data) {
        $('#myModal').html(data);
    });
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}
function close_popup() {
    $('#myModal').html('Loading...');
    var modal = document.getElementById('myModal');
    modal.style.display = "none";
}
function show_class_by_category(e) {
    var val = $(e.target).val();
    $.ajax({
        method: "POST",
        url: "featured_state_mapping_filtered_list.php",
        data: { cat : val }
    }).done(function(data) {
        
        table.clear();
        table.destroy();
        $('.class_list_table').html(data);
        table = $("#restrict_class_list").DataTable();
    });
    $.ajax({
        method: "POST",
        url: "class_vendor_option.php",
        data: { cat : val }
    }).done(function(data) {
        $('.vendor_list_filter').html(data);
    });
}
function show_class_by_vendor(e) {
    var cat = $('.cat_id').val();
    var val = $(e.target).val();
    $.ajax({
        method: "POST",
        url: "featured_state_mapping_filtered_list.php",
        data: { cat : cat, id: val}
    }).done(function(data) {
        //console.log(data);
        table.clear();
        table.destroy();
        $('.class_list_table').html(data);
        table = $("#restrict_class_list").DataTable();
    });
}
</script>