<?php
$action = isset($_POST['action'])?$_POST['action']:null;
$getHtmlContent = isset($_POST['HtmlContent'])?$_POST['HtmlContent']:null;

if(!empty($getHtmlContent) && $action == 'generatePdf') {
    $nowTime = time();
    $fileName = 'vendor_'.$nowTime.'.pdf';
    $filePath = 'vendor/vendorPdf/';
    $fileLocation = $filePath.$fileName;
    $ser=serialize($getHtmlContent);    # safe -- won't count the slash 
    $addslashesContent=addslashes($ser); 
    require("phpToPDF.php");
        // $pdf_options = array(
        //     "source_type" => 'html',
        //     "source" => $getHtmlContent,
        //     "action" => 'download',
        //     "file_name" => $fileLocation,
        //     "page_size" => 'A5');

        // $pdf_options = array(
        //     "source_type" => 'html',
        //     "source" => $getHtmlContent,
        //     "action" => 'view',
        //     "color" => 'monochrome',
        //     "page_orientation" => 'landscape');
    $pdf_options = array(
        "source_type" => 'html',
        "source" => $getHtmlContent,
        "action" => 'save',
        "save_directory" => $filePath,
        "file_name" => $fileName);

    // CALL THE phpToPDF FUNCTION WITH THE OPTIONS SET ABOVE
    phptopdf($pdf_options);

    $returnData = array('fileLocation'=>$fileLocation, 'htmlContent'=>$getHtmlContent);
    echo json_encode($returnData);

    exit();
} else if($action == 'deleteFile') {
    $file = isset($_POST['file'])?$_POST['file']:null;
    $return_text = 0;
    if(!empty($file)) {
        if( file_exists($file) ){
        unlink($file);
            $return_text = 1;
        } else {
            $return_text = 0;
        }
    }
    echo $return_text;
    exit();
}