<?php 
	include_once('elements/db_connection.php');
	include_once('elements/left_menu.php');
	include_once('elements/header.php');
?>
        <!-- /top navigation -->
<style>
#showStaffDetails {    
    margin-right: 15px;
    background-color: #fff;
    bottom: 0;
    clear: both;
    display: block;
    padding: 5px 0 0;
    position: fixed;
    right: 0;
    width: 80%;
    text-align: center;
    z-index: 10;
}
#closeStafeDetails {
    cursor: pointer;
    float: right;
    margin-right: -10px;
    margin-top: -20px;
    border: 1px solid #9e9e9e;
    padding: 0px 8px;
    font-weight: bold;
    font-size: 18px;
    border-radius: 20px;
    background-color: #b16565;
    color: #fff;
}
#closeStafeDetails:hover {
    box-shadow: 3px 4px 20px black;
    background-color: #d43f3f;
}
}
</style>
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_title">
                    <h2>Dealer Detail</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    </ul>
                    <div class="clearfix"></div>
              </div>
              <div class="x_content">
              <?php
                $user_id = $_REQUEST['user_id'];
                $sql = "SELECT users.id, users.user_name, users.email as personalEmail, user_business_details.businessName, user_business_details.streetAddres1, user_business_details.streetAddres2, user_business_details.state, user_business_details.city, user_business_details.zipCode, user_business_details.telephone, user_business_details.dealerType, user_business_details.fax, user_business_details.email, user_business_details.einTaxId, user_business_details.documents, user_business_details.isApproved, user_detail.email as user_email, user_detail.telephone as user_telephone, user_detail.street_address_1 as user_street_address_1, user_detail.street_address_2 as user_street_address_2, user_detail.state as user_state, user_detail.city as user_city, user_detail.zip_code as user_zip_code, user_detail.incentive FROM users LEFT JOIN user_business_details ON users.id = user_business_details.userId  LEFT JOIN user_detail ON users.id = user_detail.user_id WHERE users.id=$user_id ORDER BY users.id DESC";
                $result = mysqli_query($conn, $sql);
                while($row = mysqli_fetch_assoc($result)) {
                ?>
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Business Info</h2>
                        <span class="close" onclick="close_popup()">&times;</span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Dealer Type:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php 
                                if($row['dealerType'] == 'newVehicalsAndUsed') {
                                    echo 'New Vehicals & Used';
                                } else if($row['dealerType'] == 'usedOnly') {
                                    echo 'Used Only';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['businessName']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 1:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['streetAddres1']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 2:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['streetAddres2']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">City:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['city']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">State/Province:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['state']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Zip/Postal Code:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['zipCode']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['telephone']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['fax']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['email']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">EIN/Tax ID:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['einTaxId']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Document:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php if($row['documents'] != '' || $row['documents'] != null) {
                                    $document_name = explode('/', $row['documents']) ?>
                                    <a href="../dealer/db_validation/<?php echo $row['documents']?>" download><?php echo $document_name[1];?></a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="x_title">
                        <h2>Personal Info</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['user_name']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Id:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['personalEmail']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['user_telephone']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 1:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['user_street_address_1']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 2:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['user_street_address_2']?>
                            </div>
                        </div>
                        <div class="item form-group  col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">City:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['user_city']?>
                            </div>
                        </div>
                        <div class="item form-group  col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">State/Province:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['user_state']?>
                            </div>
                        </div>
                        <div class="item form-group  col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Zip/Postal Code:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['user_zip_code']?>
                            </div>
                        </div>
                        <div class="item form-group col-md-6">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Incentive: </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <?php echo $row['incentive']?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>
			  </div>
            </div>
        </div>        
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
        
