<?php 
include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
?>
<!-- /top navigation -->

<!-- page content -->
<div class="right_col" role="main">
	<div class="x_title">
		<h2>Add Apple To Apple Sub Category</h2>
		<ul class="nav navbar-right panel_toolbox">
		  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
		  </li>
		</ul>
		<div class="clearfix"></div>
	</div>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="save_a2a_sub_cat.php" method="post">
				
				<div class="item form-group">
					<label class="col-md-2">Category Name</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<select name="appleToAppleType" class="form-control">
						<?php 
                            $sql = "SELECT * FROM appletoappletype WHERE is_deleted='0'";
                            $result = mysqli_query($conn, $sql);
                            if (mysqli_num_rows($result) > 0) {
                                // output data of each row
                                while($row = mysqli_fetch_assoc($result)) {
                            ?>
							<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
						<?php } }?>
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="item form-group">
					<label class="col-md-2">Sub Category name</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" name="name" class="form-control" value="" placeholder="Sub Category Name">
					</div>
				</div> 
				<div class="clearfix"></div>
				
				<div class="form-group">
				  <div class="col-md-6 col-md-offset-3">
					<a href="apple_to_apple_sub_category.php" class="btn btn-danger">Cancel</a>
					<button id="send" type="submit" class="btn btn-success">Save</button>
				  </div>
				</div>             				
            </form>
        </div>
    </div>
</div>
<!-- footer content -->
<?php 
include_once('elements/footer.php');
?>
<!-- /footer content -->
