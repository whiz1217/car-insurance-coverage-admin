<?php 
  include_once('elements/db_connection.php');
  $cat = $_REQUEST['cat'];
  ?>
<select class="form-control vendor_id" id="vendor" name="vendor_id" required>
  <option value="">PLEASE SELECT A VENDOR</option>
  <?php
  $sql = "SELECT user_detail.company_name, users.id FROM users LEFT JOIN user_detail ON user_detail.user_id = users.id WHERE users.isDeleted = 0 AND users.user_role_id = 2 AND user_detail.category = '$cat' ORDER BY `id` DESC";
  $result = mysqli_query($conn, $sql);
  if (mysqli_num_rows($result) > 0) {
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
  ?>
  <option value="<?php echo $row['id']?>"><?php echo $row['company_name']?></option>
<?php 
  }
 } 
 ?>
</select>