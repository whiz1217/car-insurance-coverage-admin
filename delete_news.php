<?php
include_once('elements/db_connection.php');
$id = $_REQUEST['id'];
?>

  <div class="item form-group" style="height:30px; margin-top:3%; ;margin-bottom:5%;">
    <div class="col-md-12">
        <form action="news_delete.php"  method="post">
          <div class="col-md-6">
              <label class="control-label ">Are you sure to delete this News?</label>
              <input type="password" name="passwordToDelete" id="passwordToDelete" placeholder="Enter password to confirm" required>
          </div>
          
          <input type="hidden" name="id" value="<?php echo $id?>">
          <div class="col-md-6" style="float:right;">
            <div class="col-md-3" style="  margin-left:31%; ">
              <button type="submit" class="btn btn-primary">Delete</button>
            </div>
            <div class="col-md-3" style="  margin-left:10%; ">
              <button class="btn btn-danger" onclick="close_popup()">Cancel</button>
            </div>
          </div>
         
        </form>
      
    </div>
  </div>  