<?php 
include_once('elements/db_connection.php');
$error_message = '';
if(isset($_SESSION['carDealerZoneAdmin'])) {
	$carDealerZoneAdmin = json_decode($_SESSION['carDealerZoneAdmin']);
	//print_r($carDealerZoneAdmin);
	//exit();
	if(isset($carDealerZoneAdmin->id)) {
		echo "<script type='text/javascript'>  window.location='index.php'; </script>";
		exit();
	}
}
if(isset($_REQUEST['admin_login'])) {
	$user_name = $_REQUEST['user_name'];
	$password = $_REQUEST['password'];
	$sql = "SELECT * FROM users WHERE `user_name` = '".$user_name."' AND `password` = '".md5($password)."' AND `user_role_id`='1'";
	$result = mysqli_query($conn, $sql);
	
	if (mysqli_num_rows($result) > 0) {
		// output data of each row
		while($row = mysqli_fetch_assoc($result)) {
			$session_value = json_encode($row);
			$_SESSION["carDealerZoneAdmin"] = $session_value;
			echo "<script type='text/javascript'>  window.location='index.php'; </script>";
			exit();
		}
	} else {
	  $error_message = 'Wrong username or password';
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Admin login</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login" style="background: #2A3F54;">
    <div>
      <a class="hiddenanchor" id="signup"></a>
		<a class="hiddenanchor" id="signin"></a>
		
	  <style>
	  .login_content h1:before {
			background: #ff9933;
			text-shadow: none;
		}
	  .login_content h1:after {
			background: #ff9933;
			text-shadow: none;
		}
	  .login_content h1 {
			color: #ff9933;
			text-shadow: none;
		}
		.submit {
			background: #ff9933;
			color: #fff;
		}
		.submit:hover {
			background: #3399cc;
			color: #fff;
		}
	  </style>
      <div class="login_wrapper">
		<div style="position:absolute; margin-top: -75px;">
            <img class="website-logo" height="250" src="images/logo.svg" alt="logo" style="width: 350px;">
		</div>
        <div class="animate form login_form" style="top: 110px;">
          <section class="login_content">
            <form class="form-horizontal form-label-left" method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
              <h1>Admin login</h1>
              <span class="alert-danger"><?php echo $error_message;?></span>
              <div class="item">
                <input type="text" name="user_name" class="form-control" placeholder="Username" required />
              </div>
              <div class="item">
                <input type="password" name="password" class="form-control" placeholder="Password" required />
              </div>

              <div>
              <button type="submit" name="admin_login" class="btn btn-default submit">Log in</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator" style="border-top: 1px solid #ff9933;text-shadow: none;">
                <div>
                  <p style="color: #ff9933;text-shadow: none;">©2017 All Rights Reserved.Car Dealer Zone.</p>
                </div>
              </div>
            </form>
            </section>
        </div>
      </div>
    </div>
  </body>
</html>
