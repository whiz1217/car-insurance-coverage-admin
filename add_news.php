<?php 
	include_once('elements/db_connection.php');
	include_once('elements/left_menu.php');
	include_once('elements/header.php');
?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
				<div class="x_content">
					<form class="form-horizontal form-label-left" action="save_news.php" method="post">
						<div class="item form-group">
							<label class="col-md-2">Promo Codes</label>
							<div class="col-md-4 col-sm-4 col-xs-12">
							  <input type="text" name="promoCodes" class="form-control" placeholder="promo code1, promo code 2">
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="item form-group">
							<label class="col-md-2">News</label>
							<div class="col-md-4 col-sm-4 col-xs-12">
							  <textarea type="text" name="news" class="form-control" placeholder=""></textarea>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="item form-group">
							<label class="col-md-2">Status</label>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<select class="form-control" name="status">
									<option value="Active">Active</option>
									<option value="Inactive" selected>Inactive</option>
								</select>
							</div>
						</div>
						<div class="form-group">
						  <div class="col-md-6 col-md-offset-3">
							<a href="news_list.php" class="btn btn-danger">Cancel</a>
							<button id="send" type="submit" class="btn btn-success">Save</button>
						  </div>
						</div>               
					</form>
				</div>
		</div>
    </div>
    <span class="addRow" style="display:none"></span>
    <!-- footer content -->
    <?php 
    include_once('elements/footer.php');
    ?>
    <!-- /footer content -->
<script>
function show_vendor_by_category(e) {
    var cat = $(e.target).val();
    $.ajax({
      method: "POST",
      url: "vendor_list_filter_state_mapping.php",
      data: { cat : cat }
    }).done(function(data) {
      //console.log(data);
      $('.vendor_list_filter').html(data);
    });
}
</script>
