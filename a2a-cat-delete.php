<?php
  include_once('elements/db_connection.php');
  $id = $_REQUEST['id'];
?>

<div class="modal-content">
	<div class="x_panel">
        <div class="x_content">
			<form action="delete_a2a_cat.php"  method="post">
			  <div class="col-md-6">
				  <label class="control-label ">Are you sure to delete this Category?</label>
				  <input type="password" name="passwordToDelete" id="passwordToDelete" placeholder="Enter password to confirm" required>
			  </div>
			  
			  <input type="hidden" name="id" value="<?php echo $id?>">
			  <div class="col-md-6" style="float:right;">
				<div class="col-md-3" style="  margin-left:31%; ">
				  <input type="submit" class="btn btn-primary" value="Delete">
				</div>
				<div class="col-md-3" style="  margin-left:10%; ">
				  <button class="btn btn-danger" onclick="close_popup()">Cancel</button>
				</div>
			  </div>
			 
			</form>
      
		</div>
    </div>
  </div>  