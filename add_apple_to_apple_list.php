<?php 
	include_once('elements/db_connection.php');
	include_once('elements/left_menu.php');
	include_once('elements/header.php');
?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_title">
                <h2>Apples To Apples List</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="item form-group pull-right">
                    <select onchange="show_class_by_category(event)" class="form-control cat_id" style="width: 177px;">
                        <option value="">All Category</option>
                        <option value="VSC">VSC</option>
                        <option value="GAP">GAP</option>
                        <option value="ANCILLARY">ANCILLARY</option>
                        <option value="INSURANCE">INSURANCE</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="item form-group pull-right vendor_list_filter">
                    <select class="form-control" disabled style="width: 177px;">
                        <option value="">All Vendor</option>
                    </select>
                </div>
                <div class="clearfix"></div>
                <div class="class_list_table">
                <table id="restrict_class_list">
                  <thead>
                      <tr>
                          <th>Sl. no.</th>
                          <th>Category</th>
                          <th>Vendor Name</th>
                          <th>Plan Name</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php 
                        $sql = "SELECT user_detail.company_name, user_detail.category, plans.name, appletoappledata.id FROM appletoappledata LEFT JOIN user_detail ON appletoappledata.vendor_id = user_detail.user_id LEFT JOIN plans ON appletoappledata.plan_id = plans.id WHERE appletoappledata.isDeleted = 0";
                        $result = mysqli_query($conn, $sql);
                        $slNo = 1;
                        if (mysqli_num_rows($result) > 0) {
                            // output data of each row
                            while($row = mysqli_fetch_assoc($result)) {
                        ?>
                              <tr>
                                <td><?php echo $slNo;?></td>
                                <td><?php echo $row['category'];?></td>
                                <td><?php echo $row['company_name'];?></td>
                                <td><?php echo $row['name'];?></td>
                                <td>
                                <a class='btn btn-danger' onclick="delete_a2a(<?php echo $row['id'];?>)" style="cursor:pointer;">Delete</a>
                <a class='btn btn-primary' onclick="a_to_a_edit(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
                <a class='btn btn-primary' onclick="show_detail(<?php echo $row['id'];?>)" style="cursor:pointer;">View</a>
                                </td>
                              </tr>
                        <?php
                        $slNo++;
                            }
                        } else {
                          $error_message = 'Wrong username or password';
                        }
                      ?>
                  </tbody>
                </table>
              </div>
              </div>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content a2a-modal">
            Loading...
        </div>

        </div>
        <!-- /page content -->
        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
        <script>
         var table;
        $(document).ready(function(){
            table = $('#restrict_class_list').DataTable();
        });
        
        function delete_a2a(id) {
            $.ajax({
                method: "POST",
                url: "a2a-delete.php",
                data: { id : id }
            }).done(function(data) {
                $('.a2a-modal').html(data);
            });
            var modal = document.getElementById('myModal');
            modal.style.display = "block";
        }
        function show_detail(id) {
            $.ajax({
                method: "POST",
                url: "a2a-detail.php",
                data: { id : id }
            }).done(function(data) {
                $('.a2a-modal').html(data);
            });
            var modal = document.getElementById('myModal');
            modal.style.display = "block";
        }
        function a_to_a_edit(id) {
            $.ajax({
                method: "POST",
                url: "a_to_a_edit.php",
                data: { id : id }
            }).done(function(data) {
                $('.a2a-modal').html(data);
            });
            var modal = document.getElementById('myModal');
            modal.style.display = "block";
        }
        function close_popup() {
            $('.class-modal').html('Loading...');
            var modal = document.getElementById('myModal');
            modal.style.display = "none";
        }

        function show_class_by_category(e) {
            var val = $(e.target).val();
            $.ajax({
                method: "POST",
                url: "apple_to_apple_filtered_list.php",
                data: { cat : val }
            }).done(function(data) {
               
                table.clear();
                table.destroy();
                $('.class_list_table').html(data);
                table = $("#restrict_class_list").DataTable();
            });
            $.ajax({
                method: "POST",
                url: "class_vendor_option.php",
                data: { cat : val }
            }).done(function(data) {
                $('.vendor_list_filter').html(data);
            });
        }
        function show_class_by_vendor(e) {
        var cat = $('.cat_id').val();
        var val = $(e.target).val();
        $.ajax({
            method: "POST",
            url: "apple_to_apple_filtered_list.php",
            data: { cat : cat, id: val}
        }).done(function(data) {
            //console.log(data);
            table.clear();
            table.destroy();
            $('.class_list_table').html(data);
            table = $("#restrict_class_list").DataTable();
        });
        }
function show_class(e) {
  var val = $(e.target).val();
  $.ajax({
    method: "POST",
    url: "class_list.php",
    data: { id : val }
  }).done(function(data) {
    $('.restrict_class_d').html(data);
  });
}
$(document).on('click', '.tree_new label', function(e) {
  $(this).next('ul').fadeToggle();
  e.stopPropagation();
});
$(document).on('change', '.tree_new input[type=checkbox]', function(e) {
  $(this).siblings('ul').find("input[type='checkbox']").prop('checked', this.checked);
  //$(this).parentsUntil('.tree_new').children("input[type='checkbox']").prop('checked', this.checked);
  e.stopPropagation();
});
function fnShowLimitBox(prName) { 
  $('.'+prName+'Charge').show();
}
function fnShowHideInput(e, name) {
  if($(e.target).prop('checked') == true) {
    $('.'+name).show();
  } else {
    $('.'+name).hide();
  }
}
function fnHideLimitBox(prName) { 
  $('.'+prName+'Charge').hide();
}
function show_vendor_by_category(e) {
  var cat = $(e.target).val();
  $.ajax({
    method: "POST",
    url: "vendor_list_filter_a2a.php",
    data: { cat : cat }
  }).done(function(data) {
    //console.log(data);
    $('.vendor_list_filter').html(data);
  });
}
        </script>