<?php
include_once('elements/db_connection.php');
$user_id = $_REQUEST['user_id'];
$sql = "SELECT users.id, users.user_name, users.email as personalEmail, user_business_details.businessName, user_business_details.streetAddres1,user_business_details.streetAddres2, user_business_details.country_id, user_business_details.state, user_business_details.city, user_business_details.zipCode, user_business_details.telephone, user_business_details.fax, user_business_details.email, user_business_details.einTaxId, user_business_details.documents, user_business_details.isApproved, user_detail.email as user_email, user_detail.telephone as user_telephone, user_detail.street_address_1 as user_street_address_1, user_detail.street_address_2 as user_street_address_2, user_detail.country_id as user_country_id, user_detail.state as user_state, user_detail.city as user_city, user_detail.zip_code as user_zip_code,  user_detail.company_name as user_company_name, user_detail.incentive, user_detail.adminNotes FROM users LEFT JOIN user_business_details ON users.id = user_business_details.userId  LEFT JOIN user_detail ON users.id = user_detail.user_id WHERE users.id=$user_id ORDER BY users.id DESC";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
?>
<div class="x_panel">
    <div class="x_title">
        <h2>Business Info</h2>
        <span class="close" onclick="close_popup()">&times;</span>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <form class="form-horizontal form-label-left" action="update_dealer.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="user_id" value="<?php echo $row['id']?>">
   
        <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12"> Name <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input class="form-control col-md-7 col-xs-12" value="<?php echo $row['businessName']?>" name="company_name" placeholder="" required type="text">
            </div>
          </div>

          
          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 1 <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="street_address_1" required value="<?php echo $row['streetAddres1']?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 2</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="street_address_2" value="<?php echo $row['streetAddres2']?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          
          
          <div class="item form-group col-md-6">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Zip/Postal Code <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="zip_code" id="zip"  value="<?php echo $row['zipCode']?>"   onkeyup="get_city_business(event)" required class="zip_code form-control col-md-7 col-xs-12">
                </div>
           </div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12 city_vendor_add">
              <input type="text" name="city" placeholder="City" value="<?php echo $row['city']?>" required class="city form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group col-md-6">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">State/Province <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12 state_vendor_add">
              <input type="text" name="state" placeholder="State" value="<?php echo $row['state']?>" required class="state form-control col-md-7 col-xs-12"> 
            </div>
          </div>


          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="telephone" data-format="ddd-ddd-dddd" value="<?php echo $row['telephone']?>" required class="form-control col-md-7 col-xs-12 bfh-phone">
            </div>
          </div>   
          

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="fax_number" value="<?php echo $row['fax']?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          
           <div class="item form-group col-md-6">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="email" name="email" style="text-transform: none;" required value="<?php echo $row['email']?>" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            
             <div class="item form-group col-md-6">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">EIN/Tax ID<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="einTaxId" required value="<?php echo $row['einTaxId']?>" class="form-control col-md-7 col-xs-12">
                </div>
            </div>
            <div class="item form-group col-md-6">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" data-max-size="2048">Notes</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea name="adminNotes" class="form-control col-md-7 col-xs-12"><?php echo $row['adminNotes']?></textarea>
              <!-- Max upload size 10 MB -->
              </div>
            </div>
         

          <div class="clearfix"></div>
          <label class="col-md-12" for="name">Personal Info</label>
          <div class="ln_solid"></div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="personal_name" value="<?php echo $row['user_name']?>" required class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Address">Email Id <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="email" name="personal_email" style="text-transform: none;" value="<?php echo $row['personalEmail']?>" required class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="personal_telephone" data-format="ddd-ddd-dddd" value="<?php echo $row['user_telephone']?>" required class="form-control col-md-7 col-xs-12 bfh-phone">
            </div>
          </div>
          
          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 1 <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="user_street_address_1" required value="<?php echo $row['user_street_address_1']?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 2</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
            <input type="text" name="user_street_address_2" value="<?php echo $row['user_street_address_2']?>" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          
          
          <div class="item form-group col-md-6">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Zip/Postal Code <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="user_zip_code" id="zip"  value="<?php echo $row['user_zip_code']?>" onkeyup="get_city_personal(event)" required class="zip_code form-control col-md-7 col-xs-12">
                </div>
           </div>

          <div class="item form-group col-md-6">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12 city_vendor_add">
              <input type="text" name="user_city" placeholder="City" value="<?php echo $row['user_city']?>" required class="city_personal form-control col-md-7 col-xs-12">
            </div>
          </div>

          <div class="form-group col-md-6">
          <label class="control-label col-md-3 col-sm-3 col-xs-12">State/Province <span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12 state_vendor_add">
              <input type="text" name="user_state" placeholder="State" value="<?php echo $row['user_state']?>" required class="state_personal form-control col-md-7 col-xs-12"> 
            </div>
          </div>
          <div class="form-group col-md-6" style="display:none">
          <label class="control-label col-md-3 col-sm-3 col-xs-12"><input type="checkbox" onclick="showIncentive(event)" <?php if($row['incentive'] != null){ echo 'checked';}?>> Incentive</label>
            <div class="col-md-6 col-sm-6 col-xs-12 state_vendor_add">
              <input type="text" name="incentive" value="<?php echo $row['incentive']?>" class="incentive form-control col-md-7 col-xs-12"  <?php if($row['incentive'] == null){ echo 'disabled';}?>> 
            </div>
          </div>


     
          <div class="clearfix"></div>
          <div class="ln_solid"></div>
          <div class="form-group row">
            <div class="col-md-6 col-md-offset-3">
              <a class="btn btn-danger" onclick="close_popup()">Cancel</a>
              <button id="send" type="submit" class="btn btn-success">Update</button>
            </div>
          </div>
        </form>
  </div>
</div>
<?php
}
?>

