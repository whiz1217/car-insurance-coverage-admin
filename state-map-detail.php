<div class="modal-content">
	<div class="x_panel">
		<?php 
		include_once('elements/db_connection.php');
		$id = $_REQUEST['id'];
		$sqlD = "SELECT id FROM vendor_state_mapping WHERE vendor_state_mapping.vendor_id = $id AND vendor_state_mapping.isDeleted = 0 AND vendor_state_mapping.is_featured = 'no' AND vendor_state_mapping.dealer_id IS NOT NULL";
		$sqlS = "SELECT id FROM vendor_state_mapping WHERE vendor_state_mapping.vendor_id = $id AND vendor_state_mapping.isDeleted = 0 AND vendor_state_mapping.is_featured = 'no' GROUP BY vendor_state_mapping.states";
		$resultD = mysqli_query($conn, $sqlD);
		$resultS = mysqli_query($conn, $sqlS); 
		?>
		<span class="close" onclick="close_popup()">&times;</span>
		<div class="x_content">
			<div class="item form-group col-md-6">
				<label class="control-label col-md-5 col-sm-5 col-xs-12">State Mapping Info:</label>
				<div class="col-md-6 col-sm-6 col-xs-12">
				Total State: <?php echo mysqli_num_rows($resultS);?> <br />
				Total Dealer: <?php echo mysqli_num_rows($resultD);?>
				</div>
			</div>
		<?php
		$sqlUser = "SELECT * FROM user_detail WHERE user_id = $id";
		$resultUser = mysqli_query($conn, $sqlUser);
		while($rowUser = mysqli_fetch_assoc($resultUser)) {
		?>
            <div class="item form-group col-md-6">
                <label class="control-label col-md-5 col-sm-5 col-xs-12">Vendor Company Name:</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <img class="media-object logo" style=" -webkit-print-color-adjust: exact;" src="<?php echo $rowUser['user_document']?>" width="100"/><br />
                    <?php echo $rowUser['company_name'];?><br />
                    <?php echo $rowUser['website_url'];?><br />
                    <?php echo $rowUser['street_address_1']?>, <?php echo $rowUser['street_address_2']?><br />
                    <?php echo $rowUser['city_name'];?>, <?php echo $rowUser['state_name'];?> - <?php echo $rowUser['zip_code']?><br />
                    Telephone: <?php echo $rowUser['telephone']?>, Fax: <?php echo $rowUser['fax_number']?><br />
                    <?php echo $rowUser['email'];?><br />
                </div>
            </div>
        </div>
	</div>
	<?php 
	}
	$sql = "SELECT user_detail.company_name, user_detail.category, vendor_state_mapping.id, vendor_state_mapping.vendor_id, vendor_state_mapping.states FROM vendor_state_mapping LEFT JOIN user_detail ON vendor_state_mapping.vendor_id = user_detail.user_id WHERE vendor_state_mapping.vendor_id = $id AND vendor_state_mapping.is_featured = 'no' GROUP BY states";
	$result = mysqli_query($conn, $sql);
	if (mysqli_num_rows($result) > 0) {
		// output data of each row
		while($row = mysqli_fetch_assoc($result)) {
	?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">State: <?php echo $row['states']?></h3>
		</div>
		<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<?php $vendor_id = $row['vendor_id'];
					$states = $row['states'];
					$sqlD = "SELECT user_business_details.businessName FROM vendor_state_mapping LEFT JOIN user_business_details ON vendor_state_mapping.dealer_id = user_business_details.userId WHERE vendor_state_mapping.vendor_id = $vendor_id AND vendor_state_mapping.states = '$states' AND vendor_state_mapping.isDeleted = 0 AND vendor_state_mapping.is_featured = 'no' AND vendor_state_mapping.dealer_id IS NOT NULL";
					$resultD = mysqli_query($conn, $sqlD); ?>
					<th class="text-center colfix">Dealer Names <span style="float: right;">Total Dealer: <?php echo mysqli_num_rows($resultD);?></span></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
					<?php
					$sqlD = "SELECT user_business_details.businessName, user_business_details.userId FROM vendor_state_mapping LEFT JOIN user_business_details ON vendor_state_mapping.dealer_id = user_business_details.userId WHERE vendor_state_mapping.vendor_id = $vendor_id AND vendor_state_mapping.states = '$states' AND vendor_state_mapping.isDeleted = 0 AND vendor_state_mapping.is_featured = 'no'";
					$resultD = mysqli_query($conn, $sqlD);
					if (mysqli_num_rows($resultD) > 0) {
						// output data of each row
						while($rowD = mysqli_fetch_assoc($resultD)) {
							echo "<a href='dealer_detail.php?user_id=".$rowD['userId']."'>".$rowD['businessName'].'</a>, ';
						}
					}
					?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php 
	} }
	?>
</div>