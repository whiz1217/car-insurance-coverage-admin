<table id="restrict_class_list">
  <thead>
      <tr>
          <th>Sl. no.</th>
          <th>Name</th>
          <th>Action</th>
      </tr>
  </thead>
  <tbody class"vendor_list_body">
    <?php 
    include_once('elements/db_connection.php');
    $cat = $_REQUEST['cat'];
    if($cat == '') {
      $sql = "SELECT users.id, user_business_details.businessName, user_business_details.isApproved FROM users LEFT JOIN user_business_details ON users.id = user_business_details.userId WHERE users.user_role_id=3 AND users.isDeleted=0 ORDER BY users.id DESC";
    } else {
      $sql = "SELECT users.id, user_business_details.businessName, user_business_details.isApproved FROM users LEFT JOIN user_business_details ON users.id = user_business_details.userId WHERE user_business_details.isApproved= '$cat' and users.user_role_id=3 AND users.isDeleted=0 ORDER BY users.id DESC";
    }
    $result = mysqli_query($conn, $sql);
    $slNo = 1;
    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
    ?>
    <tr>
        <td><?php echo $slNo;?></td>
        <td><?php echo $row['businessName'];?></td>
        <td>
        <a class='btn btn-danger' onclick="delete_dealer(<?php echo $row['id'];?>)" style="cursor:pointer;">Delete</a>
        <a class='btn btn-primary' onclick="edit_detail(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
          <a class='btn btn-primary' onclick="show_detail_dealer(<?php echo $row['id'];?>)" style="cursor:pointer;">View</a>
          <a class='btn btn-primary' onclick="approve_disapprove_dealer(<?php echo $row['id'];?>, '<?php echo $row['isApproved'];?>')" style="cursor:pointer;">
          <?php
          if($row['isApproved'] == 'yes') {
              echo ' Click to disapprove';
          } else {
            echo ' Click to approve';
          }
          ?>
          </a>
          <a class='btn btn-success' onclick="show_staff_incentive(<?php echo $row['id'];?>)" style="cursor:pointer;">Staff Incentive</a>
        </td>
    </tr>
<?php
$slNo++;
    }
} else {
  $error_message = 'Wrong username or password';
}
?>
</tbody>
</table>