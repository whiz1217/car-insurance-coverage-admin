
    <span class="close" onclick="close_popup()">&times;</span>
    <?php
    include_once('elements/db_connection.php');
    $id = $_REQUEST['id'];
    $sql = "SELECT user_detail.company_name, user_detail.user_document, user_detail.website_url, user_detail.street_address_1, user_detail.street_address_2, user_detail.city_name, user_detail.state_name, user_detail.zip_code, user_detail.telephone, user_detail.fax_number, user_detail.email, user_detail.category, plans.name FROM appletoappledata LEFT JOIN user_detail ON appletoappledata.vendor_id = user_detail.user_id  LEFT JOIN plans ON appletoappledata.plan_id = plans.id WHERE appletoappledata.id = $id";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
    ?>
    <div class="x_panel">
    <div class="x_content">
        <div class="item form-group col-md-6">
            <label class="control-label col-md-5 col-sm-5 col-xs-12">Plan Name:</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $row['name']?>
            </div>
        </div>
        <div class="item form-group col-md-6">
            <label class="control-label col-md-5 col-sm-5 col-xs-12">Vendor Company Name:</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <img class="media-object logo" style=" -webkit-print-color-adjust: exact;" src="<?php echo $row['user_document']?>" width="100"/><br />
                <?php echo $row['company_name'];?><br />
                <?php echo $row['website_url'];?><br />
                <?php echo $row['street_address_1']?>, <?php echo $row['street_address_2']?><br />
                <?php echo $row['city_name'];?>, <?php echo $row['state_name'];?> - <?php echo $row['zip_code']?><br />
                Telephone: <?php echo $row['telephone']?>, Fax: <?php echo $row['fax_number']?><br />
                <?php echo $row['email'];?><br />
            </div>
        </div>
    </div>
        <table class="table table-bordered table-condensed">
            <thead>
            <tr>
                <th class="text-center colfix"></th>
                <th class="text-center colfix">Components Covered: (total no. of selected items) </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sqlE = "SELECT appletoappletype.name, appletoappletype.id FROM a2a_type_data LEFT JOIN appletoappletype ON a2a_type_data.a2a_type_id = appletoappletype.id WHERE a2a_type_data.appletoappledata_id = $id";
            $resultE = mysqli_query($conn, $sqlE);
            // output data of each row
            while($rowE = mysqli_fetch_assoc($resultE)) {
            ?>
                <tr>
                    <td>
                    <?php
                        if(file_exists('images/a2a/' . $rowE['name'] . '.png')) {
                          ?>
                          <img src="<?php echo 'images/a2a/' . $rowE['name'] . '.png';?>">
                          <?php
                        } else {
                          echo $rowE['name'];
                        }
                      ?>
                    </td>
                    <td>
                    <?php
                        $val_id = $rowE['id'];
                        $sqlV = "SELECT appletoapplevalue.name, a2a_value_data.value_comment FROM a2a_value_data LEFT JOIN appletoapplevalue ON a2a_value_data.a2a_value_id = appletoapplevalue.id WHERE appletoapplevalue.appleToAppleType = $val_id";
                        $resultV = mysqli_query($conn, $sqlV);
                        // output data of each row
                        if (mysqli_num_rows($resultV) > 0) {
                            while($rowV = mysqli_fetch_assoc($resultV)) {
                                echo $rowV['name'];
                                if($rowV['value_comment'] != null && $rowV['value_comment'] != '')
                                echo ': ' . $rowV['value_comment'];
                                echo ', ';
                            } 
                        } 
                    ?>
                    </td>
                </tr>
                    <?php
                        }
                    ?>
            </tbody>
        </table>
    </div>
    <?php 
        $sqlH = "SELECT * FROM highlight_coverage WHERE appletoappledata_id = $id";
        $resultH = mysqli_query($conn, $sqlH);
        // output data of each row
        if (mysqli_num_rows($resultH) > 0) {
        while($rowH = mysqli_fetch_assoc($resultH)) {
        ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Highlight Coverage</h3>
        </div>
        
        <div class="item form-group col-md-12">
            <label class="col-md-2">Labour Ready</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <?php if($rowH['labourReady'] == 'limit') {
                    echo 'Limit: ' . $rowH['labourReadyCharge'];
                } else if($rowH['labourReady'] == 'noLimit') { 
                    echo 'No Limit';
                } ?>
            </div>
        </div>

        <div class="item form-group col-md-12">
            <label class="col-md-2">Wheels</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <?php if($rowH['wheels'] == 'limit') {
                    echo 'Limit: ' . $rowH['wheelsCharge'];
                } else if($rowH['wheels'] == 'noLimit') { 
                    echo 'No Limit';
                } ?>
            </div>
        </div>

        <div class="item form-group col-md-12">
            <label class="col-md-2">Tiers</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <?php if($rowH['tiers'] == 'limit') {
                    echo 'Limit: ' . $rowH['tiersCharge'];
                } else if($rowH['tiers'] == 'noLimit') { 
                    echo 'No Limit';
                } ?>
            </div>
        </div>

        <div class="item form-group col-md-12">
            <label class="col-md-2">Road Hazard</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <?php if($rowH['roadHazard'] == 'limit') {
                    echo 'Limit: ' . $rowH['roadHazardCharge'];
                } else if($rowH['roadHazard'] == 'noLimit') { 
                    echo 'No Limit';
                } ?>
            </div>
        </div>

        <div class="item form-group col-md-12">
            <label class="col-md-2">Cosmetic Wheel Repair</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <?php if($rowH['cosmeticWheelRepair'] == 'limit') {
                    echo 'Limit: ' . $rowH['cosmeticWheelRepairCharge'];
                } else if($rowH['cosmeticWheelRepair'] == 'noLimit') { 
                    echo 'No Limit';
                } ?>
            </div>
        </div>

        <div class="item form-group col-md-12">
            <label class="col-md-2">Curb Impact Repair & Replacement</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <?php if($rowH['curbImpactRepair'] == 'limit') {
                    echo 'Limit: ' . $rowH['curbImpactRepairCharge'];
                } else if($rowH['curbImpactRepair'] == 'noLimit') { 
                    echo 'No Limit';
                } ?>
            </div>
        </div>

        <div class="item form-group col-md-12">
            <label class="col-md-2">Paintless Dent Repair & Ding</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <?php if($rowH['paintlessDentRepair'] == 'limit') {
                    echo 'Limit: ' . $rowH['paintlessDentRepairCharge'];
                } else if($rowH['paintlessDentRepair'] == 'noLimit') { 
                    echo 'No Limit';
                } ?>
            </div>
        </div>

        <div class="item form-group col-md-12">
            <label class="col-md-2">Rental</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
            <?php 
            if($rowH['rental'] == 'yes') {
                echo $rowH['rentalCharge']; 
            } else {
                echo 'Not offer with ths policy.';
            }
            ?>
            </div>
        </div>
        <div class="item form-group col-md-12">
            <label class="col-md-2">Road Side Assistance</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
            <?php 
            if($rowH['roadSideAssitance'] == 'yes') {
                echo $rowH['roadSideAssitanceCharge']; 
            } else {
                echo 'Not offer with ths policy.';
            }
            ?>
            </div>
        </div>
        <div class="item form-group col-md-12">
            <label class="col-md-2">Toing</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
            <?php 
            if($rowH['toing'] == 'yes') {
                echo $rowH['toingCharge']; 
            } else {
                echo 'Not offer with ths policy.';
            }
            ?>
            </div>
        </div>
        <div class="item form-group col-md-12">
            <label class="col-md-2">Other Highlight</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
            <?php 
            if($rowH['otherHighlight'] == 'yes') {
                echo $rowH['otherHighlightCharge']; 
            } else {
                echo 'Not offer with ths policy.';
            }
            ?>
            </div>
        </div>
        <div class="item form-group col-md-12">
            <label class="col-md-2">Tier & Wheel</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
            <?php 
            if($rowH['tierWheel'] == 'yes') {
                echo $rowH['tierWheelCharge']; 
            } else {
                echo 'Not offer with ths policy.';
            }
            ?>
            </div>
        </div>
        <div class="item form-group col-md-12">
            <label class="col-md-2">Windshield Chip</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
            <?php 
            if($rowH['windshildChip'] == 'yes') {
                echo $rowH['windshildChipCharge']; 
            } else {
                echo 'Not offer with ths policy.';
            }
            ?>
            </div>
        </div>
        <div class="item form-group col-md-12">
            <label class="col-md-2">Windshield Protection</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
            <?php 
            if($rowH['windshildProtection'] == 'yes') {
                echo $rowH['windshildProtectionCharge']; 
            } else {
                echo 'Not offer with ths policy.';
            }
            ?>
            </div>
        </div>
        <div class="item form-group col-md-12">
            <label class="col-md-2">Key Replacement</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
            <?php 
            if($rowH['keyReplacement'] == 'yes') {
                echo $rowH['keyReplacementCharge']; 
            } else {
                echo 'Not offer with ths policy.';
            }
            ?>
            </div>
        </div>
        <div class="item form-group col-md-12">
            <label class="col-md-2">Lease End Protection</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
            <?php 
            if($rowH['leaseEndProtection'] == 'yes') {
                echo $rowH['leaseEndProtectionCharge']; 
            } else {
                echo 'Not offer with ths policy.';
            }
            ?>
            </div>
        </div>
        <div class="item form-group col-md-12">
            <label class="col-md-2">Store Type</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
            <?php echo $rowH['storeType']; ?>
            </div>
        </div>
        <div class="item form-group col-md-12">
            <label class="col-md-2">Effective Day</label>
            <div class="col-md-4 col-sm-6 col-xs-12">
            <?php echo $rowH['effectiveDay']; ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <?php } } } } ?>