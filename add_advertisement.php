<?php 
include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_content">
              <form class="form-horizontal form-label-left" action="save_advertisement.php" method="post" enctype="multipart/form-data">
					<div class="item form-group">
						<label class="col-md-2">Category <span class="required">*</span></label>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<select onchange="show_vendor_by_category(event)" class="form-control cat_id" required>
									<option value="">PLEASE SELECT A CATEGORY</option>
									<option value="VSC">VSC</option>
									<option value="GAP">GAP</option>
									<option value="ANCILLARY">ANCILLARY</option>
									<option value="INSURANCE">INSURANCE</option>
								</select>
						</div>
					</div>

					<div class="item form-group">
						<label class="col-md-2">Choose vendor <span class="required">*</span></label>
						<div class="col-md-4 col-sm-6 col-xs-12 vendor_list_filter">
							<select class="form-control vendor_id" disabled required>
								<option value="">PLEASE SELECT A VENDOR</option>
							</select>
						</div>
					</div>
					
					<div class="item form-group">
						<label class="col-md-2">Advertisement Link <span class="required">*</span></label>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<input type="text" class="form-control" name="link" required>
						</div>
					</div>
					
					<div class="item form-group">
						<label class="col-md-2">Upload Advertisement <span class="required">*</span></label>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<input type="file" id="broacherDocument" name="broacherDocument[]" class="form-control col-md-7 col-xs-12" multiple>
						  <a class="form-control btn btn-primary" type="button" value="Preview" onclick="PreviewImage('broacherDocument');">Preview</a>
						</div>
					</div>
					
					<div class="clearfix"></div>
            </div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <a href="advertisement_list.php" class="btn btn-danger">Cancel</a>
                <button id="send" type="submit" class="btn btn-success">Save</button>
              </div>
            </div>               
          </form>
        </div>
      </div>
    </div>
    <span class="addRow" style="display:none"></span>
	<!-- The Modal -->
        <div id="myModal" class="modal">
            <div class="modal-content">
			<span class="close" onclick="close_popup()">×</span>
                <div style="clear:both">
				   <iframe id="viewer" frameborder="0" scrolling="no" style="width: 100%; height:1000px;"></iframe>
				</div>
            </div>
        </div>
    <!-- footer content -->
    <?php 
    include_once('elements/footer.php');
    ?>
    <!-- /footer content -->
<script>
function PreviewImage(prId) {
	var modal = document.getElementById('myModal');
	modal.style.display = "block";
	pdffile=document.getElementById(prId).files[0];
	pdffile_url=URL.createObjectURL(pdffile);
	$('#viewer').attr('src',pdffile_url);
}
function close_popup() {
		var modal = document.getElementById('myModal');
		modal.style.display = "none";
	}
function show_vendor_by_category(e) {
    var cat = $(e.target).val();
    $.ajax({
      method: "POST",
      url: "vendor_list_filter_state_mapping.php",
      data: { cat : cat }
    }).done(function(data) {
      //console.log(data);
      $('.vendor_list_filter').html(data);
    });
}
</script>
