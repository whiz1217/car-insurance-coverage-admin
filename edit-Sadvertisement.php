<!-- Modal content -->
<div class="modal-content">
<?php
include_once('elements/db_connection.php');
$id = $_REQUEST['id'];
$sql = "SELECT user_detail.company_name, user_detail.category, vendor_advertisement.id, vendor_advertisement.vendor_id, vendor_advertisement.link FROM vendor_advertisement LEFT JOIN user_detail ON vendor_advertisement.vendor_id = user_detail.user_id WHERE vendor_advertisement.id = $id";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
    $user_id = $row['vendor_id'];
    $cat = $row['category'];
?>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="update_Sadvertisement.php" method="post">
                <div class="item form-group">
                    <label class="col-md-2">Category <span class="required">*</span></label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <select onchange="show_vendor_by_category(event)" class="form-control cat_id" required>
                            <option value="">PLEASE SELECT A CATEGORY</option>
                            <option value="VSC" <?php if($cat == 'VSC'){ echo "selected";}?>>VSC</option>
                            <option value="GAP" <?php if($cat == 'GAP'){ echo "selected";}?>>GAP</option>
                            <option value="ANCILLARY" <?php if($cat == 'ANCILLARY'){ echo "selected";}?>>ANCILLARY</option>
                            <option value="INSURANCE" <?php if($cat == 'INSURANCE'){ echo "selected";}?>>INSURANCE</option>
                        </select>
                    </div>
                </div>
                <div class="item form-group">
					<input type="hidden" name="old_vendor_id" value="<?php echo $row['vendor_id'];?>">
                    <label class="col-md-2">Choose Vendor *</label>
                    <div class="col-md-4 col-sm-6 col-xs-12 vendor_list_filter">        
                        <?php 
                            $sqlV = "SELECT users.id, user_detail.company_name FROM users 
                            LEFT JOIN user_detail ON users.id = user_detail.user_id 
                            WHERE users.user_role_id='2' AND user_detail.category = '$cat' ORDER BY users.id DESC";
                            $resultV = mysqli_query($conn, $sqlV);
                        ?>
                        <select name="vendor_id" class="form-control vendor_id " required>
                            <option value="">PLEASE SELECT A VENDOR</option>
                            <?php 
                                while($rowV = mysqli_fetch_assoc($resultV)) {
                                $sqlInner = "SELECT users.id, user_detail.company_name FROM users 
                                LEFT JOIN user_detail ON users.id = user_detail.user_id 
                                WHERE users.id=".$user_id.";";
                                $resultVendor = mysqli_query($conn, $sqlInner);
                                $isSelected = '';
                                while($innerRow = mysqli_fetch_assoc($resultVendor))
                                {
                                    $isSelected = $innerRow['id'];
                                }
                                ?>
                                    <option value="<?php echo $rowV['id'];?>" <?php if($isSelected == $rowV['id']){echo 'selected';} ?>><?php echo $rowV['company_name'];?></option>
                                <?php       
                                    }
                                ?>
                        </select>
                    </div>
                </div>
				<div class="item form-group">
					<label class="col-md-2">Advertisement Link <span class="required">*</span></label>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
						<input class="form-control" type="text" name="link" value="<?php echo $row['link'];?>" required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6 col-md-offset-3">
					<a href="advertisement_list.php" class="btn btn-danger">Cancel</a>
					<button id="send" type="submit" class="btn btn-success">Save</button>
					</div>
				</div>               
            </form>
        </div>
    </div>
<?php } ?>   
</div>

<script>
function show_vendor_by_category(e) {
  var cat = $(e.target).val();
  $.ajax({
    method: "POST",
    url: "vendor_list_filter.php",
    data: { cat : cat }
  }).done(function(data) {
    //console.log(data);
    $('.vendor_list_filter').html(data);
  });
}
</script>
