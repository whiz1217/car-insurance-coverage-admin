<?php 
include_once('elements/db_connection.php');
?>
<!-- page content -->
    <style>
        #plan-tBody input.dtElement{
        width: 100%;
        }
        #displayMessage {
        float: right;
        }
        .error {
        color:red;
        }
        .success {
        color: #07e00f;
        }
        .editPanel footer {
            padding-bottom: 0px !important;
        }
        td .dtElement {
                color: black;
        }
    </style>
    <div class="x_panel" style="padding: 0px;margin: 0px;">
        <div class="x_title">
            <h2>Edit Plan</h2>
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <form class="form-horizontal form-label-left" action="plan_query.php" method="post">
            <input type="text" name="api" style="display:none" value="editPlanData">    
        
                <div class="item form-group">
                    <label class="col-md-2" >Plan Name <span class="required">*</span></label>
                    <div class="col-md-4">
                        <?php
                            $id = $_REQUEST['id'];
                            $sql = "SELECT * FROM plans WHERE id = $id";
                            $result = mysqli_query($conn, $sql);
                            while($row = mysqli_fetch_assoc($result)) {
                        ?>
                            <input id="name" class="form-control col-md-7 col-xs-12" placeholder="Enter plan name" name="PlanName" required type="text" value="<?php echo $row['name']?>">
                            <input type="text" name="PlanID" style="display:none" value="<?php echo $row['id']?>">
                        <?php 
                            }
                        ?>
                    </div>
                </div>
				<div class="ln_solid"></div>
				<div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    <div class="col-md-3 col-sm-6 col-xs-12">
						<?php
                        $id = $_REQUEST['id'];
                        $sql = "SELECT user_detail.user_id FROM plan_terms LEFT JOIN user_detail ON plan_terms.vendorId = user_detail.user_id WHERE plan_terms.plansId = $id AND plan_terms.isDeleted = 0 GROUP BY plan_terms.vendorId";
                        $result = mysqli_query($conn, $sql);
                        while($row = mysqli_fetch_assoc($result)) {
							$vendor_id =  $row['user_id'];
							$sqlUser = "SELECT * FROM user_detail WHERE user_id = $vendor_id";
							$resultUser = mysqli_query($conn, $sqlUser);
							while($rowUser = mysqli_fetch_assoc($resultUser)) {
						?>
								<select onchange="show_class_by_vendor_edit(event)" class="form-control cat_id" required>
									<option value="">Select a category</option>
									<option value="VSC" <?php if($rowUser['category'] == 'VSC') echo "selected" ?>>VSC</option>
									<option value="GAP" <?php if($rowUser['category'] == 'GAP') echo "selected" ?>>GAP</option>
									<option value="ANCILLARY" <?php if($rowUser['category'] == 'ANCILLARY') echo "selected" ?>>ANCILLARY</option>
									<option value="INSURANCE" <?php if($rowUser['category'] == 'INSURANCE') echo "selected" ?>>INSURANCE</option>
								</select>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12 vendor_list_filter">
                        <?php 
                            $cat = $rowUser['category'];
                        ?>

                        <select onchange="show_class_by_vendor(event)" class="form-control" id="vendor" name="vendor_id" required>
                        <?php
                         $vendorSelectedOption = '<option value="" disabled>Select a '.$cat.' Vendor</option>';
                        $sql = "SELECT user_detail.company_name, users.id FROM users LEFT JOIN user_detail ON user_detail.user_id = users.id WHERE users.isDeleted = 0 AND users.user_role_id = 2 AND user_detail.category = '$cat' ORDER BY `id` DESC";
                        $result = mysqli_query($conn, $sql);
                        if (mysqli_num_rows($result) > 0) {
                            while($row = mysqli_fetch_assoc($result)) {
                                $vendorSelectedOption .= '<option value="'.$row['id'].'"';
                                if($row['id'] == $vendor_id) {
                                    $vendorSelectedOption .= ' selected>' . $row['company_name'] . '</option>';
                                } else {
                                    $vendorSelectedOption .= '>' . $row['company_name'] . '</option>';
                                }
                            }
                            echo $vendorSelectedOption;
                        } 
                    }
                }
                        ?>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                    <div class="ln_solid"></div>
					<?php if($cat == 'VSC') { ?>
						<div class="item form-group vsc_category">
							<div id="addTableFunction">
								<label class="col-md-2">
								Add More Row                       
								</label>
								<div class="col-md-10">
								<a class="fa fa-plus addRow" style="cursor:pointer; color:blue;"></a> | <a id="copySelect" style="cursor:pointer; color:blue;">Copy Selected</a> | <a id="copyAllTr" style="cursor:pointer; color:blue;">Copy All</a> | <a id="deleteAllTr" style="cursor:pointer; color:blue;">Delete Selected</a>
								</div>
								<span id="displayMessage" style="display:none"></span>
							
								<table id="add_plan" style="width: 100%; margin: 0 0;">
									<thead>
										<tr>
											<th>Term:</th>
											<th>Class</th>
											<th>Cover Mileage</th>
											<th>Up To Mileage</th>
											<th>Manufacturer Warranty</th>
											<th>Effective Day</th>
											<th>Mileage From</th>
											<th>Mileage To</th>
											<th>Price ($)</th>
											<th>Deductible</th>
										</tr>
									</thead>
									<tbody id="plan-tBody">
										<?php 
										$sqlCMap = "SELECT restrict_class.name, plan_terms.classId FROM plan_terms LEFT JOIN restrict_class ON plan_terms.classId = restrict_class.id WHERE plan_terms.plansId = $id AND plan_terms.vendorId = $vendor_id GROUP BY plan_terms.classId";
										$resultCMap = mysqli_query($conn, $sqlCMap);
										while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
											$classId = $rowCMap['classId'];
											$sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $id AND  vendorId = $vendor_id AND classId = $classId AND isDeleted=0";

											$resultPlan = mysqli_query($conn, $sqlPlan);
											$slNo = 1;
											if (mysqli_num_rows($resultPlan) > 0) {
												// output data of each row
												$i = 0;
												while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
										?>
										<tr>
											<td>
												<input type="checkbox" class="copyRow">
												<input type="text" name="planTermsID[]" style="display:none" value="<?php echo $rowPlan['id'];?>">
												
												<input type="number" class="dtElement" placeholder="" name="term[]" style="width: 40%;" value="<?php echo $rowPlan['termNo'];?>">
												<span class="tremType"><?php echo $rowPlan['TermSelect'];?></span>
											</td>
											<td> 
												<select id="class" class="dtElement" name="class[]" required>
													<?php 
														$sql = "SELECT id,name as class FROM restrict_class WHERE vendor_id=$vendor_id";
														$result = mysqli_query($conn, $sql);
													?>
													<option value="" disabled>Select Class</option>
													<?php 
														while($row = mysqli_fetch_assoc($result)) {
															if($row['class'] != null) {
																if($row['id'] == $classId){
																	?>
																	<option value="<?php echo $row['id'];?>" selected><?php echo $row['class'];?></option>
																	<?php
																} else {
														?>
															<option value="<?php echo $row['id'];?>"><?php echo $row['class'];?></option>
														<?php 
																} 
															}     
														}
													?>

												</select>
											</td>
											<td><input type="number" class="dtElement" style="width: 90%;"  placeholder="Cover milage" name="coverMailage[]" value="<?php echo $rowPlan['coverMailage'];?>"></td>
											<td><input type="number" class="dtElement" style="width: 90%;"  placeholder="Up to milage" name="upToMailage[]" value="<?php echo $rowPlan['upToMailage'];?>"></td>
											<td><input type="checkbox" class="dtElement" name="manufacturerWarranty[<?php echo $i;?>]" <?php if($rowPlan['manufacturerWarranty'] == 'yes') { echo 'checked';}?>></td>
											<td><input type="number" class="dtElement" style="width: 90%;"  placeholder="Effective Day" name="effectiveDay[]" value="<?php echo $rowPlan['effectiveDay'];?>"></td>
											<td><input type="number" class="dtElement" style="width: 90%;"  placeholder="Milage From" name="mailageFrom[]" value="<?php echo $rowPlan['milage_from'];?>"></td>
											<td><input type="number" class="dtElement" style="width: 90%;" placeholder="Milage To" name="mailageTo[]" value="<?php echo $rowPlan['milage_to'];?>"></td>
											<td><?php echo $rowPlan['price_unit'];?> 
											<input type="number" class="dtElement" style="width: 80%;" placeholder="Price" name="price[]" value="<?php echo $rowPlan['price'];?>"></td>
											<td><?php echo $rowPlan['price_unit'];?> 
											<input type="number" class="dtElement" style="width: 80%;" placeholder="deductable" name="deductable[]" value="<?php echo $rowPlan['deductable'];?>"></td>
										</tr>
										<?php
													$slNo++;
													$i++;
												}
											} else {
												$error_message = 'Nothing found!';
											}
										}
										?>
									</tbody>
								</table> 
								<div class="ln_solid"></div>
								<div class="form-group row" style="margin-top: 10px;">
									<div class="col-md-6 col-md-offset-3">
									<a class="btn btn-danger" onclick="close_popup()">Cancel</a>
									<button type="submit" class="btn btn-success">Save</button>
									</div>
								</div> 
							</div>        
						</div>        
						<?php
						} else  if($cat == 'INSURANCE') {
						?>
							<div class="item form-group insurance_category">
								<div id="addTableFunction">
									<label class="col-md-2" for="email">
									  Add More Row                       
									</label>
									<div class="col-md-10">
									  <a class="fa fa-plus" onclick="addRowInsurance()" style="cursor:pointer; color:blue;"></a> | <a onclick="copySelectInsurance()" style="cursor:pointer; color:blue;">Copy Selected</a> | <a onclick="copyAllTrInsurance()" style="cursor:pointer; color:blue;">Copy All</a> | <a onclick="deleteAllTrInsurance()" style="cursor:pointer; color:blue;">Delete Selected</a>
									</div>
									<span id="displayMessage" style="display:none"></span>
								  <table id="add_plan_insurance" style="width: 100%; margin: 0 0;">
									<thead>
										<tr>
											<th>Term:<br/> <input type="radio" class="flat" name="TermSelectInsurance" id="TermDay" value="days" checked>Day <input type="radio" class="flat" name="TermSelectInsurance" id="TermMonth" value="months">Month</th>
											<th>Class</th>
											<th>Store Type</th>
											<th>Price ($)</th>
											<th>Deductible</th>
										</tr>
									</thead>
									<tbody id="plan-tBody" class="plan-tBody-insurance">
										<?php 
										$sqlCMap = "SELECT restrict_class.name, plan_terms.classId FROM plan_terms LEFT JOIN restrict_class ON plan_terms.classId = restrict_class.id WHERE plan_terms.plansId = $id AND plan_terms.vendorId = $vendor_id GROUP BY plan_terms.classId";
										$resultCMap = mysqli_query($conn, $sqlCMap);
										while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
											$classId = $rowCMap['classId'];
											$sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $id AND  vendorId = $vendor_id AND classId = $classId AND isDeleted=0";

											$resultPlan = mysqli_query($conn, $sqlPlan);
											$slNo = 1;
											if (mysqli_num_rows($resultPlan) > 0) {
												// output data of each row
												$i = 0;
												while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
										?>
										<tr>
											<td>
												<input type="checkbox" class="copyRow">
												<input type="text" name="planTermsID[]" style="display:none" value="<?php echo $rowPlan['id'];?>">
												
												<input type="number" class="dtElement" placeholder="" name="term[]" style="width: 40%;" value="<?php echo $rowPlan['termNo'];?>">
												<span class="tremType"><?php echo $rowPlan['TermSelect'];?></span>
											</td>
											<td> 
												<select id="class" class="dtElement" name="class[]" required>
													<?php 
														$sql = "SELECT id,name as class FROM restrict_class WHERE vendor_id=$vendor_id";
														$result = mysqli_query($conn, $sql);
													?>
													<option value="" disabled>Select Class</option>
													<?php 
														while($row = mysqli_fetch_assoc($result)) {
															if($row['class'] != null) {
																if($row['id'] == $classId){
																	?>
																	<option value="<?php echo $row['id'];?>" selected><?php echo $row['class'];?></option>
																	<?php
																} else {
														?>
															<option value="<?php echo $row['id'];?>"><?php echo $row['class'];?></option>
														<?php 
																} 
															}     
														}
													?>

												</select>
											</td>
											
											<td> 
												<select class="dtElement" name="storeType[]">
													<option value="">Select</option>
													<option value="Independent" <?php if($rowPlan['storeType']=='Independent') { echo 'selected';}?>>independent</option>
													<option value="Franchise" <?php if($rowPlan['storeType']=='Franchise') { echo 'selected';}?>>Franchise</option>
													<option value="Both"  <?php if($rowPlan['storeType']=='Both') { echo 'selected';}?>>Both</option>
												</select>
											</td>
											
											<td><?php echo $rowPlan['price_unit'];?> 
											<input type="number" class="dtElement" style="width: 80%;" placeholder="Price" name="price[]" value="<?php echo $rowPlan['price'];?>"></td>
											<td><?php echo $rowPlan['price_unit'];?> 
											<input type="number" class="dtElement" style="width: 80%;" placeholder="deductable" name="deductable[]" value="<?php echo $rowPlan['deductable'];?>"></td>
										</tr>
										<?php
													$slNo++;
													$i++;
												}
											} else {
												$error_message = 'Nothing found!';
											}
										}
										?>
									</tbody>
								  </table> 
								  <div class="ln_solid"></div>
								  <div class="form-group">
									<div class="col-md-6 col-md-offset-3">
									  <a class="btn btn-danger" onclick="close_popup()">Cancel</a>
									  <button id="send" type="submit" class="btn btn-success">Save</button>
									</div>
								  </div> 
								</div>
							  </div>
						<?php
						} else  if($cat == 'GAP') {
						?>
						<div class="item form-group gap_category">
							<div id="addTableFunction">
								<label class="col-md-2" for="email">
								  Add More Row                       
								</label>
								<div class="col-md-10">
								  <a class="fa fa-plus" onclick="addRowGap()" style="cursor:pointer; color:blue;"></a> | <a onclick="copySelectGap()" style="cursor:pointer; color:blue;">Copy Selected</a> | <a onclick="copyAllTrGap()" style="cursor:pointer; color:blue;">Copy All</a> | <a onclick="deleteAllTrGap()" style="cursor:pointer; color:blue;">Delete Selected</a>
								</div>
								<span id="displayMessage" style="display:none"></span>
							  <table id="add_plan_gap" style="width: 100%; margin: 0 0;">
								<thead>
									<tr>
										<th>Term From:<br/> <input type="radio" class="flat" name="TermFromOption" value="days" checked>Day <input type="radio" class="flat" name="TermFromOption" value="months">Month</th>
										<th>Term To:<br/> <input type="radio" class="flat" name="TermToOption" value="days" checked>Day <input type="radio" class="flat" name="TermToOption" id="TermMonth" value="months">Month</th>
										<th>Class</th>
										<th>Coverage Limit</th>
										<th>Store Type</th>
										<th>Payment</th>
										<th>Price ($)</th>
									</tr>
								</thead>
								<tbody id="plan-tBody" class="plan-tBody-gap">
									<?php 
										$sqlCMap = "SELECT restrict_class.name, plan_terms.classId FROM plan_terms LEFT JOIN restrict_class ON plan_terms.classId = restrict_class.id WHERE plan_terms.plansId = $id AND plan_terms.vendorId = $vendor_id GROUP BY plan_terms.classId";
										$resultCMap = mysqli_query($conn, $sqlCMap);
										while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
											$classId = $rowCMap['classId'];
											$sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $id AND  vendorId = $vendor_id AND classId = $classId AND isDeleted=0";

											$resultPlan = mysqli_query($conn, $sqlPlan);
											$slNo = 1;
											if (mysqli_num_rows($resultPlan) > 0) {
												// output data of each row
												$i = 0;
												while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
										?>
										<tr>
											<td>
												<input type="checkbox" class="copyRow">
												<input type="text" name="planTermsID[]" style="display:none" value="<?php echo $rowPlan['id'];?>">
												
												<input type="number" class="dtElement" placeholder="" name="termFrom[]" style="width: 40%;" value="<?php echo $rowPlan['termFrom'];?>">
												<span class="tremType"><?php echo $rowPlan['TermFromOption'];?></span>
											</td>
											<td>
												<input type="text" name="planTermsID[]" style="display:none" value="<?php echo $rowPlan['id'];?>">
												
												<input type="number" class="dtElement" placeholder="" name="termTo[]" style="width: 40%;" value="<?php echo $rowPlan['termTo'];?>">
												<span class="tremType"><?php echo $rowPlan['TermToOption'];?></span>
											</td>
											<td> 
												<select id="class" class="dtElement" name="class[]" required>
													<?php 
														$sql = "SELECT id,name as class FROM restrict_class WHERE vendor_id=$vendor_id";
														$result = mysqli_query($conn, $sql);
													?>
													<option value="" disabled>Select Class</option>
													<?php 
														while($row = mysqli_fetch_assoc($result)) {
															if($row['class'] != null) {
																if($row['id'] == $classId){
																	?>
																	<option value="<?php echo $row['id'];?>" selected><?php echo $row['class'];?></option>
																	<?php
																} else {
														?>
															<option value="<?php echo $row['id'];?>"><?php echo $row['class'];?></option>
														<?php 
																} 
															}     
														}
													?>

												</select>
											</td>
											<td>None <input type="checkbox" class="dtElement2" name="coverageLimitNone[<?php echo $i;?>]" <?php if($rowPlan['coverageLimitNone'] == 'on') { echo 'checked'; }?>> | Amount <input type="number" class="dtElement" placeholder="" value="<?php echo $rowPlan['coverageLimit'];?>" name="coverageLimit[]" style="width: 30%;"></td>
											<td> 
												<select class="dtElement" name="storeType[]">
													<option value="">Select</option>
													<option value="Independent" <?php if($rowPlan['storeType']=='Independent') { echo 'selected';}?>>independent</option>
													<option value="Franchise" <?php if($rowPlan['storeType']=='Franchise') { echo 'selected';}?>>Franchise</option>
													<option value="Both"  <?php if($rowPlan['storeType']=='Both') { echo 'selected';}?>>Both</option>
												</select>
											</td>
											<td>
												<select class="dtElement" name="payment[]">
													<option value="">Select</option><option value="Lease">Lease</option>
													<option value="Finance" <?php if($rowPlan['payment']=='Finance') { echo 'selected';}?>>Finance</option>
													<option value="Cash" <?php if($rowPlan['payment']=='Cash') { echo 'selected';}?>>Cash</option>
													<option value="Cash & Finance" <?php if($rowPlan['payment']=='Cash & Finance') { echo 'selected';}?>>Cash & Finance</option>
												</select>
											</td>
											<td><?php echo $rowPlan['price_unit'];?> 
											<input type="number" class="dtElement" style="width: 80%;" placeholder="Price" name="price[]" value="<?php echo $rowPlan['price'];?>"></td>
										</tr>
										<?php
													$slNo++;
													$i++;
												}
											} else {
												$error_message = 'Nothing found!';
											}
										}
										?>
								</tbody>
							  </table> 
							  <div class="ln_solid"></div>
							  <div class="form-group">
								<div class="col-md-6 col-md-offset-3">
								  <a class="btn btn-danger" onclick="close_popup()">Cancel</a>
								  <button id="send" type="submit" class="btn btn-success">Save</button>
								</div>
							  </div> 
							</div>   
						  </div>   
						<?php
						} else  if($cat == 'ANCILLARY') {
						?>
							<div class="item form-group anciallary_category">
								<div id="addTableFunction">
									<label class="col-md-2" for="email">
									  Add More Row                       
									</label>
									<div class="col-md-10">
									  <a class="fa fa-plus" onclick="addRowAnciallary()" style="cursor:pointer; color:blue;"></a> | <a onclick="copySelectAnciallary()" style="cursor:pointer; color:blue;">Copy Selected</a> | <a onclick="copyAllTrAnciallary()" style="cursor:pointer; color:blue;">Copy All</a> | <a onclick="deleteAllTrAnciallary()" style="cursor:pointer; color:blue;">Delete Selected</a>
									</div>
									<span id="displayMessage" style="display:none"></span>
								  <table id="add_plan_anciallary" style="width: 100%; margin: 0 0;">
									<thead>
										<tr>
											<th>Term:<br/> <input type="radio" class="flat" name="TermSelectAnciallary" id="TermDay" value="days" checked>Day <input type="radio" class="flat" name="TermSelectAnciallary" id="TermMonth" value="months">Month</th>
											<th>Class</th>
											<th>Coverage Limit</th>
											<th>Store Type</th>
											<th>Options</th>
											<th>Payment</th>
											<th>Price ($)</th>
										</tr>
									</thead>
									<tbody id="plan-tBody" class="plan-tBody-anciallary">
										<?php 
										$sqlCMap = "SELECT restrict_class.name, plan_terms.classId FROM plan_terms LEFT JOIN restrict_class ON plan_terms.classId = restrict_class.id WHERE plan_terms.plansId = $id AND plan_terms.vendorId = $vendor_id GROUP BY plan_terms.classId";
										$resultCMap = mysqli_query($conn, $sqlCMap);
										while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
											$classId = $rowCMap['classId'];
											$sqlPlan = "SELECT * FROM plan_terms WHERE plansId = $id AND  vendorId = $vendor_id AND classId = $classId AND isDeleted=0";

											$resultPlan = mysqli_query($conn, $sqlPlan);
											$slNo = 1;
											if (mysqli_num_rows($resultPlan) > 0) {
												// output data of each row
												$i = 0;
												while($rowPlan = mysqli_fetch_assoc($resultPlan)) {
										?>
										<tr>
											<td>
												<input type="checkbox" class="copyRow">
												<input type="text" name="planTermsID[]" style="display:none" value="<?php echo $rowPlan['id'];?>">
												
												<input type="number" class="dtElement" placeholder="" name="term[]" style="width: 40%;" value="<?php echo $rowPlan['termNo'];?>">
												<span class="tremType"><?php echo $rowPlan['TermSelectAnciallary'];?></span>
											</td>
											<td> 
												<select id="class" class="dtElement" name="class[]" required>
													<?php 
														$sql = "SELECT id,name as class FROM restrict_class WHERE vendor_id=$vendor_id";
														$result = mysqli_query($conn, $sql);
													?>
													<option value="" disabled>Select Class</option>
													<?php 
														while($row = mysqli_fetch_assoc($result)) {
															if($row['class'] != null) {
																if($row['id'] == $classId){
																	?>
																	<option value="<?php echo $row['id'];?>" selected><?php echo $row['class'];?></option>
																	<?php
																} else {
														?>
															<option value="<?php echo $row['id'];?>"><?php echo $row['class'];?></option>
														<?php 
																} 
															}     
														}
													?>

												</select>
											</td>
											<td>None <input type="checkbox" class="dtElement2" name="coverageLimitNone[<?php echo $i;?>]" <?php if($rowPlan['coverageLimitNone'] == 'on') { echo 'checked'; }?>> | Amount <input type="number" class="dtElement" placeholder="" value="<?php echo $rowPlan['coverageLimit'];?>" name="coverageLimit[]" style="width: 30%;"></td>
											<td> 
												<select class="dtElement" name="storeType[]">
													<option value="">Select</option>
													<option value="Independent" <?php if($rowPlan['storeType']=='Independent') { echo 'selected';}?>>independent</option>
													<option value="Franchise" <?php if($rowPlan['storeType']=='Franchise') { echo 'selected';}?>>Franchise</option>
													<option value="Both"  <?php if($rowPlan['storeType']=='Both') { echo 'selected';}?>>Both</option>
												</select>
											</td>
											<td>
												<select class="dtElement" name="options[]">
													<option value="">Select</option>
													<option value="ALLOY WHEELS" <?php if($rowPlan['options']=='ALLOY WHEELS') { echo 'selected';}?>>ALLOY WHEELS</option>
													<option value="CHROME WHEELS" <?php if($rowPlan['options']=='CHROME WHEELS') { echo 'selected';}?>>CHROME WHEELS</option>
													<option value="BOTH" <?php if($rowPlan['options']=='BOTH') { echo 'selected';}?>>BOTH</option>
													<option value="NONE" <?php if($rowPlan['options']=='NONE') { echo 'selected';}?>>NONE</option>
												</select>
											</td>
											<td>
												<select class="dtElement" name="payment[]">
													<option value="">Select</option><option value="Lease">Lease</option>
													<option value="Finance" <?php if($rowPlan['payment']=='Finance') { echo 'selected';}?>>Finance</option>
													<option value="Cash" <?php if($rowPlan['payment']=='Cash') { echo 'selected';}?>>Cash</option>
													<option value="Cash & Finance" <?php if($rowPlan['payment']=='Cash & Finance') { echo 'selected';}?>>Cash & Finance</option>
												</select>
											</td>
											<td><?php echo $rowPlan['price_unit'];?> 
											<input type="number" class="dtElement" style="width: 80%;" placeholder="Price" name="price[]" value="<?php echo $rowPlan['price'];?>"></td>
										</tr>
										<?php
													$slNo++;
													$i++;
												}
											} else {
												$error_message = 'Nothing found!';
											}
										}
										?>
									</tbody>
								  </table> 
								  <div class="ln_solid"></div>
								  <div class="form-group">
									<div class="col-md-6 col-md-offset-3">
									  <a class="btn btn-danger" onclick="close_popup()">Cancel</a>
									  <button id="send" type="submit" class="btn btn-success">Save</button>
									</div>
								  </div> 
							  </div>             
							</div>             
						<?php
						}
						?>   
					</form>

<script>
$(document).ready(function() {
$(window).keydown(function(event){
	if(event.keyCode == 13) {
	event.preventDefault();
	return false;
	}
});
});
</script>
		</div>
    </div>
    <span class="addRow" style="display:none"></span>
	<script>
var t, ti, ta, tg, vendorId = '', getPlanListData = [];
$(document).ready(function(){
	t = $('#add_plan_vsc').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        "filter":     false
    });
	ti = $('#add_plan_insurance').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        "filter":     false
    });
	tg = $('#add_plan_gap').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        "filter":     false
    });
	ta = $('#add_plan_anciallary').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        "filter":     false
    });
 
	// if(vendorId == '') {
	//   document.getElementById("addTableFunction").style.opacity = 0.6;
	// }
	//$(".fa-plus").off("click");
	$('#add_plan_vsc tbody').on( 'click', 'tr', function () {
		var trDomElement = this;
		$(this).find('td .copyRow').each(function() {
			if($(this)[0].checked){
				$(trDomElement).addClass('selected');
			} else {
				$(trDomElement).removeClass('selected');
			}
		});
	});
	$('#add_plan_insurance tbody').on( 'click', 'tr', function () {
		var trDomElement = this;
		$(this).find('td .copyRow').each(function() {
			if($(this)[0].checked){
				$(trDomElement).addClass('selected');
			} else {
				$(trDomElement).removeClass('selected');
			}
		});
	});
	$('#add_plan_gap tbody').on( 'click', 'tr', function () {
		var trDomElement = this;
		$(this).find('td .copyRow').each(function() {
			if($(this)[0].checked){
				$(trDomElement).addClass('selected');
			} else {
				$(trDomElement).removeClass('selected');
			}
		});
	});
	$('#add_plan_anciallary tbody').on( 'click', 'tr', function () {
		var trDomElement = this;
		$(this).find('td .copyRow').each(function() {
			if($(this)[0].checked){
				$(trDomElement).addClass('selected');
			} else {
				$(trDomElement).removeClass('selected');
			}
		});
	});
});
function addRow() {
	vendorId = $('#vendor').val();
    var rowCount = t.rows().count();
    //console.log(rowCount);
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
          +'<option value="" disabled selected>Select class</option>';
    if(getPlanListData.length) {
      getPlanListData.forEach(function(item){
        classSelectElement += '<option value="'+ item.id + '">' + item.class + '</option>';
      });
    }
    classSelectElement += '</select>';
    t.row.add( [
        '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
        classSelectElement,
        '<input type="number" class="dtElement" placeholder="Cover milage" name="coverMailage[]">',
        '<input type="number" class="dtElement" placeholder="Up to milage" name="upToMailage[]">',
        '<input type="checkbox" class="dtElement" name="manufacturerWarranty['+rowCount+']">',
        '<input type="number" class="dtElement" placeholder="Milage From" name="mailageFrom[]">',
        '<input type="number" class="dtElement" placeholder="Milage To" name="mailageTo[]">',
        '<input type="number" class="dtElement" placeholder="Price" name="price[]">',
        '<input type="number" class="dtElement" placeholder="Price" name="deductable[]">'
    ] ).draw( false );
  };
function addRowInsurance() {
    var rowCount = ti.rows().count();
    //console.log(rowCount);
	vendorId = $('#vendor').val();
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
          +'<option value="" disabled selected>Select class</option>';
    if(getPlanListData.length) {
      getPlanListData.forEach(function(item){
        classSelectElement += '<option value="'+ item.id + '">' + item.class + '</option>';
      });
    }
    classSelectElement += '</select>';
    ti.row.add( [
        '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
        classSelectElement,
        '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
        '<input type="number" class="dtElement" placeholder="Price" name="deductable[]">',
        '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
    ] ).draw( false );
  };
function addRowGap() {
    var rowCount = tg.rows().count();
	vendorId = $('#vendor').val();
    //console.log(rowCount);
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
      var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
            +'<option value="" disabled selected>Select class</option>';
      if(getPlanListData.length) {
        getPlanListData.forEach(function(item){
          classSelectElement += '<option value="'+ item.id + '">' + item.class + '</option>';
        });
      }
      classSelectElement += '</select>';
      tg.row.add([
          '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="termFrom[]" style="width: 70%;">',
          '<input type="number" class="dtElement" placeholder="" name="termTo[]" style="width: 70%;">',
          classSelectElement,
          'None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
          '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
          '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
          '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
        ] ).draw( false );
};
function addRowAnciallary() {
    var rowCount = ta.rows().count();
	vendorId = $('#vendor').val();
    //console.log(rowCount);
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
	var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
		+'<option value="" disabled selected>Select class</option>';
	if(getPlanListData.length) {
		getPlanListData.forEach(function(item){
		  classSelectElement += '<option value="'+ item.id + '">' + item.class + '</option>';
		});
	}
	classSelectElement += '</select>';
	ta.row.add([
          '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
          classSelectElement,
          'None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
          '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
          '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>',
          '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
          '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
	] ).draw( false );
};
function copyAllTr() {
	vendorId = $('#vendor').val();
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    $(".plan-tBody-vsc").find('tr').each(function(event) {
      var rowCount = t.rows().count();
      //console.log(rowCount);
      var dtElement = [], tdCount = 0;
      $(this).find('td .dtElement').each(function() {
        if(tdCount == 0){
          dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" name="term[]" placeholder="" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 1){
          var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
                +'<option value="" disabled selected>Select class</option>';
              
          var selectedID = $(this).val();
          if(getPlanListData.length) {
            getPlanListData.forEach(function(item){
              classSelectElement += '<option value="'+ item.id +'"';
              if(item.id == selectedID) {
                classSelectElement += ' selected>' + item.class + '</option>';
              } else {
                classSelectElement += '>' + item.class + '</option>';
              }
            });
          }
          classSelectElement += '</select>';
          dtElement.push(classSelectElement);
        } else if(tdCount == 2){
          dtElement.push('<input type="number" class="dtElement" placeholder="Cover milage" name="coverMailage[]" value="'+$(this).val()+'">');
        } else if(tdCount == 3){
          dtElement.push('<input type="number" class="dtElement" placeholder="Up to milage" name="upToMailage[]" value="'+$(this).val()+'">');
        } else if(tdCount == 4){
          if($(this).prop( "checked" )) {
            dtElement.push('<input type="checkbox" class="dtElement" name="manufacturerWarranty['+rowCount+']" checked>');
          } else {
            dtElement.push('<input type="checkbox" class="dtElement" name="manufacturerWarranty['+rowCount+']">');
          }
        } else if(tdCount == 5){
          dtElement.push('<input type="number" class="dtElement" placeholder="Milage From" name="mailageFrom[]" value="'+$(this).val()+'">');
        } else if(tdCount == 6){
          dtElement.push('<input type="number" class="dtElement" placeholder="Milage To" name="mailageTo[]" value="'+$(this).val()+'">');
        } else if(tdCount == 7){
          dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="price[]" value="'+$(this).val()+'">');
        } else if(tdCount == 8){
          dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="deductable[]" value="'+$(this).val()+'">');
        }
        tdCount ++;
      });
      if(dtElement.length > 0) t.row.add(dtElement).draw( false );
    });
  };
function copyAllTrInsurance() {
	vendorId = $('#vendor').val();
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    $(".plan-tBody-insurance").find('tr').each(function(event) {
      var rowCount = ti.rows().count();
      //console.log(rowCount);
      var dtElement = [], tdCount = 0;
      $(this).find('td .dtElement').each(function() {
        if(tdCount == 0){
          dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" name="term[]" placeholder="" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 1){
          var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
                +'<option value="" disabled selected>Select class</option>';
              
          var selectedID = $(this).val();
          if(getPlanListData.length) {
            getPlanListData.forEach(function(item){
              classSelectElement += '<option value="'+ item.id +'"';
              if(item.id == selectedID) {
                classSelectElement += ' selected>' + item.class + '</option>';
              } else {
                classSelectElement += '>' + item.class + '</option>';
              }
            });
          }
          classSelectElement += '</select>';
          dtElement.push(classSelectElement);
        } else if(tdCount == 2){
          var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          if($(this).val()=='Independent') {
           var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          } else if($(this).val()=='Franchise') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
          }  else if($(this).val()=='Both') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
          }
          dtElement.push(storeType);
        } else if(tdCount == 3){
          dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="deductable[]" value="'+$(this).val()+'">');
        } else if(tdCount == 4){
         dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="price[]" value="'+$(this).val()+'">');
        }
        tdCount ++;
      });
      if(dtElement.length > 0) ti.row.add(dtElement).draw( false );
    });
  };
function copyAllTrGap() {
	//console.log(rowCount);
	vendorId = $('#vendor').val();
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    $(".plan-tBody-gap").find('tr').each(function(event) {
      var rowCount = tg.rows().count();
      //console.log(rowCount);
      var dtElement = [], tdCount = 0;
      $(this).find('td .dtElement').each(function() {
        if(tdCount == 0){
          dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="termFrom[]" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 1){
          dtElement.push('<input type="number" class="dtElement" placeholder="" name="termTo[]" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 2){
          var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
                +'<option value="" disabled selected>Select class</option>';
              
          var selectedID = $(this).val();
          if(getPlanListData.length) {
            getPlanListData.forEach(function(item){
              classSelectElement += '<option value="'+ item.id +'"';
              if(item.id == selectedID) {
                classSelectElement += ' selected>' + item.class + '</option>';
              } else {
                classSelectElement += '>' + item.class + '</option>';
              }
            });
          }
          classSelectElement += '</select>';
          dtElement.push(classSelectElement);
        } else if(tdCount == 3){
          dtElement.push('None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;" value="'+$(this).val()+'">');
        } else if(tdCount == 4){
          var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          if($(this).val()=='Independent') {
           var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          } else if($(this).val()=='Franchise') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
          }  else if($(this).val()=='Both') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
          }
          dtElement.push(storeType);
        } else if(tdCount == 5){
          var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          if($(this).val()=='Lease') {
            var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease" selected>Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance" selected>Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash" selected>Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash & Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance" selected>Cash & Finance</option></select>';
          }
		  dtElement.push(payment);
        } else if(tdCount == 6){
          dtElement.push('<input type="number" class="dtElement" placeholder="" name="price[]" value="'+$(this).val()+'">');
        }
        tdCount ++;
      });
      if(dtElement.length > 0) tg.row.add(dtElement).draw( false );
    });
};
function copyAllTrAnciallary() {
	//console.log(rowCount);
	vendorId = $('#vendor').val();
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    $(".plan-tBody-anciallary").find('tr').each(function(event) {
      var rowCount = ta.rows().count();
      //console.log(rowCount);
      var dtElement = [], tdCount = 0;
      $(this).find('td .dtElement').each(function() {
        if(tdCount == 0){
          dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 1){
          var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
                +'<option value="" disabled selected>Select class</option>';
              
          var selectedID = $(this).val();
          if(getPlanListData.length) {
            getPlanListData.forEach(function(item){
              classSelectElement += '<option value="'+ item.id +'"';
              if(item.id == selectedID) {
                classSelectElement += ' selected>' + item.class + '</option>';
              } else {
                classSelectElement += '>' + item.class + '</option>';
              }
            });
          }
          classSelectElement += '</select>';
          dtElement.push(classSelectElement);
        } else if(tdCount == 2){
          dtElement.push('None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;" value="'+$(this).val()+'">');
        }  else if(tdCount == 3){
          var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          if($(this).val()=='Independent') {
           var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          } else if($(this).val()=='Franchise') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
          }  else if($(this).val()=='Both') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
          }
          dtElement.push(storeType);
        } else if(tdCount == 4){
          var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          if($(this).val()=='ALLOY WHEELS') {
           var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS" selected>ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='CHROME WHEELS') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS" selected>CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='BOTH') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH" selected>BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='NONE') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE" selected>NONE</option></select>';
          }
          dtElement.push(options);
        } else if(tdCount == 5){
          var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          if($(this).val()=='Lease') {
            var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease" selected>Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance" selected>Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash" selected>Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash & Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance" selected>Cash & Finance</option></select>';
          }
		  dtElement.push(payment);
        } else if(tdCount == 6){
          dtElement.push('<input type="number" class="dtElement" placeholder="" name="price[]" value="'+$(this).val()+'">');
        }
        tdCount ++;
      });
      if(dtElement.length > 0) ta.row.add(dtElement).draw( false );
    });
};
function copySelect() {
	vendorId = $('#vendor').val();
	if(vendorId == '') {
	  showMessage('Please select the vendor');
	  return false;
	}
	$(".plan-tBody-vsc").find('tr.selected').each(function(event) {
		var rowCount = t.rows().count();
		//console.log(rowCount);
		var dtElement = [], tdCount = 0;
		$(this).find('td .dtElement').each(function() {
			if(tdCount == 0){
			  dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" name="term[]" placeholder="" style="width: 70%;" value="'+$(this).val()+'">');
			} else if(tdCount == 1){
			  var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
					+'<option value="" disabled selected>Select class</option>';
				  
			  var selectedID = $(this).val();
			  if(getPlanListData.length) {
				getPlanListData.forEach(function(item){
				  classSelectElement += '<option value="'+ item.id +'"';
				  if(item.id == selectedID) {
					classSelectElement += ' selected>' + item.class + '</option>';
				  } else {
					classSelectElement += '>' + item.class + '</option>';
				  }
				});
			  }
			  classSelectElement += '</select>';
			  dtElement.push(classSelectElement);
			} else if(tdCount == 2){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Cover milage" name="coverMailage[]" value="'+$(this).val()+'">');
			} else if(tdCount == 3){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Up to milage" name="upToMailage[]" value="'+$(this).val()+'">');
			} else if(tdCount == 4){
			  if($(this).prop( "checked" )) {
				dtElement.push('<input type="checkbox" class="dtElement" name="manufacturerWarranty['+rowCount+']" checked>');
			  } else {
				dtElement.push('<input type="checkbox" class="dtElement" name="manufacturerWarranty['+rowCount+']">');
			  }
			} else if(tdCount == 5){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Milage From" name="mailageFrom[]" value="'+$(this).val()+'">');
			} else if(tdCount == 6){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Milage To" name="mailageTo[]" value="'+$(this).val()+'">');
			} else if(tdCount == 7){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="price[]" value="'+$(this).val()+'">');
			} else if(tdCount == 8){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="deductable[]" value="'+$(this).val()+'">');
			}
			tdCount ++;
		});
		if(dtElement.length > 0) t.row.add(dtElement).draw( false );
	});
};
function copySelectInsurance() {
	vendorId = $('#vendor').val();
	if(vendorId == '') {
	  showMessage('Please select the vendor');
	  return false;
	}
	$(".plan-tBody-insurance").find('tr.selected').each(function(event) {
		var rowCount = ti.rows().count();
		//console.log(rowCount);
		var dtElement = [], tdCount = 0;
		$(this).find('td .dtElement').each(function() {
			if(tdCount == 0){
			  dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" name="term[]" placeholder="" style="width: 70%;" value="'+$(this).val()+'">');
			} else if(tdCount == 1){
			  var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
					+'<option value="" disabled selected>Select class</option>';
				  
			  var selectedID = $(this).val();
			  if(getPlanListData.length) {
				getPlanListData.forEach(function(item){
				  classSelectElement += '<option value="'+ item.id +'"';
				  if(item.id == selectedID) {
					classSelectElement += ' selected>' + item.class + '</option>';
				  } else {
					classSelectElement += '>' + item.class + '</option>';
				  }
				});
			  }
			  classSelectElement += '</select>';
			  dtElement.push(classSelectElement);
			} else if(tdCount == 2){
          var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          if($(this).val()=='Independent') {
           var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          } else if($(this).val()=='Franchise') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
          }  else if($(this).val()=='Both') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
          }
          dtElement.push(storeType);
        } else if(tdCount == 3){
          dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="deductable[]" value="'+$(this).val()+'">');
        } else if(tdCount == 4){
         dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="price[]" value="'+$(this).val()+'">');
        }
			tdCount ++;
		});
		if(dtElement.length > 0) ti.row.add(dtElement).draw( false );
	});
};
function copySelectGap() {
	vendorId = $('#vendor').val();
	if(vendorId == '') {
	  showMessage('Please select the vendor');
	  return false;
	}
	$(".plan-tBody-gap").find('tr.selected').each(function(event) {
		var rowCount = tg.rows().count();
		//console.log(rowCount);
		var dtElement = [], tdCount = 0;
		$(this).find('td .dtElement').each(function() {
			if(tdCount == 0){
			  dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="termFrom[]" style="width: 70%;" value="'+$(this).val()+'">');
			} else if(tdCount == 1){
			  dtElement.push('<input type="number" class="dtElement" placeholder="" name="termTo[]" style="width: 70%;" value="'+$(this).val()+'">');
			} else if(tdCount == 2){
			  var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
					+'<option value="" disabled selected>Select class</option>';
				  
			  var selectedID = $(this).val();
			  if(getPlanListData.length) {
				getPlanListData.forEach(function(item){
				  classSelectElement += '<option value="'+ item.id +'"';
				  if(item.id == selectedID) {
					classSelectElement += ' selected>' + item.class + '</option>';
				  } else {
					classSelectElement += '>' + item.class + '</option>';
				  }
				});
			  }
			  classSelectElement += '</select>';
			  dtElement.push(classSelectElement);
			} else if(tdCount == 3){
			  dtElement.push('None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;" value="'+$(this).val()+'">');
			} else if(tdCount == 4){
			  var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
			  if($(this).val()=='Independent') {
			   var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
			  } else if($(this).val()=='Franchise') {
				var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
			  }  else if($(this).val()=='Both') {
				var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
			  }
			  dtElement.push(storeType);
			} else if(tdCount == 5){
			  var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
			  if($(this).val()=='Lease') {
				var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease" selected>Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
			  } else if($(this).val()=='Finance') {
				var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance" selected>Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
			  } else if($(this).val()=='Cash') {
				var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash" selected>Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
			  } else if($(this).val()=='Cash & Finance') {
				var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance" selected>Cash & Finance</option></select>';
			  }
			  dtElement.push(payment);
			} else if(tdCount == 6){
			  dtElement.push('<input type="number" class="dtElement" placeholder="" name="price[]" value="'+$(this).val()+'">');
			}
			tdCount ++;
		});
		if(dtElement.length > 0) tg.row.add(dtElement).draw( false );
	});
};
function copySelectAnciallary() {
	vendorId = $('#vendor').val();
	if(vendorId == '') {
	  showMessage('Please select the vendor');
	  return false;
	}
	$(".plan-tBody-anciallary").find('tr.selected').each(function(event) {
		var rowCount = ta.rows().count();
		//console.log(rowCount);
		var dtElement = [], tdCount = 0;
		$(this).find('td .dtElement').each(function() {
			if(tdCount == 0){
          dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 1){
          var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
                +'<option value="" disabled selected>Select class</option>';
              
          var selectedID = $(this).val();
          if(getPlanListData.length) {
            getPlanListData.forEach(function(item){
              classSelectElement += '<option value="'+ item.id +'"';
              if(item.id == selectedID) {
                classSelectElement += ' selected>' + item.class + '</option>';
              } else {
                classSelectElement += '>' + item.class + '</option>';
              }
            });
          }
          classSelectElement += '</select>';
          dtElement.push(classSelectElement);
        } else if(tdCount == 2){
          dtElement.push('None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;" value="'+$(this).val()+'">');
        }  else if(tdCount == 3){
          var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          if($(this).val()=='Independent') {
           var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          } else if($(this).val()=='Franchise') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
          }  else if($(this).val()=='Both') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
          }
          dtElement.push(storeType);
        } else if(tdCount == 4){
          var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          if($(this).val()=='ALLOY WHEELS') {
           var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS" selected>ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='CHROME WHEELS') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS" selected>CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='BOTH') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH" selected>BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='NONE') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE" selected>NONE</option></select>';
          }
          dtElement.push(options);
        } else if(tdCount == 5){
          var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          if($(this).val()=='Lease') {
            var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease" selected>Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance" selected>Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash" selected>Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash & Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance" selected>Cash & Finance</option></select>';
          }
		  dtElement.push(payment);
        } else if(tdCount == 6){
          dtElement.push('<input type="number" class="dtElement" placeholder="" name="price[]" value="'+$(this).val()+'">');
        }
        tdCount ++;
		});
		if(dtElement.length > 0) ta.row.add(dtElement).draw( false );
	});
};
function deleteAllTr() {
	$(".plan-tBody-vsc").find('tr.selected').each(function(event) {
       t.row($(this)).remove().draw( false );
	});
}
function deleteAllTrInsurance() {
	$(".plan-tBody-insurance").find('tr.selected').each(function(event) {
       ti.row($(this)).remove().draw( false );
	});
}
function deleteAllTrGap() {
	$(".plan-tBody-gap").find('tr.selected').each(function(event) {
       tg.row($(this)).remove().draw( false );
	});
}
function deleteAllTrAnciallary() {
	$(".plan-tBody-anciallary").find('tr.selected').each(function(event) {
       ta.row($(this)).remove().draw( false );
	});
}
function add_plan() {
    $("#plan-tBody").find('tr.selected').each(function(event) {
      t.row($(this)).remove().draw( false );
    });
  };
var messageTimeObj = null;
function showMessage(message, className, timeout){
  clearTimeout(messageTimeObj);
  message = message || '';
  className = className || 'error';
  timeout = timeout || 3000;
  var messageElement = $("#displayMessage");
  messageElement.addClass(className);
  messageElement.text(message);
  document.getElementById("displayMessage").style.display = 'block';
  messageTimeObj = setTimeout(function() {
    messageElement.removeClass(className);
    messageElement.text('');
    document.getElementById("displayMessage").style.display = 'none';
  }, timeout);
}
function fnRemoveRow(e) {
  //console.log(vendorId);
  vendorId = $('#vendor').val();
  if(vendorId == ''){
    showMessage('Please select the vendor');
    return false;
  }
  var newRow = $(e.target).parents('tr');
  t.row(newRow).remove().draw( false );
}
function fnCopyRow(e) {
  var newRow = $(e.target).parents('tr');
  var cells = newRow[0].getElementsByTagName('td');
}
function show_class_by_category_edit() {
    var val = $('#vendor').val();
    $.ajax({
        method: "POST",
        url: "class_vendor_option-for-select.php",
        data: { cat : val }
    }).done(function(data) {
        $('.vendor_list_filter').html(data);
		var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
            +'<option value="">Select class</option></select>';
        if(val == 'VSC') {
			$('.vsc_category').show();
			$('.gap_category').hide();
			$('.anciallary_category').hide();
			$('.insurance_category').hide();
			$('.sub_category').hide();
			$(".plan-tBody-vsc").find('tr').each(function(event) {
				t.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-insurance").find('tr').each(function(event) {
				ti.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-gap").find('tr').each(function(event) {
				tg.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-anciallary").find('tr').each(function(event) {
				ta.row($(this)).remove().draw( false );
			});
			t.row.add([
			  '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
			  classSelectElement,
			  '<input type="number" class="dtElement" placeholder="Cover milage" name="coverMailage[]">',
			  '<input type="number" class="dtElement" placeholder="Up to milage" name="upToMailage[]">',
			  '<input type="checkbox" class="dtElement" name="manufacturerWarranty[0]">',
			  '<input type="number" class="dtElement" placeholder="Milage From" name="mailageFrom[]">',
			  '<input type="number" class="dtElement" placeholder="Milage To" name="mailageTo[]">',
			  '<input type="number" class="dtElement" placeholder="Price" name="price[]">',
			  '<input type="number" class="dtElement" placeholder="Price" name="deductable[]">'
			] ).draw( false );

        } else if(val == 'INSURANCE') {
			$('.insurance_category').show();
			$('.vsc_category').hide();
			$('.gap_category').hide();
			$('.anciallary_category').hide();
			$('.sub_category').hide();
			$(".plan-tBody-vsc").find('tr').each(function(event) {
				t.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-insurance").find('tr').each(function(event) {
				ti.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-gap").find('tr').each(function(event) {
				tg.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-anciallary").find('tr').each(function(event) {
				ta.row($(this)).remove().draw( false );
			});
			ti.row.add( [
				'<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
				classSelectElement,
				'<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
				'<input type="number" class="dtElement" placeholder="Price" name="deductable[]">',
				'<input type="number" class="dtElement" placeholder="Price" name="price[]">'
			] ).draw( false );
        } else if(val == 'GAP') {
			$('.vsc_category').hide();
			$('.gap_category').show();
			$('.anciallary_category').hide();
			$('.insurance_category').hide();
			$('.sub_category').hide();
			$(".plan-tBody-vsc").find('tr').each(function(event) {
				t.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-insurance").find('tr').each(function(event) {
				ti.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-gap").find('tr').each(function(event) {
				tg.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-anciallary").find('tr').each(function(event) {
				ta.row($(this)).remove().draw( false );
			});
			tg.row.add([
			  '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="termFrom[]" style="width: 70%;">',
			  '<input type="number" class="dtElement" placeholder="" name="termTo[]" style="width: 70%;">',
			  classSelectElement,
			  'None <input type="checkbox" class="dtElement2" name="coverageLimitNone[0]"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
			  '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
			  '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
			  '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
			] ).draw( false );
        } else if(val == 'ANCILLARY') {
			$('.vsc_category').hide();
			$('.gap_category').hide();
			$('.insurance_category').hide();
			$('.anciallary_category').show();
			$('.sub_category').show();
			$(".plan-tBody-vsc").find('tr').each(function(event) {
				t.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-insurance").find('tr').each(function(event) {
				ti.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-gap").find('tr').each(function(event) {
				tg.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-anciallary").find('tr').each(function(event) {
				ta.row($(this)).remove().draw( false );
			});
			ta.row.add([
			  '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
			  classSelectElement,
			  'None <input type="checkbox" class="dtElement2" name="coverageLimitNone[0]"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
			  '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
			  '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>',
			  '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
			  '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
		] ).draw( false );
        }
    });
}
var countVenderClick = 0;
function show_class_by_vendor_edit() {
  // var cat = $('.cat_id').val();
  // var val = $(e.target).val();
  vendorId = $('#vendor').val();
  if(vendorId == '') {
    countVenderClick = 0;
    document.getElementById("addTableFunction").style.opacity = 0.6;
  } else {
    countVenderClick ++;
    document.getElementById("addTableFunction").style.opacity = 1;
    var e = document.getElementById("vendor");
    $.get("plan_query.php?api=getPlanListData&vendor_id="+vendorId, function(data, status) {
      data = JSON.parse(data);
      getPlanListData = data.classList;
      var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
            +'<option value="">Select class</option>';
      if(getPlanListData.length) {
        getPlanListData.forEach(function(item){
          classSelectElement += '<option value="'+ item.id + '">' + item.class + '</option>';
        });
      }
      classSelectElement += '</select>';
      var val = $('.cat_id').val();
      if(val == 'VSC') {
        $(".plan-tBody-vsc").find('tr').each(function(event) {
          t.row($(this)).remove().draw( false );
		  //console.log('as');
        });
        t.row.add([
          '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
          classSelectElement,
          '<input type="number" class="dtElement" placeholder="Cover milage" name="coverMailage[]">',
          '<input type="number" class="dtElement" placeholder="Up to milage" name="upToMailage[]">',
          '<input type="checkbox" class="dtElement" name="manufacturerWarranty[0]">',
          '<input type="number" class="dtElement" placeholder="Milage From" name="mailageFrom[]">',
          '<input type="number" class="dtElement" placeholder="Milage To" name="mailageTo[]">',
          '<input type="number" class="dtElement" placeholder="Price" name="price[]">',
          '<input type="number" class="dtElement" placeholder="Price" name="deductable[]">'
        ] ).draw( false );
      
      } else if(val == 'INSURANCE') {
        $(".plan-tBody-insurance").find('tr').each(function(event) {
          ti.row($(this)).remove().draw( false );
		  //console.log('as');
        });
		ti.row.add( [
			'<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
			classSelectElement,
			'<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
			'<input type="number" class="dtElement" placeholder="Price" name="deductable[]">',
			'<input type="number" class="dtElement" placeholder="Price" name="price[]">'
		] ).draw( false );      
      } else if(val == 'GAP') {
        $(".plan-tBody-gap").find('tr').each(function(event) {
          tg.row($(this)).remove().draw( false );
        });
        tg.row.add([
          '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="termFrom[]" style="width: 70%;">',
          '<input type="number" class="dtElement" placeholder="" name="termTo[]" style="width: 70%;">',
          classSelectElement,
          'None <input type="checkbox" class="dtElement2" name="coverageLimitNone[0]"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
          '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
          '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
          '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
        ] ).draw( false );
      
      } else if(val == 'ANCILLARY') {
        $(".plan-tBody-anciallary").find('tr').each(function(event) {
          ta.row($(this)).remove().draw( false );
        });
        ta.row.add([
			  '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
			  classSelectElement,
			  'None <input type="checkbox" class="dtElement2" name="coverageLimitNone[0]"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
			  '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
			  '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>',
			  '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
			  '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
		] ).draw( false );
      }
    });
  }
}
show_class_by_vendor_edit();
</script>