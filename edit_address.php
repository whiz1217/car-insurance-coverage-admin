<!-- Modal content -->
<div class="modal-content">
<?php
include_once('elements/db_connection.php');
$id = $_REQUEST['id'];
$sql = "SELECT * FROM dealer_portal_settings WHERE id = $id";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
?>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="update_address.php" method="post">
				
				<div class="item form-group">
					<label class="col-md-2">Address</label>
					<div class="col-md-10 col-sm-10 col-xs-12">
						<input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
						<textarea type="text" name="address" rows="10" class="form-control" placeholder="Body"><?php echo $row['address'];?></textarea>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="form-group">
				  <div class="col-md-6 col-md-offset-3">
					<a href="address_list.php" class="btn btn-danger">Cancel</a>
					<button id="send" type="submit" class="btn btn-success">Save</button>
				  </div>
				</div>             				
            </form>
        </div>
    </div>
<?php } ?>   
</div>
