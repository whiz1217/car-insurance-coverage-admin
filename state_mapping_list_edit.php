<!-- Modal content -->
<div class="modal-content">
<?php
include_once('elements/db_connection.php');
$id = $_REQUEST['id'];
$sql = "SELECT user_detail.company_name, user_detail.category, vendor_state_mapping.id, vendor_state_mapping.vendor_id, vendor_state_mapping.states FROM vendor_state_mapping LEFT JOIN user_detail ON vendor_state_mapping.vendor_id = user_detail.user_id WHERE vendor_state_mapping.id = $id";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
    $user_id = $row['vendor_id'];
    $state = $row['states'];
    $cat = $row['category'];
?>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="update_state_mapping.php" method="post">
                <div class="item form-group">
                    <label class="col-md-2">Category <span class="required">*</span></label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <select onchange="show_vendor_by_category(event)" class="form-control cat_id" required>
                            <option value="">PLEASE SELECT A CATEGORY</option>
                            <option value="VSC" <?php if($cat == 'VSC'){ echo "selected";}?>>VSC</option>
                            <option value="GAP" <?php if($cat == 'GAP'){ echo "selected";}?>>GAP</option>
                            <option value="ANCILLARY" <?php if($cat == 'ANCILLARY'){ echo "selected";}?>>ANCILLARY</option>
                            <option value="INSURANCE" <?php if($cat == 'INSURANCE'){ echo "selected";}?>>INSURANCE</option>
                        </select>
                    </div>
                </div>
                <div class="item form-group">
                <input type="hidden" name="old_vendor_id" value="<?php echo $row['vendor_id'];?>">
                    <label class="col-md-2">Choose Vendor *</label>
                    <div class="col-md-4 col-sm-6 col-xs-12 vendor_list_filter">        
                        <?php 
                            $sqlV = "SELECT users.id, user_detail.company_name FROM users 
                            LEFT JOIN user_detail ON users.id = user_detail.user_id 
                            WHERE users.user_role_id='2' AND user_detail.category = '$cat' ORDER BY users.id DESC";
                            $resultV = mysqli_query($conn, $sqlV);
                        ?>
                        <select name="vendor_id" class="form-control vendor_id " required>
                            <option value="">PLEASE SELECT A VENDOR</option>
                            <?php 
                                while($rowV = mysqli_fetch_assoc($resultV)) {
                                $sqlInner = "SELECT users.id, user_detail.company_name FROM users 
                                LEFT JOIN user_detail ON users.id = user_detail.user_id 
                                WHERE users.id=".$user_id.";";
                                $resultVendor = mysqli_query($conn, $sqlInner);
                                $isSelected = '';
                                while($innerRow = mysqli_fetch_assoc($resultVendor))
                                {
                                    $isSelected = $innerRow['id'];
                                }
                                ?>
                                    <option value="<?php echo $rowV['id'];?>" <?php if($isSelected == $rowV['id']){echo 'selected';} ?>><?php echo $rowV['company_name'];?></option>
                                <?php       
                                    }
                                ?>
                        </select>
                    </div>
                </div>
                
                <div class="item form-group">
                    <label class="col-md-2">Choose state</label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                    <select onchange="show_dealer(event)" name="state" class="form-control" required>
                        <option value="">PLEASE SELECT A STATE</option>
                        <?php 
                            $sqlA = "SELECT user_business_details.state FROM user_business_details LEFT JOIN users ON user_business_details.userId = users.id  WHERE users.isDeleted = 0 AND user_business_details.state != '' GROUP BY user_business_details.state ORDER BY user_business_details.state ASC";
                            $resultA = mysqli_query($conn, $sqlA);
                            while($rowA = mysqli_fetch_assoc($resultA)) {
                            ?>
                                <option value="<?php echo $rowA['state'];?>" <?php if($rowA['state'] == $row['states']) {echo 'selected';}?>>
                                    <?php echo $rowA['state'];?>
                                </option>
                            <?php       
                                }
                            ?>
                    </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="x_title">
                    <h2>State Mapping</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="row text-center state_data_div">
                    <div class="col-xs-4 dealer_list">
                    <select class="form-control" id ="features" name="Features[]" multiple="multiple" style="height:200px;">
                    <?php 
                    $sql = "SELECT users.id, user_business_details.businessName FROM users LEFT JOIN user_business_details ON users.id = user_business_details.userId WHERE users.user_role_id=3 AND user_business_details.state = '$state' ORDER BY users.id DESC";
                    $result = mysqli_query($conn, $sql);
                    while($row = mysqli_fetch_assoc($result)) {
                        $sqlAS = "SELECT states, dealer_id FROM vendor_state_mapping WHERE states = '$state' AND vendor_id = $user_id AND is_featured = 'no'";
                        $resultAS = mysqli_query($conn, $sqlAS);
                        $selected = false;
                        if (mysqli_num_rows($resultAS) > 0) {
                            while($rowAS = mysqli_fetch_assoc($resultAS)) {
                                if($rowAS['dealer_id'] == $row['id']) {
                                    $selected = true;
                            } } } if(!$selected) {
                    ?>
                    <option value="<?php echo $row['id'];?>"><?php echo $row['businessName'];?></option>
                    <?php } } ?>
                    </select>
                    </div>
                    <div class="col-xs-2">
                        <div class="input-group">
                            <button type="button" value="" class="btn btn-xs btn-primary" onclick="add_state()" id="add">Add >></button>
                        </div>
                        <div class="input-group" style="padding-top:10px;">
                            <button type="button" value="" class="btn btn-xs btn-danger" onclick="remove_state()" id="remove"><< Remove</button> 
                        </div>
                    </div>
                    <div class="col-xs-4 state_data_div">
                        <select class="form-control" name="selectedDealer[]" size="9" id="selected_features" multiple="multiple" style="height:200px;">
                        <?php 
                        $sqlAS = "SELECT vendor_state_mapping.dealer_id, user_business_details.businessName FROM vendor_state_mapping LEFT JOIN user_business_details ON vendor_state_mapping.dealer_id = user_business_details.userId LEFT JOIN users ON vendor_state_mapping.dealer_id = users.id  WHERE vendor_state_mapping.states = '$state' AND vendor_state_mapping.vendor_id = $user_id AND vendor_state_mapping.isDeleted = 0 AND users.isDeleted = 0 AND vendor_state_mapping.is_featured = 'no'";
                        $resultAS = mysqli_query($conn, $sqlAS);
                        while($rowAS = mysqli_fetch_assoc($resultAS)) {
                            ?>
                            <option value="<?php echo $rowAS['dealer_id'];?>" selected><?php echo $rowAS['businessName'];?></option>
                        <?php       
                        }
                        ?>
                        </select>
                    </div>
            </div>
            
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                <a href="state_mapping_list.php" class="btn btn-danger">Cancel</a>
                <button id="send" type="submit" class="btn btn-success">Save</button>
                </div>
            </div>               
            </form>
        </div>
    </div>
<?php } ?>   
</div>

<script>
function show_dealer(e) {
    var val = $(e.target).val();
    var vendor_id = $('.vendor_id').val();
    $.ajax({
      method: "POST",
      url: "show_dealer.php",
      data: { state : val, vendor_id : vendor_id }
    }).done(function(data) {
      $('.state_data_div').html(data);
    });
    // $.ajax({
    //   method: "POST",
    //   url: "show_feature_dealer.php",
    //   data: { state : val, vendor_id : vendor_id }
    // }).done(function(data) {
    //   $('.feature_state_data_div').html(data);
    // });
}
function add_state() {  
    console.log('asd');
    return !$('#features option:selected').remove().appendTo('#selected_features');  
};  
function remove_state() {  
    console.log('asd');  
    return !$('#selected_features option:selected').remove().appendTo('#features');  
};
// function feature_add_state() {  
//       return !$('#feature_features option:selected').remove().appendTo('#feature_selected_features');  
//   };  
//   function feature_remove_state() {    
//       return !$('#feature_selected_features option:selected').remove().appendTo('#feature_features');  
//   };
function show_vendor_by_category(e) {
  var cat = $(e.target).val();
  $.ajax({
    method: "POST",
    url: "vendor_list_filter.php",
    data: { cat : cat }
  }).done(function(data) {
    //console.log(data);
    $('.vendor_list_filter').html(data);
  });
}
</script>
