<!-- Modal content -->
<div class="modal-content">
<?php
include_once('elements/db_connection.php');
$id = $_REQUEST['id'];
$sql = "SELECT * FROM faq_video WHERE id = $id";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
?>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="update_video.php" method="post" enctype="multipart/form-data">
				
				<div class="item form-group">
					<label class="col-md-2">Category</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
						<select name="video_category_id" class="form-control">
						<?php 
                            $sqlCat = "SELECT * FROM master_video_category WHERE is_deleted='0'";
                            $resultCat = mysqli_query($conn, $sqlCat);
                            $slNo = 1;
                            if (mysqli_num_rows($resultCat) > 0) {
                                // output data of each row
                                while($rowCat = mysqli_fetch_assoc($resultCat)) {
                            ?>
							<option value="<?php echo $rowCat['id'];?>" <?php if($rowCat['id'] == $row['video_category_id']) { echo 'selected'; }?>><?php echo $rowCat['name'];?></option>
						<?php } }?>
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="item form-group">
					<label class="col-md-2">Sub Category</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" name="sub_category" class="form-control" value="<?php echo $row['sub_category'];?>" placeholder="Sub Category">
					</div>
				</div> 
				<div class="clearfix"></div>
				
				<div class="item form-group">
					<label class="col-md-2">Upload Video</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="file" name="video">
					</div>
				</div> 
				<div class="form-group">
				  <div class="col-md-6 col-md-offset-3">
					<a href="video_list.php" class="btn btn-danger">Cancel</a>
					<button id="send" type="submit" class="btn btn-success">Save</button>
				  </div>
				</div>             				
            </form>
        </div>
    </div>
<?php } ?>   
</div>
