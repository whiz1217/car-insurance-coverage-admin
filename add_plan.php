<?php 
	include_once('elements/db_connection.php');
	include_once('elements/left_menu.php');
	include_once('elements/header.php');
?>
<!-- /top navigation -->

<!-- page content -->
<style>
#plan-tBody input.dtElement{
  width: 100%;
}
#displayMessage {
  float: right;
}
.error {
  color:red;
}
.success {
  color: #07e00f;
}
</style>
<div class="right_col" role="main">
  <div class="x_panel">
      <div class="x_title">
        <h2>Create Plan</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form class="form-horizontal form-label-left" action="plan_query.php" method="post" enctype="multipart/form-data">
          <input type="text" name="api" style="display:none" value="insertPlanData">
          <div class="item form-group">
              <label class="col-md-2" >Category <span class="required">*</span></label>
              <div class="col-md-4">
                  <select onchange="show_class_by_category(event)" class="form-control cat_id" required>
                      <option value="">Select A Category</option>
                      <option value="VSC">VSC</option>
                      <option value="GAP">GAP</option>
                      <option value="ANCILLARY">ANCILLARY</option>
                      <option value="INSURANCE">INSURANCE</option>
                  </select>
              </div>
          </div>
          <div class="item form-group">
              <label class="col-md-2" >Vendor <span class="required">*</span></label>
              <div class="col-md-4 vendor_list_filter">
                <select id="vendor" name="vendor_id" class="form-control" disabled required>
                  <option value="">Select A Vendor</option>
                </select>
              </div>
          </div>
          <div class="item form-group">
              <label class="col-md-2" >Plan Name <span class="required">*</span></label>
              <div class="col-md-4">
                <input id="name" class="form-control col-md-7 col-xs-12" placeholder="Enter plan name" name="PlanName" required type="text">
              </div>
          </div>
		  <div class="item form-group sub_category" style="display:none;">
			<label class="col-md-2" >Sub Category</label>
			<div class="col-md-4">
			  <select name="sub_category" class="form-control">
				  <option value="">Select a Sub Category</option>
				  <option value="TIERS ONLY">TIERS ONLY</option>
				  <option value="KEY REPLACEMENT">KEY REPLACEMENT</option>
				  <option value="DENT & DING">DENT & DING</option>
				  <option value="WINDSHIELD REPLACEMENT">WINDSHIELD REPLACEMENT</option>
				  <option value="ETCH">ETCH</option>
				  <option value="VEHICLE REPLACEMENT">VEHICLE REPLACEMENT</option>
				  <option value="TIRE & WHEEL">TIRE & WHEEL</option>
				  <option value="PAINT & NTERIOR">PAINT & NTERIOR</option>
				  <option value="WEAR & TEAR PROTECTION">WEAR & TEAR PROTECTION</option>
				  <option value="PACKAGES">PACKAGES</option>
			  </select>
			</div>
          </div>
          <div class="item form-group">
              <label class="col-md-2">Upload Brochures</label>
              <div class="col-md-4">
              <input type="file" name="broacherDocument[]" class="form-control col-md-7 col-xs-12" multiple>
              </div>
          </div>
          <div class="clearfix"></div>
          <div class="ln_solid"></div>
          <div class="item form-group vsc_category" style="display:none;">
            <div id="addTableFunction">
                <label class="col-md-2" for="email">
                  Add More Row                       
                </label>
                <div class="col-md-10">
                  <a class="fa fa-plus" onclick="addRow()" style="cursor:pointer; color:blue;"></a> | <a onclick="copySelect()" style="cursor:pointer; color:blue;">Copy Selected</a> | <a onclick="copyAllTr()" style="cursor:pointer; color:blue;">Copy All</a> | <a onclick="deleteAllTr()" style="cursor:pointer; color:blue;">Delete Selected</a>
                </div>
                <span id="displayMessage" style="display:none"></span>
              <table id="add_plan_vsc" style="width: 100%; margin: 0 0;">
                <thead>
                    <tr>
                        <th>Term:<br/> <input type="radio" class="flat" name="TermSelect" id="TermDay" value="days" checked>Day <input type="radio" class="flat" name="TermSelect" id="TermMonth" value="months">Month</th>
                        <th>Class</th>
                        <th>Cover Mileage</th>
                        <th>Up To Mileage</th>
                        <th>Manufacturer Warranty</th>
                        <th>Mileage<br /> From</th>
                        <th>Mileage<br /> To</th>
                        <th>Price ($)</th>
                        <th>Deductible</th>
                    </tr>
                </thead>
                <tbody id="plan-tBody" class="plan-tBody-vsc">
                    
                </tbody>
              </table> 
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <a class="btn btn-danger" href="plan_list.php">Cancel</a>
                  <button id="send" type="submit" class="btn btn-success">Save</button>
                </div>
              </div> 
            </div>
          </div>
		  <div class="item form-group insurance_category" style="display:none;">
            <div id="addTableFunction">
                <label class="col-md-2" for="email">
                  Add More Row                       
                </label>
                <div class="col-md-10">
                  <a class="fa fa-plus" onclick="addRowInsurance()" style="cursor:pointer; color:blue;"></a> | <a onclick="copySelectInsurance()" style="cursor:pointer; color:blue;">Copy Selected</a> | <a onclick="copyAllTrInsurance()" style="cursor:pointer; color:blue;">Copy All</a> | <a onclick="deleteAllTrInsurance()" style="cursor:pointer; color:blue;">Delete Selected</a>
                </div>
                <span id="displayMessage" style="display:none"></span>
              <table id="add_plan_insurance" style="width: 100%; margin: 0 0;">
                <thead>
                    <tr>
                        <th>Term:<br/> <input type="radio" class="flat" name="TermSelectInsurance" id="TermDay" value="days" checked>Day <input type="radio" class="flat" name="TermSelectInsurance" id="TermMonth" value="months">Month</th>
                        <th>Class</th>
                        <th>Store Type</th>
						<th>Deductible</th>
                        <th>Price ($)</th>
                    </tr>
                </thead>
                <tbody id="plan-tBody" class="plan-tBody-insurance">
                    
                </tbody>
              </table> 
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <a class="btn btn-danger" href="plan_list.php">Cancel</a>
                  <button id="send" type="submit" class="btn btn-success">Save</button>
                </div>
              </div> 
            </div>
          </div>
          <div class="item form-group gap_category" style="display:none;">
            <div id="addTableFunction">
                <label class="col-md-2" for="email">
                  Add More Row                       
                </label>
                <div class="col-md-10">
                  <a class="fa fa-plus" onclick="addRowGap()" style="cursor:pointer; color:blue;"></a> | <a onclick="copySelectGap()" style="cursor:pointer; color:blue;">Copy Selected</a> | <a onclick="copyAllTrGap()" style="cursor:pointer; color:blue;">Copy All</a> | <a onclick="deleteAllTrGap()" style="cursor:pointer; color:blue;">Delete Selected</a>
                </div>
                <span id="displayMessage" style="display:none"></span>
              <table id="add_plan_gap" style="width: 100%; margin: 0 0;">
                <thead>
                    <tr>
                        <th>Term From:<br/> <input type="radio" class="flat" name="TermFromOption" value="days" checked>Day <input type="radio" class="flat" name="TermFromOption" value="months">Month</th>
                        <th>Term To:<br/> <input type="radio" class="flat" name="TermToOption" value="days" checked>Day <input type="radio" class="flat" name="TermToOption" id="TermMonth" value="months">Month</th>
                        <th>Class</th>
                        <th>Coverage Limit</th>
                        <th>Store Type</th>
                        <th>Payment</th>
                        <th>Price ($)</th>
                    </tr>
                </thead>
                <tbody id="plan-tBody" class="plan-tBody-gap">
                    
                </tbody>
              </table> 
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <a class="btn btn-danger" href="plan_list.php">Cancel</a>
                  <button id="send" type="submit" class="btn btn-success">Save</button>
                </div>
              </div> 
            </div>   
          </div>   
          <div class="item form-group anciallary_category" style="display:none;">
            <div id="addTableFunction">
                <label class="col-md-2" for="email">
                  Add More Row                       
                </label>
                <div class="col-md-10">
                  <a class="fa fa-plus" onclick="addRowAnciallary()" style="cursor:pointer; color:blue;"></a> | <a onclick="copySelectAnciallary()" style="cursor:pointer; color:blue;">Copy Selected</a> | <a onclick="copyAllTrAnciallary()" style="cursor:pointer; color:blue;">Copy All</a> | <a onclick="deleteAllTrAnciallary()" style="cursor:pointer; color:blue;">Delete Selected</a>
                </div>
                <span id="displayMessage" style="display:none"></span>
              <table id="add_plan_anciallary" style="width: 100%; margin: 0 0;">
                <thead>
                    <tr>
                        <th>Term:<br/> <input type="radio" class="flat" name="TermSelectAnciallary" id="TermDay" value="days" checked>Day <input type="radio" class="flat" name="TermSelectAnciallary" id="TermMonth" value="months">Month</th>
                        <th>Class</th>
                        <th>Coverage Limit</th>
                        <th>Store Type</th>
                        <th>Options</th>
                        <th>Payment</th>
                        <th>Price ($)</th>
                    </tr>
                </thead>
                <tbody id="plan-tBody" class="plan-tBody-anciallary">
                    
                </tbody>
              </table> 
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <a class="btn btn-danger" href="plan_list.php">Cancel</a>
                  <button id="send" type="submit" class="btn btn-success">Save</button>
                </div>
              </div> 
          </div>             
          </div>             
        </form>
      </div>
    </div>
</div>
<span class="addRow" style="display:none"></span>
<!-- footer content -->
<?php 
include_once('elements/footer.php');
?>
<!-- /footer content -->
<script>
var t, ti, ta, tg, vendorId = '', getPlanListData = [];
$(document).ready(function(){
	t = $('#add_plan_vsc').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        "filter":     false
    });
	ti = $('#add_plan_insurance').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        "filter":     false
    });
	tg = $('#add_plan_gap').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        "filter":     false
    });
	ta = $('#add_plan_anciallary').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        "filter":     false
    });
 
	// if(vendorId == '') {
	//   document.getElementById("addTableFunction").style.opacity = 0.6;
	// }
	//$(".fa-plus").off("click");
	$('#add_plan_vsc tbody').on( 'click', 'tr', function () {
		var trDomElement = this;
		$(this).find('td .copyRow').each(function() {
			if($(this)[0].checked){
				$(trDomElement).addClass('selected');
			} else {
				$(trDomElement).removeClass('selected');
			}
		});
	});
	$('#add_plan_insurance tbody').on( 'click', 'tr', function () {
		var trDomElement = this;
		$(this).find('td .copyRow').each(function() {
			if($(this)[0].checked){
				$(trDomElement).addClass('selected');
			} else {
				$(trDomElement).removeClass('selected');
			}
		});
	});
	$('#add_plan_gap tbody').on( 'click', 'tr', function () {
		var trDomElement = this;
		$(this).find('td .copyRow').each(function() {
			if($(this)[0].checked){
				$(trDomElement).addClass('selected');
			} else {
				$(trDomElement).removeClass('selected');
			}
		});
	});
	$('#add_plan_anciallary tbody').on( 'click', 'tr', function () {
		var trDomElement = this;
		$(this).find('td .copyRow').each(function() {
			if($(this)[0].checked){
				$(trDomElement).addClass('selected');
			} else {
				$(trDomElement).removeClass('selected');
			}
		});
	});
});
function addRow() {
    var rowCount = t.rows().count();
    //console.log(rowCount);
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
          +'<option value="" disabled selected>Select class</option>';
    if(getPlanListData.length) {
      getPlanListData.forEach(function(item){
        classSelectElement += '<option value="'+ item.id + '">' + item.class + '</option>';
      });
    }
    classSelectElement += '</select>';
    t.row.add( [
        '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
        classSelectElement,
        '<input type="number" class="dtElement" placeholder="Cover milage" name="coverMailage[]">',
        '<input type="number" class="dtElement" placeholder="Up to milage" name="upToMailage[]">',
        '<input type="checkbox" class="dtElement" name="manufacturerWarranty['+rowCount+']">',
        '<input type="number" class="dtElement" placeholder="Milage From" name="mailageFrom[]">',
        '<input type="number" class="dtElement" placeholder="Milage To" name="mailageTo[]">',
        '<input type="number" class="dtElement" placeholder="Price" name="price[]">',
        '<input type="number" class="dtElement" placeholder="Price" name="deductable[]">'
    ] ).draw( false );
  };
function addRowInsurance() {
    var rowCount = ti.rows().count();
    //console.log(rowCount);
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
          +'<option value="" disabled selected>Select class</option>';
    if(getPlanListData.length) {
      getPlanListData.forEach(function(item){
        classSelectElement += '<option value="'+ item.id + '">' + item.class + '</option>';
      });
    }
    classSelectElement += '</select>';
    ti.row.add( [
        '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
        classSelectElement,
        '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
        '<input type="number" class="dtElement" placeholder="Price" name="deductable[]">',
        '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
    ] ).draw( false );
  };
function addRowGap() {
    var rowCount = tg.rows().count();
    //console.log(rowCount);
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
      var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
            +'<option value="" disabled selected>Select class</option>';
      if(getPlanListData.length) {
        getPlanListData.forEach(function(item){
          classSelectElement += '<option value="'+ item.id + '">' + item.class + '</option>';
        });
      }
      classSelectElement += '</select>';
      tg.row.add([
          '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="termFrom[]" style="width: 70%;">',
          '<input type="number" class="dtElement" placeholder="" name="termTo[]" style="width: 70%;">',
          classSelectElement,
          'None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
          '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
          '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
          '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
        ] ).draw( false );
};
function addRowAnciallary() {
    var rowCount = ta.rows().count();
    //console.log(rowCount);
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
	var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
		+'<option value="" disabled selected>Select class</option>';
	if(getPlanListData.length) {
		getPlanListData.forEach(function(item){
		  classSelectElement += '<option value="'+ item.id + '">' + item.class + '</option>';
		});
	}
	classSelectElement += '</select>';
	ta.row.add([
          '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
          classSelectElement,
          'None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
          '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
          '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>',
          '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
          '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
	] ).draw( false );
};
function copyAllTr() {
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    $(".plan-tBody-vsc").find('tr').each(function(event) {
      var rowCount = t.rows().count();
      //console.log(rowCount);
      var dtElement = [], tdCount = 0;
      $(this).find('td .dtElement').each(function() {
        if(tdCount == 0){
          dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" name="term[]" placeholder="" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 1){
          var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
                +'<option value="" disabled selected>Select class</option>';
              
          var selectedID = $(this).val();
          if(getPlanListData.length) {
            getPlanListData.forEach(function(item){
              classSelectElement += '<option value="'+ item.id +'"';
              if(item.id == selectedID) {
                classSelectElement += ' selected>' + item.class + '</option>';
              } else {
                classSelectElement += '>' + item.class + '</option>';
              }
            });
          }
          classSelectElement += '</select>';
          dtElement.push(classSelectElement);
        } else if(tdCount == 2){
          dtElement.push('<input type="number" class="dtElement" placeholder="Cover milage" name="coverMailage[]" value="'+$(this).val()+'">');
        } else if(tdCount == 3){
          dtElement.push('<input type="number" class="dtElement" placeholder="Up to milage" name="upToMailage[]" value="'+$(this).val()+'">');
        } else if(tdCount == 4){
          if($(this).prop( "checked" )) {
            dtElement.push('<input type="checkbox" class="dtElement" name="manufacturerWarranty['+rowCount+']" checked>');
          } else {
            dtElement.push('<input type="checkbox" class="dtElement" name="manufacturerWarranty['+rowCount+']">');
          }
        } else if(tdCount == 5){
          dtElement.push('<input type="number" class="dtElement" placeholder="Milage From" name="mailageFrom[]" value="'+$(this).val()+'">');
        } else if(tdCount == 6){
          dtElement.push('<input type="number" class="dtElement" placeholder="Milage To" name="mailageTo[]" value="'+$(this).val()+'">');
        } else if(tdCount == 7){
          dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="price[]" value="'+$(this).val()+'">');
        } else if(tdCount == 8){
          dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="deductable[]" value="'+$(this).val()+'">');
        }
        tdCount ++;
      });
      if(dtElement.length > 0) t.row.add(dtElement).draw( false );
    });
  };
function copyAllTrInsurance() {
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    $(".plan-tBody-insurance").find('tr').each(function(event) {
      var rowCount = ti.rows().count();
      //console.log(rowCount);
      var dtElement = [], tdCount = 0;
      $(this).find('td .dtElement').each(function() {
        if(tdCount == 0){
          dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" name="term[]" placeholder="" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 1){
          var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
                +'<option value="" disabled selected>Select class</option>';
              
          var selectedID = $(this).val();
          if(getPlanListData.length) {
            getPlanListData.forEach(function(item){
              classSelectElement += '<option value="'+ item.id +'"';
              if(item.id == selectedID) {
                classSelectElement += ' selected>' + item.class + '</option>';
              } else {
                classSelectElement += '>' + item.class + '</option>';
              }
            });
          }
          classSelectElement += '</select>';
          dtElement.push(classSelectElement);
        } else if(tdCount == 2){
          var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          if($(this).val()=='Independent') {
           var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          } else if($(this).val()=='Franchise') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
          }  else if($(this).val()=='Both') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
          }
          dtElement.push(storeType);
        } else if(tdCount == 3){
          dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="deductable[]" value="'+$(this).val()+'">');
        } else if(tdCount == 4){
         dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="price[]" value="'+$(this).val()+'">');
        }
        tdCount ++;
      });
      if(dtElement.length > 0) ti.row.add(dtElement).draw( false );
    });
  };
function copyAllTrGap() {
	//console.log(rowCount);
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    $(".plan-tBody-gap").find('tr').each(function(event) {
      var rowCount = tg.rows().count();
      //console.log(rowCount);
      var dtElement = [], tdCount = 0;
      $(this).find('td .dtElement').each(function() {
        if(tdCount == 0){
          dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="termFrom[]" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 1){
          dtElement.push('<input type="number" class="dtElement" placeholder="" name="termTo[]" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 2){
          var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
                +'<option value="" disabled selected>Select class</option>';
              
          var selectedID = $(this).val();
          if(getPlanListData.length) {
            getPlanListData.forEach(function(item){
              classSelectElement += '<option value="'+ item.id +'"';
              if(item.id == selectedID) {
                classSelectElement += ' selected>' + item.class + '</option>';
              } else {
                classSelectElement += '>' + item.class + '</option>';
              }
            });
          }
          classSelectElement += '</select>';
          dtElement.push(classSelectElement);
        } else if(tdCount == 3){
          dtElement.push('None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;" value="'+$(this).val()+'">');
        } else if(tdCount == 4){
          var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          if($(this).val()=='Independent') {
           var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          } else if($(this).val()=='Franchise') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
          }  else if($(this).val()=='Both') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
          }
          dtElement.push(storeType);
        } else if(tdCount == 5){
          var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          if($(this).val()=='Lease') {
            var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease" selected>Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance" selected>Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash" selected>Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash & Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance" selected>Cash & Finance</option></select>';
          }
		  dtElement.push(payment);
        } else if(tdCount == 6){
          dtElement.push('<input type="number" class="dtElement" placeholder="" name="price[]" value="'+$(this).val()+'">');
        }
        tdCount ++;
      });
      if(dtElement.length > 0) tg.row.add(dtElement).draw( false );
    });
};
function copyAllTrAnciallary() {
	//console.log(rowCount);
    if(vendorId == ''){
      showMessage('Please select the vendor');
      return false;
    }
    $(".plan-tBody-anciallary").find('tr').each(function(event) {
      var rowCount = ta.rows().count();
      //console.log(rowCount);
      var dtElement = [], tdCount = 0;
      $(this).find('td .dtElement').each(function() {
        if(tdCount == 0){
          dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 1){
          var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
                +'<option value="" disabled selected>Select class</option>';
              
          var selectedID = $(this).val();
          if(getPlanListData.length) {
            getPlanListData.forEach(function(item){
              classSelectElement += '<option value="'+ item.id +'"';
              if(item.id == selectedID) {
                classSelectElement += ' selected>' + item.class + '</option>';
              } else {
                classSelectElement += '>' + item.class + '</option>';
              }
            });
          }
          classSelectElement += '</select>';
          dtElement.push(classSelectElement);
        } else if(tdCount == 2){
          dtElement.push('None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;" value="'+$(this).val()+'">');
        }  else if(tdCount == 3){
          var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          if($(this).val()=='Independent') {
           var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          } else if($(this).val()=='Franchise') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
          }  else if($(this).val()=='Both') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
          }
          dtElement.push(storeType);
        } else if(tdCount == 4){
          var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          if($(this).val()=='ALLOY WHEELS') {
           var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS" selected>ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='CHROME WHEELS') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS" selected>CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='BOTH') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH" selected>BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='NONE') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE" selected>NONE</option></select>';
          }
          dtElement.push(options);
        } else if(tdCount == 5){
          var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          if($(this).val()=='Lease') {
            var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease" selected>Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance" selected>Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash" selected>Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash & Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance" selected>Cash & Finance</option></select>';
          }
		  dtElement.push(payment);
        } else if(tdCount == 6){
          dtElement.push('<input type="number" class="dtElement" placeholder="" name="price[]" value="'+$(this).val()+'">');
        }
        tdCount ++;
      });
      if(dtElement.length > 0) ta.row.add(dtElement).draw( false );
    });
};
function copySelect() {
	if(vendorId == '') {
	  showMessage('Please select the vendor');
	  return false;
	}
	$(".plan-tBody-vsc").find('tr.selected').each(function(event) {
		var rowCount = t.rows().count();
		//console.log(rowCount);
		var dtElement = [], tdCount = 0;
		$(this).find('td .dtElement').each(function() {
			if(tdCount == 0){
			  dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" name="term[]" placeholder="" style="width: 70%;" value="'+$(this).val()+'">');
			} else if(tdCount == 1){
			  var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
					+'<option value="" disabled selected>Select class</option>';
				  
			  var selectedID = $(this).val();
			  if(getPlanListData.length) {
				getPlanListData.forEach(function(item){
				  classSelectElement += '<option value="'+ item.id +'"';
				  if(item.id == selectedID) {
					classSelectElement += ' selected>' + item.class + '</option>';
				  } else {
					classSelectElement += '>' + item.class + '</option>';
				  }
				});
			  }
			  classSelectElement += '</select>';
			  dtElement.push(classSelectElement);
			} else if(tdCount == 2){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Cover milage" name="coverMailage[]" value="'+$(this).val()+'">');
			} else if(tdCount == 3){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Up to milage" name="upToMailage[]" value="'+$(this).val()+'">');
			} else if(tdCount == 4){
			  if($(this).prop( "checked" )) {
				dtElement.push('<input type="checkbox" class="dtElement" name="manufacturerWarranty['+rowCount+']" checked>');
			  } else {
				dtElement.push('<input type="checkbox" class="dtElement" name="manufacturerWarranty['+rowCount+']">');
			  }
			} else if(tdCount == 5){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Milage From" name="mailageFrom[]" value="'+$(this).val()+'">');
			} else if(tdCount == 6){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Milage To" name="mailageTo[]" value="'+$(this).val()+'">');
			} else if(tdCount == 7){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="price[]" value="'+$(this).val()+'">');
			} else if(tdCount == 8){
			  dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="deductable[]" value="'+$(this).val()+'">');
			}
			tdCount ++;
		});
		if(dtElement.length > 0) t.row.add(dtElement).draw( false );
	});
};
function copySelectInsurance() {
	if(vendorId == '') {
	  showMessage('Please select the vendor');
	  return false;
	}
	$(".plan-tBody-insurance").find('tr.selected').each(function(event) {
		var rowCount = ti.rows().count();
		//console.log(rowCount);
		var dtElement = [], tdCount = 0;
		$(this).find('td .dtElement').each(function() {
			if(tdCount == 0){
			  dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" name="term[]" placeholder="" style="width: 70%;" value="'+$(this).val()+'">');
			} else if(tdCount == 1){
			  var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
					+'<option value="" disabled selected>Select class</option>';
				  
			  var selectedID = $(this).val();
			  if(getPlanListData.length) {
				getPlanListData.forEach(function(item){
				  classSelectElement += '<option value="'+ item.id +'"';
				  if(item.id == selectedID) {
					classSelectElement += ' selected>' + item.class + '</option>';
				  } else {
					classSelectElement += '>' + item.class + '</option>';
				  }
				});
			  }
			  classSelectElement += '</select>';
			  dtElement.push(classSelectElement);
			} else if(tdCount == 2){
          var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          if($(this).val()=='Independent') {
           var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          } else if($(this).val()=='Franchise') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
          }  else if($(this).val()=='Both') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
          }
          dtElement.push(storeType);
        } else if(tdCount == 3){
          dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="deductable[]" value="'+$(this).val()+'">');
        } else if(tdCount == 4){
         dtElement.push('<input type="number" class="dtElement" placeholder="Price" name="price[]" value="'+$(this).val()+'">');
        }
			tdCount ++;
		});
		if(dtElement.length > 0) ti.row.add(dtElement).draw( false );
	});
};
function copySelectGap() {
	if(vendorId == '') {
	  showMessage('Please select the vendor');
	  return false;
	}
	$(".plan-tBody-gap").find('tr.selected').each(function(event) {
		var rowCount = tg.rows().count();
		//console.log(rowCount);
		var dtElement = [], tdCount = 0;
		$(this).find('td .dtElement').each(function() {
			if(tdCount == 0){
			  dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="termFrom[]" style="width: 70%;" value="'+$(this).val()+'">');
			} else if(tdCount == 1){
			  dtElement.push('<input type="number" class="dtElement" placeholder="" name="termTo[]" style="width: 70%;" value="'+$(this).val()+'">');
			} else if(tdCount == 2){
			  var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
					+'<option value="" disabled selected>Select class</option>';
				  
			  var selectedID = $(this).val();
			  if(getPlanListData.length) {
				getPlanListData.forEach(function(item){
				  classSelectElement += '<option value="'+ item.id +'"';
				  if(item.id == selectedID) {
					classSelectElement += ' selected>' + item.class + '</option>';
				  } else {
					classSelectElement += '>' + item.class + '</option>';
				  }
				});
			  }
			  classSelectElement += '</select>';
			  dtElement.push(classSelectElement);
			} else if(tdCount == 3){
			  dtElement.push('None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;" value="'+$(this).val()+'">');
			} else if(tdCount == 4){
			  var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
			  if($(this).val()=='Independent') {
			   var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
			  } else if($(this).val()=='Franchise') {
				var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
			  }  else if($(this).val()=='Both') {
				var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
			  }
			  dtElement.push(storeType);
			} else if(tdCount == 5){
			  var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
			  if($(this).val()=='Lease') {
				var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease" selected>Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
			  } else if($(this).val()=='Finance') {
				var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance" selected>Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
			  } else if($(this).val()=='Cash') {
				var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash" selected>Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
			  } else if($(this).val()=='Cash & Finance') {
				var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance" selected>Cash & Finance</option></select>';
			  }
			  dtElement.push(payment);
			} else if(tdCount == 6){
			  dtElement.push('<input type="number" class="dtElement" placeholder="" name="price[]" value="'+$(this).val()+'">');
			}
			tdCount ++;
		});
		if(dtElement.length > 0) tg.row.add(dtElement).draw( false );
	});
};
function copySelectAnciallary() {
	if(vendorId == '') {
	  showMessage('Please select the vendor');
	  return false;
	}
	$(".plan-tBody-anciallary").find('tr.selected').each(function(event) {
		var rowCount = ta.rows().count();
		//console.log(rowCount);
		var dtElement = [], tdCount = 0;
		$(this).find('td .dtElement').each(function() {
			if(tdCount == 0){
          dtElement.push('<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;" value="'+$(this).val()+'">');
        } else if(tdCount == 1){
          var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
                +'<option value="" disabled selected>Select class</option>';
              
          var selectedID = $(this).val();
          if(getPlanListData.length) {
            getPlanListData.forEach(function(item){
              classSelectElement += '<option value="'+ item.id +'"';
              if(item.id == selectedID) {
                classSelectElement += ' selected>' + item.class + '</option>';
              } else {
                classSelectElement += '>' + item.class + '</option>';
              }
            });
          }
          classSelectElement += '</select>';
          dtElement.push(classSelectElement);
        } else if(tdCount == 2){
          dtElement.push('None <input type="checkbox" class="dtElement2" name="coverageLimitNone['+rowCount+']"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;" value="'+$(this).val()+'">');
        }  else if(tdCount == 3){
          var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          if($(this).val()=='Independent') {
           var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent" selected>independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>';
          } else if($(this).val()=='Franchise') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise" selected>Franchise</option><option value="Both">Both</option></select>';
          }  else if($(this).val()=='Both') {
            var storeType = '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both" selected>Both</option></select>';
          }
          dtElement.push(storeType);
        } else if(tdCount == 4){
          var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          if($(this).val()=='ALLOY WHEELS') {
           var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS" selected>ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='CHROME WHEELS') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS" selected>CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='BOTH') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH" selected>BOTH</option><option value="NONE">NONE</option></select>';
          } else if($(this).val()=='NONE') {
            var options = '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE" selected>NONE</option></select>';
          }
          dtElement.push(options);
        } else if(tdCount == 5){
          var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          if($(this).val()=='Lease') {
            var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease" selected>Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance" selected>Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash" selected>Cash</option><option value="Cash & Finance">Cash & Finance</option></select>';
          } else if($(this).val()=='Cash & Finance') {
			var payment = '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance" selected>Cash & Finance</option></select>';
          }
		  dtElement.push(payment);
        } else if(tdCount == 6){
          dtElement.push('<input type="number" class="dtElement" placeholder="" name="price[]" value="'+$(this).val()+'">');
        }
        tdCount ++;
		});
		if(dtElement.length > 0) ta.row.add(dtElement).draw( false );
	});
};
function deleteAllTr() {
	$(".plan-tBody-vsc").find('tr.selected').each(function(event) {
       t.row($(this)).remove().draw( false );
	});
}
function deleteAllTrInsurance() {
	$(".plan-tBody-insurance").find('tr.selected').each(function(event) {
       ti.row($(this)).remove().draw( false );
	});
}
function deleteAllTrGap() {
	$(".plan-tBody-gap").find('tr.selected').each(function(event) {
       tg.row($(this)).remove().draw( false );
	});
}
function deleteAllTrAnciallary() {
	$(".plan-tBody-anciallary").find('tr.selected').each(function(event) {
       ta.row($(this)).remove().draw( false );
	});
}
function add_plan() {
    $("#plan-tBody").find('tr.selected').each(function(event) {
      t.row($(this)).remove().draw( false );
    });
  };
var messageTimeObj = null;
function showMessage(message, className, timeout){
  clearTimeout(messageTimeObj);
  message = message || '';
  className = className || 'error';
  timeout = timeout || 3000;
  var messageElement = $("#displayMessage");
  messageElement.addClass(className);
  messageElement.text(message);
  document.getElementById("displayMessage").style.display = 'block';
  messageTimeObj = setTimeout(function() {
    messageElement.removeClass(className);
    messageElement.text('');
    document.getElementById("displayMessage").style.display = 'none';
  }, timeout);
}
function fnRemoveRow(e) {
  //console.log(vendorId);
  if(vendorId == ''){
    showMessage('Please select the vendor');
    return false;
  }
  var newRow = $(e.target).parents('tr');
  t.row(newRow).remove().draw( false );
}
function fnCopyRow(e) {
  var newRow = $(e.target).parents('tr');
  var cells = newRow[0].getElementsByTagName('td');
}
function show_class_by_category(e) {
    var val = $(e.target).val();
    $.ajax({
        method: "POST",
        url: "class_vendor_option-for-select.php",
        data: { cat : val }
    }).done(function(data) {
        $('.vendor_list_filter').html(data);
		var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
            +'<option value="">Select class</option></select>';
        if(val == 'VSC') {
			$('.vsc_category').show();
			$('.gap_category').hide();
			$('.anciallary_category').hide();
			$('.insurance_category').hide();
			$('.sub_category').hide();
			$(".plan-tBody-vsc").find('tr').each(function(event) {
				t.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-insurance").find('tr').each(function(event) {
				ti.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-gap").find('tr').each(function(event) {
				tg.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-anciallary").find('tr').each(function(event) {
				ta.row($(this)).remove().draw( false );
			});
			t.row.add([
			  '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
			  classSelectElement,
			  '<input type="number" class="dtElement" placeholder="Cover milage" name="coverMailage[]">',
			  '<input type="number" class="dtElement" placeholder="Up to milage" name="upToMailage[]">',
			  '<input type="checkbox" class="dtElement" name="manufacturerWarranty[0]">',
			  '<input type="number" class="dtElement" placeholder="Milage From" name="mailageFrom[]">',
			  '<input type="number" class="dtElement" placeholder="Milage To" name="mailageTo[]">',
			  '<input type="number" class="dtElement" placeholder="Price" name="price[]">',
			  '<input type="number" class="dtElement" placeholder="Price" name="deductable[]">'
			] ).draw( false );

        } else if(val == 'INSURANCE') {
			$('.insurance_category').show();
			$('.vsc_category').hide();
			$('.gap_category').hide();
			$('.anciallary_category').hide();
			$('.sub_category').hide();
			$(".plan-tBody-vsc").find('tr').each(function(event) {
				t.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-insurance").find('tr').each(function(event) {
				ti.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-gap").find('tr').each(function(event) {
				tg.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-anciallary").find('tr').each(function(event) {
				ta.row($(this)).remove().draw( false );
			});
			ti.row.add( [
				'<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
				classSelectElement,
				'<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
				'<input type="number" class="dtElement" placeholder="Price" name="deductable[]">',
				'<input type="number" class="dtElement" placeholder="Price" name="price[]">'
			] ).draw( false );
        } else if(val == 'GAP') {
			$('.vsc_category').hide();
			$('.gap_category').show();
			$('.anciallary_category').hide();
			$('.insurance_category').hide();
			$('.sub_category').hide();
			$(".plan-tBody-vsc").find('tr').each(function(event) {
				t.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-insurance").find('tr').each(function(event) {
				ti.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-gap").find('tr').each(function(event) {
				tg.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-anciallary").find('tr').each(function(event) {
				ta.row($(this)).remove().draw( false );
			});
			tg.row.add([
			  '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="termFrom[]" style="width: 70%;">',
			  '<input type="number" class="dtElement" placeholder="" name="termTo[]" style="width: 70%;">',
			  classSelectElement,
			  'None <input type="checkbox" class="dtElement2" name="coverageLimitNone[0]"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
			  '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
			  '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
			  '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
			] ).draw( false );
        } else if(val == 'ANCILLARY') {
			$('.vsc_category').hide();
			$('.gap_category').hide();
			$('.insurance_category').hide();
			$('.anciallary_category').show();
			$('.sub_category').show();
			$(".plan-tBody-vsc").find('tr').each(function(event) {
				t.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-insurance").find('tr').each(function(event) {
				ti.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-gap").find('tr').each(function(event) {
				tg.row($(this)).remove().draw( false );
			});
			$(".plan-tBody-anciallary").find('tr').each(function(event) {
				ta.row($(this)).remove().draw( false );
			});
			ta.row.add([
			  '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
			  classSelectElement,
			  'None <input type="checkbox" class="dtElement2" name="coverageLimitNone[0]"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
			  '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
			  '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>',
			  '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
			  '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
		] ).draw( false );
        }
    });
}
var countVenderClick = 0;
function show_class_by_vendor(e) {
  // var cat = $('.cat_id').val();
  // var val = $(e.target).val();
  vendorId = $(e.target).val();
  if(vendorId == '') {
    countVenderClick = 0;
    document.getElementById("addTableFunction").style.opacity = 0.6;
  } else {
    countVenderClick ++;
    document.getElementById("addTableFunction").style.opacity = 1;
    var e = document.getElementById("vendor");
    $.get("plan_query.php?api=getPlanListData&vendor_id="+vendorId, function(data, status) {
      data = JSON.parse(data);
      getPlanListData = data.classList;
      var classSelectElement = '<select class="dtElement" id="vendor" name="class[]">'
            +'<option value="">Select class</option>';
      if(getPlanListData.length) {
        getPlanListData.forEach(function(item){
          classSelectElement += '<option value="'+ item.id + '">' + item.class + '</option>';
        });
      }
      classSelectElement += '</select>';
      var val = $('.cat_id').val();
      if(val == 'VSC') {
        $(".plan-tBody-vsc").find('tr').each(function(event) {
          t.row($(this)).remove().draw( false );
		  //console.log('as');
        });
        t.row.add([
          '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
          classSelectElement,
          '<input type="number" class="dtElement" placeholder="Cover milage" name="coverMailage[]">',
          '<input type="number" class="dtElement" placeholder="Up to milage" name="upToMailage[]">',
          '<input type="checkbox" class="dtElement" name="manufacturerWarranty[0]">',
          '<input type="number" class="dtElement" placeholder="Milage From" name="mailageFrom[]">',
          '<input type="number" class="dtElement" placeholder="Milage To" name="mailageTo[]">',
          '<input type="number" class="dtElement" placeholder="Price" name="price[]">',
          '<input type="number" class="dtElement" placeholder="Price" name="deductable[]">'
        ] ).draw( false );
      
      } else if(val == 'INSURANCE') {
        $(".plan-tBody-insurance").find('tr').each(function(event) {
          ti.row($(this)).remove().draw( false );
		  //console.log('as');
        });
		ti.row.add( [
			'<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
			classSelectElement,
			'<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
			'<input type="number" class="dtElement" placeholder="Price" name="deductable[]">',
			'<input type="number" class="dtElement" placeholder="Price" name="price[]">'
		] ).draw( false );      
      } else if(val == 'GAP') {
        $(".plan-tBody-gap").find('tr').each(function(event) {
          tg.row($(this)).remove().draw( false );
        });
        tg.row.add([
          '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="termFrom[]" style="width: 70%;">',
          '<input type="number" class="dtElement" placeholder="" name="termTo[]" style="width: 70%;">',
          classSelectElement,
          'None <input type="checkbox" class="dtElement2" name="coverageLimitNone[0]"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
          '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
          '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
          '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
        ] ).draw( false );
      
      } else if(val == 'ANCILLARY') {
        $(".plan-tBody-anciallary").find('tr').each(function(event) {
          ta.row($(this)).remove().draw( false );
        });
        ta.row.add([
			  '<input type="checkbox" class="copyRow"> <input type="number" class="dtElement" placeholder="" name="term[]" style="width: 70%;">',
			  classSelectElement,
			  'None <input type="checkbox" class="dtElement2" name="coverageLimitNone[0]"> | Amount <input type="number" class="dtElement" placeholder="" name="coverageLimit[]" style="width: 30%;">',
			  '<select class="dtElement" name="storeType[]"><option value="">Select</option><option value="Independent">independent</option><option value="Franchise">Franchise</option><option value="Both">Both</option></select>',
			  '<select class="dtElement" name="options[]"><option value="">Select</option><option value="ALLOY WHEELS">ALLOY WHEELS</option><option value="CHROME WHEELS">CHROME WHEELS</option><option value="BOTH">BOTH</option><option value="NONE">NONE</option></select>',
			  '<select class="dtElement" name="payment[]"><option value="">Select</option><option value="Lease">Lease</option><option value="Finance">Finance</option><option value="Cash">Cash</option><option value="Cash & Finance">Cash & Finance</option></select>',
			  '<input type="number" class="dtElement" placeholder="Price" name="price[]">'
		] ).draw( false );
      }
    });
  }
}
</script>