<?php
include_once('elements/db_connection.php');
//echo '<pre>';
//print_r($_REQUEST['model']);
//print_r($_REQUEST['model_range']);
//exit;
$name = strtoupper($_REQUEST['name']);
$vendor_id = $_REQUEST['vendor_id'];
$general_range = $_REQUEST['general_range'];
$restrict_class_id = $_REQUEST['restrict_class_id'];
$warrenty_goes_backup = strtoupper($_REQUEST['warrenty_goes_backup']);
$sql = "UPDATE restrict_class SET warrenty_goes_backup= '$warrenty_goes_backup',name= '$name', vendor_id= '$vendor_id', general_range= '$general_range' WHERE id = '$restrict_class_id'";
mysqli_query($conn, $sql);
$sql = "DELETE FROM restrict_class_mapping_for_car_make WHERE restrict_class_id = '$restrict_class_id'";
mysqli_query($conn, $sql);
if(isset($_REQUEST['make'])) {
    $make = $_REQUEST['make'];
    foreach($make as $mk) {
        $make_id = $mk;
        $make_range = $_REQUEST['make_range'][$make_id];
        $sql = "INSERT INTO restrict_class_mapping_for_car_make (restrict_class_id, make_id, make_range) VALUES ('$restrict_class_id', '$make_id', '$make_range')";
        mysqli_query($conn, $sql);
    }
}
$sql = "DELETE FROM restrict_class_mapping_for_car_model WHERE restrict_class_id = '$restrict_class_id'";
mysqli_query($conn, $sql);
if(isset($_REQUEST['model'])) {
    $model = $_REQUEST['model'];
    foreach($model as $ml) {
        $model_id = $ml;
        $model_range = $_REQUEST['model_range'][$model_id];
        $sql = "INSERT INTO restrict_class_mapping_for_car_model (restrict_class_id, model_id, model_range) VALUES ('$restrict_class_id', '$model_id', '$model_range')";
        mysqli_query($conn, $sql);
    }
}
$sql = "DELETE FROM restrict_class_mapping_for_car_trim WHERE restrict_class_id = '$restrict_class_id'";
mysqli_query($conn, $sql);
if(isset($_REQUEST['trim'])) {
    $trim = $_REQUEST['trim'];
    foreach($trim as $tr) {
        $trim_id = $tr;
        $trim_range = $_REQUEST['trim_range'][$trim_id];
        $sql = "INSERT INTO restrict_class_mapping_for_car_trim (restrict_class_id, trim_id, trim_range) VALUES ('$restrict_class_id', '$trim_id', '$trim_range')";
        mysqli_query($conn, $sql);
    }
}
$sql = "DELETE FROM restrict_engine_type_mapping_for_vendor WHERE restrict_class_id = '$restrict_class_id'";
mysqli_query($conn, $sql);

$sqlEType = "SELECT * FROM master_engine_type ORDER BY name ASC";
$resultEType = mysqli_query($conn, $sqlEType);
// output data of each row
while($rowEType = mysqli_fetch_assoc($resultEType)) {
    $engine_type_id = $rowEType['id'];
    if(isset($_REQUEST['eType']) && isset($_REQUEST['eType'][$engine_type_id])) {
        $sql = "INSERT INTO restrict_engine_type_mapping_for_vendor (restrict_class_id, engine_type_restricted, engine_type_id) VALUES ('$restrict_class_id', 'yes', '$engine_type_id')";
        mysqli_query($conn, $sql);
    } else {
        $surcharge = null;
        if(isset($_REQUEST['eTypeSurCharge']) && isset($_REQUEST['eTypeSurCharge'][$engine_type_id])) {
            $surcharge = $_REQUEST['eTypeSurCharge'][$engine_type_id];
        }
        $sql = "INSERT INTO restrict_engine_type_mapping_for_vendor (restrict_class_id, engine_type_restricted, engine_type_id, surcharge) VALUES ('$restrict_class_id', 'no', '$engine_type_id', '$surcharge')";
        mysqli_query($conn, $sql);
    }
}
header('Location: restrict_class_list.php');
?>