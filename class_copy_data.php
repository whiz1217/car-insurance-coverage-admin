<?php 
    include_once('elements/db_connection.php');
	$classId = $_REQUEST['class_id'];
	$vendorId = 0;
	$sql = "SELECT restrict_class.vendor_id, restrict_class.warrenty_goes_backup,restrict_class.general_range, user_detail.category FROM restrict_class LEFT JOIN user_detail ON user_detail.user_id = restrict_class.vendor_id WHERE restrict_class.id=".$classId.";";
	$result = mysqli_query($conn, $sql);
	while($row = mysqli_fetch_assoc($result)) {
		$vendorId = $row['vendor_id'];
		$cat = $row['category'];
		$general_range = $row['general_range'];
		$warrenty_goes_backup = $row['warrenty_goes_backup'];
	}
?>
		<div class="item form-group">
			<label class="col-md-12 tree_model_other" style="cursor: pointer;">
				Restriction To Apply
			</label>
		</div>
		<div id="toggleId" style="display:none;">
			<span class="col-md-12">
				1. Checked means restrict for all year modal.<br />
				2. Checked and year range given means restrict for given year.<br />
			</span>
			<div class="clearfix"></div>
            <ul class="tree_new tree_model">
            <?php 
            $sqlCMap = "SELECT restrict_class_mapping_for_car_make.make_id, master_car_make.name, restrict_class_mapping_for_car_make.make_range 
			FROM restrict_class_mapping_for_car_make 
			LEFT JOIN master_car_make ON restrict_class_mapping_for_car_make.make_id = master_car_make.id 
			WHERE restrict_class_mapping_for_car_make.restrict_class_id = $classId";
            $resultCMap = mysqli_query($conn, $sqlCMap);
            $arData = array();
            while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
                $make_id = $rowCMap['make_id'];
                $arData[$make_id] = array();
                $arData[$make_id]['name'] = $rowCMap['name'];
                $arData[$make_id]['range'] = $rowCMap['make_range'];
                $arData[$make_id]['model'] = array();
            }
            $sqlCMap = "SELECT restrict_class_mapping_for_car_model.model_id, master_car_model.model_name, master_car_model.model_make_id, restrict_class_mapping_for_car_model.model_range FROM restrict_class_mapping_for_car_model LEFT JOIN master_car_model ON restrict_class_mapping_for_car_model.model_id = master_car_model.id WHERE restrict_class_mapping_for_car_model.restrict_class_id = $classId";
            $resultCMap = mysqli_query($conn, $sqlCMap);
            while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
                $model_make_id = $rowCMap['model_make_id'];
                $sqlMake = "SELECT * FROM master_car_make WHERE name = '$model_make_id'";
                $resultMak = mysqli_query($conn, $sqlMake);
                while($rowMak = mysqli_fetch_assoc($resultMak)) {
                    $make_id = $rowMak['id'];
                    $model_id = $rowCMap['model_id'];
                    if(!isset($arData[$make_id])) {
                        $arData[$make_id] = array();
                        $arData[$make_id]['name'] = $rowMak['name'];
                        $arData[$make_id]['model'] = array();
                    }
                    $arData[$make_id]['model'][$model_id] = array();
                    $arData[$make_id]['model'][$model_id]['name'] = $rowCMap['model_name'];
                    $arData[$make_id]['model'][$model_id]['range'] = $rowCMap['model_range'];
                    $arData[$make_id]['model'][$model_id]['trim'] = array();
                }
            }
            $sqlCMap = "SELECT restrict_class_mapping_for_car_trim.trim_id, master_car_trim.model_trim, master_car_trim.model_make_id, master_car_trim.model_name, restrict_class_mapping_for_car_trim.trim_range FROM restrict_class_mapping_for_car_trim LEFT JOIN master_car_trim ON restrict_class_mapping_for_car_trim.trim_id = master_car_trim.id WHERE restrict_class_mapping_for_car_trim.restrict_class_id = $classId";
            $resultCMap = mysqli_query($conn, $sqlCMap);
            while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
                $model_make_id = $rowCMap['model_make_id'];
                $sqlMake = "SELECT * FROM master_car_make WHERE name = '$model_make_id'";
                $resultMak = mysqli_query($conn, $sqlMake);
                while($rowMak = mysqli_fetch_assoc($resultMak)) {
                    $make_id = $rowMak['id'];
                    $model_name = $rowCMap['model_name'];
                    $make_name = $rowCMap['model_make_id'];
                    $trim_id = $rowCMap['trim_id'];
                    $sqlModel = "SELECT * FROM master_car_model WHERE model_name = '$model_name' AND model_make_id = '$make_name'";
                    $resultModel = mysqli_query($conn, $sqlModel);
                    while($rowModel = mysqli_fetch_assoc($resultModel)) {
                        $model_id = $rowModel['id'];
                        if(!isset($arData[$make_id])) {
                            $arData[$make_id] = array();
                            $arData[$make_id]['name'] = $rowMak['name'];
                            $arData[$make_id]['model'] = array();
                        }
                        if(!isset($arData[$make_id]['model'][$model_id])) {
                            $arData[$make_id]['model'][$model_id] = array();
                            $arData[$make_id]['model'][$model_id]['name'] = $rowModel['model_name'];
                            $arData[$make_id]['model'][$model_id]['trim'] = array();
                        }
                        $arData[$make_id]['model'][$model_id]['trim'][$trim_id] = array();
                        $arData[$make_id]['model'][$model_id]['trim'][$trim_id]['name'] = $rowCMap['model_trim'];
                        $arData[$make_id]['model'][$model_id]['trim'][$trim_id]['range'] = $rowCMap['trim_range'];
                    }
                }
            }
            //echo '<pre>';
            //print_r($arData);
            //exit;

            $sql = "SELECT * FROM master_car_make WHERE restrictedByDefault = 'no' ORDER BY name ASC";
            $result = mysqli_query($conn, $sql);
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {
                $makeSelected = false;
                $makeRange = '';
                $modelData = array();
                if(!empty($arData)) {
                    foreach($arData as $val) {
                        if($val['name'] == $row['name']) {
                            $makeSelected = true;
                            if(isset($val['range']))
                            $makeRange = $val['range'];
                            $modelData = $val['model'];
                        }
                    }
                }
            ?>
            <li class="has model row">
                <span class="col-md-2">
                <input type="checkbox" class="model_checkbox" name="make[]" value="<?php echo $row['id'];?>" <?php if($makeSelected){ ?> checked <?php } ?>>
                <label class="model_label"><?php echo $row['name'];?></label>
                </span>
                <span class="col-md-10" style="padding-bottom: 20px;">
                <input class="form-control" value="<?php echo $makeRange;?>" style="width:30%" placeholder="Year range (eg. 2009, 2010-2017)" name="make_range[<?php echo $row['id'];?>]" type="text">
                </span>
                <br />
                <ul class="tree_new tree_make">
                <?php 
                $make_id = $row['make_id'];
                $sqlMake = "SELECT * FROM master_car_model 
				WHERE model_make_id = '$make_id'
				ORDER BY model_name ASC";
                $resultMake = mysqli_query($conn, $sqlMake);
                // output data of each row
                while($rowMake = mysqli_fetch_assoc($resultMake)) {
                    $modelSelected = false;
                    $modelRange = '';
                    $trimData = array();
                    if(!empty($modelData)) {
                        foreach($modelData as $val) {
                            //echo '<pre>';
                            //print_r($val);
                            //exit;
                            if($val['name'] == $rowMake['model_name']) {
                                $modelSelected = true;
                                if(isset($val['range']))
                                $modelRange = $val['range'];
                                $trimData = $val['trim'];
                            }
                        }
                    }
                ?>
                <li class="has make_li row">
                    <span class="col-md-2">
                    <input type="checkbox" class="make_checkbox" name="model[]" value="<?php echo $rowMake['id'];?>" <?php if($modelSelected){ ?> checked <?php } ?>>
                    <label class="make_label"><?php echo $rowMake['model_name'];?></label>
                    </span>
                    <span class="col-md-10" style="padding-bottom: 20px;">
                    <input class="form-control" value="<?php echo $modelRange;?>" style="width:30%" placeholder="Year range (eg. 2009, 2010-2017)" name="model_range[<?php echo $rowMake['id'];?>]" type="text">
                    </span>
                    <ul class="make">
                    <?php 
                    $model_name = $rowMake['model_name'];
                    $sqlTrim = "SELECT * FROM master_car_trim WHERE model_make_id = '$make_id' AND model_name = '$model_name' ORDER BY model_trim ASC";
                    $resultTrim = mysqli_query($conn, $sqlTrim);
                    // output data of each row
                    while($rowTrim = mysqli_fetch_assoc($resultTrim)) {
                        $trimSelected = false;
                        $trimRange = '';
                        if(!empty($trimData)) {
                            foreach($trimData as $val) {
                                if($val['name'] == $rowTrim['model_trim']) {
                                    $trimSelected = true;
                                    if(isset($val['range']))
                                    $trimRange = $val['range'];
                                }
                            }
                        }
                    ?>
                    <li class="row">
                        <span class="col-md-6">
                        <input type="checkbox" name="trim[]" value="<?php echo $rowTrim['id'];?>" <?php if($trimSelected){ ?> checked <?php } ?>>
                        <label><?php echo $rowTrim['model_trim'];?></label>
                        </span>
                        <span class="col-md-4" style="padding-bottom: 20px;">
                        <input id="name" value="<?php echo $trimRange;?>" class="form-control" placeholder="Year range (eg. 2009, 2010-2017)" name="trim_range[<?php echo $rowTrim['id'];?>]" type="text">
                        </span>
                    </li>
                    <?php } ?>
                    </ul>
                </li>
                <?php } ?>
                </ul>
            </li>
                <?php } ?>
		</ul>
		</div>
		<div class="clearfix"></div>
		<div class="item form-group">
			<label class="col-md-12 tree_model_other_two" style="cursor: pointer;">
				Restricted By Default
			</label>
		</div>
		<div class="clearfix"></div>
		<ul class="tree_new tree_model" id="toggleIdTwo" style="display:none;">
			<?php
			$sql = "SELECT * FROM master_car_make WHERE restrictedByDefault = 'yes' ORDER BY name ASC";
            $result = mysqli_query($conn, $sql);
            // output data of each row
            while($row = mysqli_fetch_assoc($result)) {
                $makeSelected = false;
                $makeRange = '';
                $modelData = array();
                if(!empty($arData)) {
                    foreach($arData as $val) {
                        if($val['name'] == $row['name']) {
                            $makeSelected = true;
                            if(isset($val['range']))
                            $makeRange = $val['range'];
                            $modelData = $val['model'];
                        }
                    }
                }
            ?>
            <li class="has model row">
                <span class="col-md-2">
                <input type="checkbox" class="model_checkbox" name="make[]" value="<?php echo $row['id'];?>" <?php if($makeSelected){ ?> checked <?php } ?>>
                <label class="model_label"><?php echo $row['name'];?></label>
                </span>
                <span class="col-md-10" style="padding-bottom: 20px;">
                <input class="form-control" value="<?php echo $makeRange;?>" style="width:30%" placeholder="Year range (eg. 2009, 2010-2017)" name="make_range[<?php echo $row['id'];?>]" type="text">
                </span>
                <br />
                <ul class="tree_new tree_make">
                <?php 
                $make_id = $row['make_id'];
                $sqlMake = "SELECT * FROM master_car_model 
				WHERE model_make_id = '$make_id'
				ORDER BY model_name ASC";
                $resultMake = mysqli_query($conn, $sqlMake);
                // output data of each row
                while($rowMake = mysqli_fetch_assoc($resultMake)) {
                    $modelSelected = false;
                    $modelRange = '';
                    $trimData = array();
                    if(!empty($modelData)) {
                        foreach($modelData as $val) {
                            //echo '<pre>';
                            //print_r($val);
                            //exit;
                            if($val['name'] == $rowMake['model_name']) {
                                $modelSelected = true;
                                if(isset($val['range']))
                                $modelRange = $val['range'];
                                $trimData = $val['trim'];
                            }
                        }
                    }
                ?>
                <li class="has make_li row">
                    <span class="col-md-2">
                    <input type="checkbox" class="make_checkbox" name="model[]" value="<?php echo $rowMake['id'];?>" <?php if($modelSelected){ ?> checked <?php } ?>>
                    <label class="make_label"><?php echo $rowMake['model_name'];?></label>
                    </span>
                    <span class="col-md-10" style="padding-bottom: 20px;">
                    <input class="form-control" value="<?php echo $modelRange;?>" style="width:30%" placeholder="Year range (eg. 2009, 2010-2017)" name="model_range[<?php echo $rowMake['id'];?>]" type="text">
                    </span>
                    <ul class="make">
                    <?php 
                    $model_name = $rowMake['model_name'];
                    $sqlTrim = "SELECT * FROM master_car_trim WHERE model_make_id = '$make_id' AND model_name = '$model_name' ORDER BY model_trim ASC";
                    $resultTrim = mysqli_query($conn, $sqlTrim);
                    // output data of each row
                    while($rowTrim = mysqli_fetch_assoc($resultTrim)) {
                        $trimSelected = false;
                        $trimRange = '';
                        if(!empty($trimData)) {
                            foreach($trimData as $val) {
                                if($val['name'] == $rowTrim['model_trim']) {
                                    $trimSelected = true;
                                    if(isset($val['range']))
                                    $trimRange = $val['range'];
                                }
                            }
                        }
                    ?>
                    <li class="row">
                        <span class="col-md-6">
                        <input type="checkbox" name="trim[]" value="<?php echo $rowTrim['id'];?>" <?php if($trimSelected){ ?> checked <?php } ?>>
                        <label><?php echo $rowTrim['model_trim'];?></label>
                        </span>
                        <span class="col-md-4" style="padding-bottom: 20px;">
                        <input id="name" value="<?php echo $trimRange;?>" class="form-control" placeholder="Year range (eg. 2009, 2010-2017)" name="trim_range[<?php echo $rowTrim['id'];?>]" type="text">
                        </span>
                    </li>
                    <?php } ?>
                    </ul>
                </li>
                <?php } ?>
                </ul>
            </li>
                <?php } ?>
		</ul>
		<div class="clearfix"></div>
		<div class="item form-group">
			<label class="col-md-12" onclick="toggleEngine('commercialRestriction')" style="cursor:pointer;">Commercial Restriction</label>
		</div>
		<?php 
		$sqlEType = "SELECT met.id, 
		met.name, 
		retmfv.engine_type_restricted,
		retmfv.engine_type_id,
		retmfv.surcharge 
		FROM master_engine_type as met LEFT JOIN 
		restrict_engine_type_mapping_for_vendor as retmfv
		ON met.id = retmfv.engine_type_id 
		WHERE retmfv.restrict_class_id =".$classId." AND met.type = 'Commercial Restriction'
		ORDER BY met.id ASC;";
		$resultEType = mysqli_query($conn, $sqlEType);
		// output data of each row
		while($rowEType = mysqli_fetch_assoc($resultEType)) {
		?>
			<div class="item form-group col-md-6 commercialRestriction" style="display:none;">
				<div class="col-md-12 surcharges">
					<label class="col-md-6"><input type="checkbox" 
					<?php 
						if($rowEType['engine_type_restricted']=='yes'){
							echo "checked";
						} 
					?>
					 name="eType[<?php echo $rowEType['id'];?>]" onclick="fnEnableinputBox(event)"> 
					 <?php echo $rowEType['name'];?></label>
					<div class="col-md-6">
					<input  name="eTypeSurCharge[<?php echo $rowEType['id'];?>]" class="form-control col-md-7 col-xs-12" placeholder="Surcharges in $" type="text" value="<?php echo $rowEType['surcharge'] ;?>"
					<?php 
						if($rowEType['engine_type_restricted']=='yes'){
							echo "disabled";
						} 
					?>>
					</div>
				</div>
			</div>
		<?php } ?>
		
		<div class="clearfix"></div>
		<div class="item form-group">
			<label class="col-md-12" onclick="toggleEngine('engineTypeRestriction')" style="cursor:pointer;">Engine Type Restriction</label>
		</div>
		<?php 
		$sqlEType = "SELECT met.id, 
		met.name, 
		retmfv.engine_type_restricted,
		retmfv.engine_type_id,
		retmfv.surcharge 
		FROM master_engine_type as met LEFT JOIN 
		restrict_engine_type_mapping_for_vendor as retmfv
		ON met.id = retmfv.engine_type_id 
		WHERE retmfv.restrict_class_id =".$classId." AND met.type = 'Engine Type Restriction'
		ORDER BY met.id ASC;";
		$resultEType = mysqli_query($conn, $sqlEType);
		// output data of each row
		while($rowEType = mysqli_fetch_assoc($resultEType)) {
		?>
			<div class="item form-group col-md-6 engineTypeRestriction" style="display:none;">
				<div class="col-md-12 surcharges">
					<label class="col-md-6"><input type="checkbox" 
					<?php 
						if($rowEType['engine_type_restricted']=='yes'){
							echo "checked";
						} 
					?>
					 name="eType[<?php echo $rowEType['id'];?>]" onclick="fnEnableinputBox(event)"> 
					 <?php echo $rowEType['name'];?></label>
					<div class="col-md-6">
					<input  name="eTypeSurCharge[<?php echo $rowEType['id'];?>]" class="form-control col-md-7 col-xs-12" placeholder="Surcharges in $" type="text" value="<?php echo $rowEType['surcharge'] ;?>"
					<?php 
						if($rowEType['engine_type_restricted']=='yes'){
							echo "disabled";
						} 
					?>>
					</div>
				</div>
			</div>
		<?php } ?>
		
		<div class="clearfix"></div>
		<div class="item form-group">
			<label class="col-md-12" onclick="toggleEngine('driveTrein')" style="cursor:pointer;">DriveTrain</label>
		</div>
		<?php 
		$sqlEType = "SELECT met.id, 
		met.name, 
		retmfv.engine_type_restricted,
		retmfv.engine_type_id,
		retmfv.surcharge 
		FROM master_engine_type as met LEFT JOIN 
		restrict_engine_type_mapping_for_vendor as retmfv
		ON met.id = retmfv.engine_type_id 
		WHERE retmfv.restrict_class_id =".$classId." AND met.type = 'Drive Trein'
		ORDER BY met.id ASC;";
		$resultEType = mysqli_query($conn, $sqlEType);
		// output data of each row
		while($rowEType = mysqli_fetch_assoc($resultEType)) {
		?>
			<div class="item form-group col-md-6 driveTrein" style="display:none;">
				<div class="col-md-12 surcharges">
					<label class="col-md-6"><input type="checkbox" 
					<?php 
						if($rowEType['engine_type_restricted']=='yes'){
							echo "checked";
						} 
					?>
					 name="eType[<?php echo $rowEType['id'];?>]" onclick="fnEnableinputBox(event)"> 
					 <?php echo $rowEType['name'];?></label>
					<div class="col-md-6">
					<input  name="eTypeSurCharge[<?php echo $rowEType['id'];?>]" class="form-control col-md-7 col-xs-12" placeholder="Surcharges in $" type="text" value="<?php echo $rowEType['surcharge'] ;?>"
					<?php 
						if($rowEType['engine_type_restricted']=='yes'){
							echo "disabled";
						} 
					?>>
					</div>
				</div>
			</div>
		<?php } ?>
		
		<div class="clearfix"></div>
		<div class="item form-group">
			<label class="col-md-12" onclick="toggleEngine('packagesRestriction')" style="cursor:pointer;">Millage & Packages Restriction</label>
		</div>
		<?php 
		$sqlEType = "SELECT met.id, 
		met.name, 
		retmfv.engine_type_restricted,
		retmfv.engine_type_id,
		retmfv.surcharge 
		FROM master_engine_type as met LEFT JOIN 
		restrict_engine_type_mapping_for_vendor as retmfv
		ON met.id = retmfv.engine_type_id 
		WHERE retmfv.restrict_class_id =".$classId." AND met.type = 'Millage & Packages Restriction'
		ORDER BY met.id ASC;";
		$resultEType = mysqli_query($conn, $sqlEType);
		// output data of each row
		while($rowEType = mysqli_fetch_assoc($resultEType)) {
		?>
			<div class="item form-group col-md-6 packagesRestriction" style="display:none;">
				<div class="col-md-12 surcharges">
					<label class="col-md-6"><input type="checkbox" 
					<?php 
						if($rowEType['engine_type_restricted']=='yes'){
							echo "checked";
						} 
					?>
					 name="eType[<?php echo $rowEType['id'];?>]" onclick="fnEnableinputBox(event)"> 
					 <?php echo $rowEType['name'];?></label>
					<div class="col-md-6">
					<input  name="eTypeSurCharge[<?php echo $rowEType['id'];?>]" class="form-control col-md-7 col-xs-12" placeholder="Surcharges in $" type="text" value="<?php echo $rowEType['surcharge'] ;?>"
					<?php 
						if($rowEType['engine_type_restricted']=='yes'){
							echo "disabled";
						} 
					?>>
					</div>
				</div>
			</div>
		<?php } ?>
		
		<div class="clearfix"></div>
		<div class="item form-group">
			<label class="col-md-12" onclick="toggleEngine('additional')" style="cursor:pointer;">Additional Surcharge</label>
		</div>
		<?php 
		$sqlEType = "SELECT met.id, 
		met.name, 
		retmfv.engine_type_restricted,
		retmfv.engine_type_id,
		retmfv.surcharge 
		FROM master_engine_type as met LEFT JOIN 
		restrict_engine_type_mapping_for_vendor as retmfv
		ON met.id = retmfv.engine_type_id 
		WHERE retmfv.restrict_class_id =".$classId." AND met.type = 'additional'
		ORDER BY met.id ASC;";
		$resultEType = mysqli_query($conn, $sqlEType);
		// output data of each row
		while($rowEType = mysqli_fetch_assoc($resultEType)) {
		?>
			<div class="item form-group col-md-6 additional" style="display:none;">
				<div class="col-md-12 surcharges">
					<label class="col-md-6"><input type="checkbox" 
					<?php 
						if($rowEType['engine_type_restricted']=='yes'){
							echo "checked";
						} 
					?>
					 name="eType[<?php echo $rowEType['id'];?>]" onclick="fnEnableinputBox(event)"> 
					 <?php echo $rowEType['name'];?></label>
					<div class="col-md-6">
					<input  name="eTypeSurCharge[<?php echo $rowEType['id'];?>]" class="form-control col-md-7 col-xs-12" placeholder="Surcharges in $" type="text" value="<?php echo $rowEType['surcharge'] ;?>"
					<?php 
						if($rowEType['engine_type_restricted']=='yes'){
							echo "disabled";
						} 
					?>>
					</div>
				</div>
			</div>
		<?php } ?>