<?php
include_once('elements/db_connection.php');
if(isset($_REQUEST['state']) && !empty($_REQUEST['state']) && isset($_REQUEST['vendor_id'])) {
?>
<div class="checkbox">
  <label><input onclick="select_all_dealer()" class="selected_dealer_all" type="checkbox">Select All</label>
</div>
<?php
	$states = $_REQUEST['state'];
	//print_r($states);
	$vendor_id = $_REQUEST['vendor_id'];
	$arSelectedDealer = array();
	$sql = "SELECT * FROM vendor_state_mapping WHERE vendor_id=$vendor_id AND is_featured = 'yes'";
	$result = mysqli_query($conn, $sql);
	while($row = mysqli_fetch_assoc($result)) {
		if($row['dealer_id'] != null)
		$arSelectedDealer[] = $row['dealer_id'];
	}
	foreach($states as $state) {
		$sqlAS = "SELECT users.id, user_business_details.businessName, user_business_details.state FROM users LEFT JOIN user_business_details ON users.id = user_business_details.userId WHERE users.user_role_id=3 AND user_business_details.state = '$state' AND users.isDeleted = 0 ORDER BY users.id DESC";
		$resultAS = mysqli_query($conn, $sqlAS);
		while($rowAS = mysqli_fetch_assoc($resultAS)) {
			if(in_array($rowAS['id'], $arSelectedDealer)) {
		?>
			<div class="checkbox">
			  <label><input class="selected_dealer" type="checkbox" name="dealers[<?php echo $rowAS['state'];?>][]" value="<?php echo $rowAS['id'];?>" checked><?php echo $rowAS['businessName'] . ' (' . $rowAS['state'] . ')';?></label>
			</div>
<?php } else { ?>
			<div class="checkbox">
			  <label><input class="selected_dealer" type="checkbox" name="dealers[<?php echo $rowAS['state'];?>][]" value="<?php echo $rowAS['id'];?>"><?php echo $rowAS['businessName'] . ' (' . $rowAS['state'] . ')';?></label>
			</div>	
<?php } } } } ?>