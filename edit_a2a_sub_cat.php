<!-- Modal content -->
<div class="modal-content">
<?php
include_once('elements/db_connection.php');
$id = $_REQUEST['id'];
$sql = "SELECT * FROM appletoapplevalue WHERE id = $id";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
?>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="update_a2a_sub_cat.php" method="post">
				
				<div class="item form-group">
					<label class="col-md-2">Category Name</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
						<select name="appleToAppleType" class="form-control">
						<?php 
                            $sqlCat = "SELECT * FROM appletoappletype WHERE is_deleted='0'";
                            $resultCat = mysqli_query($conn, $sqlCat);
                            $slNo = 1;
                            if (mysqli_num_rows($resultCat) > 0) {
                                // output data of each row
                                while($rowCat = mysqli_fetch_assoc($resultCat)) {
                            ?>
							<option value="<?php echo $rowCat['id'];?>" <?php if($rowCat['id'] == $row['appleToAppleType']) { echo 'selected'; }?>><?php echo $rowCat['name'];?></option>
						<?php } }?>
						</select>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<div class="item form-group">
					<label class="col-md-2">Sub Category Name</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input type="text" name="name" class="form-control" value="<?php echo $row['name'];?>" placeholder="Sub Category name">
					</div>
				</div> 
				<div class="clearfix"></div>
				
				<div class="form-group">
				  <div class="col-md-6 col-md-offset-3">
					<a href="apple_to_apple_sub_category.php" class="btn btn-danger">Cancel</a>
					<button id="send" type="submit" class="btn btn-success">Save</button>
				  </div>
				</div>             				
            </form>
        </div>
    </div>
<?php } ?>   
</div>
