<?php
include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
?>
<style>
.class-data-body .form-group label:hover {
	color: #ec9221;
}
.loader-copy-class {
  border: 5px solid #f3f3f3;
  border-radius: 50%;
  border-top: 5px solid blue;
  border-right: 5px solid green;
  border-bottom: 5px solid red;
  width: 20px;
  height: 20px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
</style>
<!-- /top navigation -->
<!-- page content -->
<div class="right_col" role="main">
  <div class="x_panel">
      <div class="x_title">
        <h2>Create Class</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <form class="form-horizontal form-label-left" action="save_class.php" method="post">
			<div class="col-xs-8"></div>
			<div class="col-xs-4" style="color: #ec9221;">COPY DATA FROM OLD CLASS</div>
			<div class="col-md-6">
			
			  <div class="item form-group">
				<label class="col-md-4">Category <span class="required">*</span></label>
				<div class="col-md-8">
				  <select onchange="show_vendor_by_category(event)" class="form-control cat_id">
					  <option value="">PLEASE SELECT A CATEGORY</option>
					  <option value="VSC">VSC</option>
					  <option value="GAP">GAP</option>
					  <option value="ANCILLARY">ANCILLARY</option>
					  <option value="INSURANCE">INSURANCE</option>
				  </select>
				</div>
			  </div>
			  
			  <div class="item form-group">
				<label class="col-md-4">Choose Vendor <span class="required">*</span></label>
				<div class="col-md-8 vendor_list_filter">
					<select class="form-control" disabled>
						<option value="">PLEASE SELECT A VENDOR</option>
					</select>
				</div>
			  </div>
			  
			  <div class="item form-group">
				<label class="col-md-4" >Class Name <span class="required">*</span></label>
				<div class="col-md-8">
				  <input class="form-control col-md-7 col-xs-12" placeholder="Enter class name" name="name" required type="text">
				</div>
			  </div>
			  
			  <div class="item form-group">
				<label class="col-md-4">Vehicle Restriction Rule</label>
				<div class="col-md-8">
				  <input class="form-control" placeholder="Year range (eg. 2009, 2010-2017)" name="general_range" type="text">
				</div>
			  </div>
			  
			  <div class="item form-group">
				<label class="col-md-4" >Warranty Goes Back Up</label>
				<div class="col-md-8">
				  <input class="form-control col-md-7 col-xs-12" placeholder="Warrenty Goes Back Up" name="warrenty_goes_backup" type="text">
				</div>
			  </div>
			  
			</div>
			<div class="col-md-1">
			</div>
			<div class="col-md-5" style="height:210px; border: 1px solid #cbcbcc; overflow-y: scroll; padding: 10px;">
				<?php
				$sql = "SELECT * FROM restrict_class WHERE isDeleted = 'no'";
				$result = mysqli_query($conn, $sql);
				// output data of each row
				while($row = mysqli_fetch_assoc($result)) {
				?>
					<div class="col-md-12">
					<div class="loader-copy-class loader-copy-class-<?php echo $row['id'];?>" style="display:none;"></div>
						<?php echo $row['name'];?> <a href="javascript:void(0)" class="btn btn-primary" style="float:right;" onclick="copy_class(<?php echo $row['id'];?>)">Copy</a>
					</div>
			    <?php } ?>
			</div>
            <div class="clearfix"></div>
			<div class="ln_solid"></div>
			<div class="item form-group">
				<div class="col-md-3">
					<input type="text" class="form-control" placeholder="">
				</div>
				<div class="col-md-3">
					<a href="javascript:void(0)" class="btn btn-primary">Search</a>
				</div>
			</div>
			<div class="clearfix"></div>
            <div class="class-data-body">
				<div class="item form-group">
					<label class="col-md-12 tree_model_other" style="cursor: pointer;">
						Restriction To Apply
					</label>
				</div>
				<div id="toggleId" style="display:none;">
					<span class="col-md-12">
						1. Checked means restrict for all year modal.<br />
						2. Checked and year range given means restrict for given year.<br />
					</span>
					<div class="clearfix"></div>
					<ul class="tree_new tree_model to_apply">
						
					</ul>
				</div>
				<div class="clearfix"></div>
				<div class="item form-group">
					<label class="col-md-12 tree_model_other_two" style="cursor: pointer;">
						Restricted By Default
					</label>
				</div>
				<div class="clearfix"></div>
				<ul class="tree_new tree_model to_default" id="toggleIdTwo" style="display:none;">
					
				</ul>
				<div class="clearfix"></div>
				<div class="item form-group">
					<label class="col-md-12" onclick="toggleEngine('commercialRestriction')" style="cursor:pointer;">Commercial Restriction</label>
				</div>
				<?php 
				$sqlEType = "SELECT * FROM master_engine_type WHERE type='Commercial Restriction'";
				$resultEType = mysqli_query($conn, $sqlEType);
				// output data of each row
				while($rowEType = mysqli_fetch_assoc($resultEType)) {
				?>
				<div class="item form-group col-md-6 commercialRestriction" style="display:none;">
					<div class="col-md-12 surcharges">
						<label class="col-md-6"><input type="checkbox" name="eType[<?php echo $rowEType['id'];?>]" onclick="fnEnableinputBox(event)"> <?php echo $rowEType['name'];?></label>
						<div class="col-md-6">
							<input  name="eTypeSurCharge[<?php echo $rowEType['id'];?>]" class="form-control col-md-7 col-xs-12" placeholder="Surcharges in $" type="text">
						</div>
					</div>
				</div>
			<?php
			}
			?>
				<div class="clearfix"></div>
				<div class="item form-group">
					<label class="col-md-12" onclick="toggleEngine('engineTypeRestriction')" style="cursor:pointer;">Engine Type Restriction</label>
				</div>
				  <?php 
				  $sqlEType = "SELECT * FROM master_engine_type WHERE type='Engine Type Restriction'";
				  $resultEType = mysqli_query($conn, $sqlEType);
				  // output data of each row
				  while($rowEType = mysqli_fetch_assoc($resultEType)) {
				  ?>
				  <div class="item form-group col-md-6 engineTypeRestriction" style="display:none;">
					<div class="col-md-12 surcharges">
					  <label class="col-md-6"><input type="checkbox" name="eType[<?php echo $rowEType['id'];?>]" onclick="fnEnableinputBox(event)"> <?php echo $rowEType['name'];?></label>
					  <div class="col-md-6">
						<input  name="eTypeSurCharge[<?php echo $rowEType['id'];?>]" class="form-control col-md-7 col-xs-12" placeholder="Surcharges in $" type="text">
					  </div>
					</div>
				  </div>
				  <?php
				  }
				  ?>
				  <div class="clearfix"></div>
				  <div class="item form-group">
					<label class="col-md-12" onclick="toggleEngine('driveTrein')" style="cursor:pointer;">DriveTrain</label>
				  </div>
          <?php 
          $sqlEType = "SELECT * FROM master_engine_type WHERE type='Drive Trein'";
          $resultEType = mysqli_query($conn, $sqlEType);
          // output data of each row
          while($rowEType = mysqli_fetch_assoc($resultEType)) {
          ?>
          <div class="item form-group col-md-6 driveTrein" style="display:none;">
            <div class="col-md-12 surcharges">
              <label class="col-md-6"><input type="checkbox" name="eType[<?php echo $rowEType['id'];?>]" onclick="fnEnableinputBox(event)"> <?php echo $rowEType['name'];?></label>
              <div class="col-md-6">
                <input  name="eTypeSurCharge[<?php echo $rowEType['id'];?>]" class="form-control col-md-7 col-xs-12" placeholder="Surcharges in $" type="text">
              </div>
            </div>
          </div>
          <?php
          }
          ?>
		  <div class="clearfix"></div>
          <div class="item form-group">
            <label class="col-md-12" onclick="toggleEngine('packagesRestriction')" style="cursor:pointer;">Millage & Packages Restriction</label>
          </div>
          <?php 
          $sqlEType = "SELECT * FROM master_engine_type WHERE type='Millage & Packages Restriction'";
          $resultEType = mysqli_query($conn, $sqlEType);
          // output data of each row
          while($rowEType = mysqli_fetch_assoc($resultEType)) {
          ?>
          <div class="item form-group col-md-6 packagesRestriction" style="display:none;">
            <div class="col-md-12 surcharges">
              <label class="col-md-6"><input type="checkbox" name="eType[<?php echo $rowEType['id'];?>]" onclick="fnEnableinputBox(event)"> <?php echo $rowEType['name'];?></label>
              <div class="col-md-6">
                <input  name="eTypeSurCharge[<?php echo $rowEType['id'];?>]" class="form-control col-md-7 col-xs-12" placeholder="Surcharges in $" type="text">
              </div>
            </div>
          </div>
          <?php
          }
          ?>
          <div class="clearfix"></div>
          <div class="item form-group">
            <label class="col-md-12" onclick="toggleEngine('additional')" style="cursor:pointer;">Additional Surcharge </label>
          </div>
          <?php 
          $sqlEType = "SELECT * FROM master_engine_type WHERE type='additional'";
          $resultEType = mysqli_query($conn, $sqlEType);
          // output data of each row
          while($rowEType = mysqli_fetch_assoc($resultEType)) {
          ?>
          <div class="item form-group col-md-6 additional" style="display:none;">
            <div class="col-md-12 surcharges">
              <label class="col-md-6"><input type="checkbox" name="eType[<?php echo $rowEType['id'];?>]" onclick="fnEnableinputBox(event)"> <?php echo $rowEType['name'];?></label>
              <div class="col-md-6">
                <input  name="eTypeSurCharge[<?php echo $rowEType['id'];?>]" class="form-control col-md-7 col-xs-12" placeholder="Surcharges in $" type="text">
              </div>
            </div>
          </div>
          <?php
          }
          ?>
          </div>
		  
          <div class=" form-group">
            <div class="col-md-6 col-md-offset-3">
              <a class="btn btn-danger" href="restrict_class_list.php">Cancel</a>
              <button id="send" type="submit" class="btn btn-success">Save</button>
            </div>
          </div>
          <br />
        </form>
      </div>
    </div>
</div>

<!-- /page content -->
<!-- footer content -->
<?php 
include_once('elements/footer.php');
?>
<!-- /footer content -->
<script>
$(document).ready(function(){
	$.post('ajax_add_restrict_class.php',{type:'to_apply'},function(m){
		$('.to_apply').html(m);
	});
	$.post('ajax_add_restrict_class.php',{type:'to_default'},function(m){
		$('.to_default').html(m);
	});
});
$(document).on('click', '.tree_model_other', function(e) {
	$('#toggleId').toggle();
});

$(document).on('click', '.tree_model_other_two', function(e) {
	$('#toggleIdTwo').toggle();
});
function toggleEngine(name) {
	$('.' + name).toggle();
};
function copy_class(class_id) {
	$('.loader-copy-class-' + class_id).show();
	$.ajax({
		method: "POST",
		url: "class_copy_data.php",
		data: { class_id : class_id }
	  }).done(function(data) {
		//console.log(data);
		$('.class-data-body').html(data);
		$('.loader-copy-class-' + class_id).hide();
	  });
};

$(document).on('click', '.tree_model .model_label', function(e) {
  var model = $(this).closest('.model').first();
  $(model).children('ul').fadeToggle();
  e.stopPropagation();
});
$(document).on('click', '.tree_make .make_label', function(e) {
  var make = $(this).closest('.make_li').first();
  $(make).children('ul').fadeToggle();
  e.stopPropagation();
});
$(document).on('change', '.tree_model .model_checkbox', function(e) {
  var make = $(this).closest('.model').first();
  $(make).find("input[type='checkbox']").prop('checked', this.checked);
  e.stopPropagation();
});
$(document).on('change', '.tree_make .make_checkbox', function(e) {
  var make = $(this).closest('.make_li').first();
  $(make).find("input[type='checkbox']").prop('checked', this.checked);
  e.stopPropagation();
});
$(document).on('click', 'a', function(e) {
  switch ($(this).text()) {
    case 'Collapse All':
      $('.tree_new ul').fadeOut();
      break;
    case 'Expand All':
      $('.tree_new ul').fadeIn();
      break;
    case 'Checked All':
      $(".tree_new input[type='checkbox']").prop('checked', true);
      break;
    case 'Unchek All':
      $(".tree_new input[type='checkbox']").prop('checked', false);
      break;
    default:
  }
});
function fnEnableinputBox(e) {
  var val = $(e.target).prop( "checked" );
  //console.log(val);
  var newRow = $(e.target).parents('.surcharges');
  $(newRow).find('input').each(function(i) {
    if(i == 1) {
      if(val) {
        $(this).prop("disabled", true);
      } else {
        $(this).prop("disabled", false);
      }
    }
  });
}
function show_vendor_by_category(e) {
  var cat = $(e.target).val();
  $.ajax({
    method: "POST",
    url: "vendor_list_filter.php",
    data: { cat : cat }
  }).done(function(data) {
    //console.log(data);
    $('.vendor_list_filter').html(data);
  });
}
function topFunction() {
  $('html, body').animate({scrollTop:0}, 'slow');
}
function bottomFunction() {
  $('html, body').animate({scrollTop:$(document).height()}, 'slow');
}
function search(e) {
  var name = $(e.target).val();
  //console.log(name);
  var pattern = name.toLowerCase();
  var targetId = "";
  var divs = document.getElementsByClassName("forSearch");
  //console.log(divs);
  for (var i = 0; i < divs.length; i++) {
      var para = divs[i].getElementsByTagName("label");
      var index = para[0].innerText.toLowerCase().indexOf(pattern);
      if (index != -1) {
        targetId = divs[i].id;
        //console.log(divs[i].id);
        //console.log(index);
        //console.log(targetId);
        document.getElementById(targetId).scrollIntoView();
        $('#' + targetId).css('background', '#c6f7c6');
        setTimeout(function(){$('#' + targetId).css('background', 'none'); }, 3000);
        break;
      }
  }  
}
</script>        