<?php
include_once('elements/db_connection.php');
//echo '<pre>';
//print_r($_REQUEST);
//exit;
$vendor_id = $_REQUEST['vendor_id'];
$plan_id = $_REQUEST['plan_id'];
$sql = "INSERT INTO appletoappledata (vendor_id, plan_id) VALUES ('$vendor_id', '$plan_id')";
$result = mysqli_query($conn, $sql);
$appletoappledata_id = mysqli_insert_id($conn);
if(isset($_REQUEST['A2AType'])) {
    $A2AType = $_REQUEST['A2AType'];
    foreach($A2AType as $mk) {
        $a2a_type_id = $mk;
        $sql = "INSERT INTO a2a_type_data (appletoappledata_id, a2a_type_id) VALUES ($appletoappledata_id, $a2a_type_id)";
        mysqli_query($conn, $sql);
		if(isset($_REQUEST['A2ATypeValue'][$a2a_type_id])) {
			$A2ATypeValue = $_REQUEST['A2ATypeValue'][$a2a_type_id];
			foreach($A2ATypeValue as $ml) {
				$a2a_value_id = $ml;
				$value_comment = $_REQUEST['A2ATypeComment'][$a2a_value_id];
				$sql = "INSERT INTO a2a_value_data (appletoappledata_id, a2a_value_id, value_comment) VALUES ($appletoappledata_id, $a2a_value_id, '$value_comment')";
				mysqli_query($conn, $sql);
			}
		}
    }
}
$labourReady = null;
$labourReadyCharge = null;
if(isset($_REQUEST['labourReady'])) {
    $labourReady = $_REQUEST['labourReady'];
    if($labourReady == 'limit') {
        $labourReadyCharge = $_REQUEST['labourReadyCharge'];
    }
}
$rental = 'no';
$rentalCharge = null;
if(isset($_REQUEST['rental'])) {
    $rental = 'yes';
    $rentalCharge = $_REQUEST['rentalCharge'];
}
$roadSideAssitance = 'no';
$roadSideAssitanceCharge = null;
if(isset($_REQUEST['roadSideAssitance'])) {
    $roadSideAssitance = 'yes';
    $roadSideAssitanceCharge = $_REQUEST['roadSideAssitanceCharge'];
}
$toing = 'no';
$toingCharge = null;
if(isset($_REQUEST['toing'])) {
    $toing = 'yes';
    $toingCharge = $_REQUEST['toingCharge'];
}
$wheels = null;
$wheelsCharge = null;
if(isset($_REQUEST['wheels'])) {
    $wheels = $_REQUEST['wheels'];
    if($wheels == 'limit') {
        $wheelsCharge = $_REQUEST['wheelsCharge'];
    }
}
$tiers = null;
$tiersCharge = null;
if(isset($_REQUEST['tiers'])) {
    $tiers = $_REQUEST['tiers'];
    if($tiers == 'limit') {
        $tiersCharge = $_REQUEST['tiersCharge'];
    }
}
$roadHazard = null;
$roadHazardCharge = null;
if(isset($_REQUEST['roadHazard'])) {
    $roadHazard = $_REQUEST['roadHazard'];
    if($roadHazard == 'limit') {
        $roadHazardCharge = $_REQUEST['roadHazardCharge'];
    }
}
$cosmeticWheelRepair = null;
$cosmeticWheelRepairCharge = null;
if(isset($_REQUEST['cosmeticWheelRepair'])) {
    $cosmeticWheelRepair = $_REQUEST['cosmeticWheelRepair'];
    if($cosmeticWheelRepair == 'limit') {
        $cosmeticWheelRepairCharge = $_REQUEST['cosmeticWheelRepairCharge'];
    }
}
$curbImpactRepair = null;
$curbImpactRepairCharge = null;
if(isset($_REQUEST['curbImpactRepair'])) {
    $curbImpactRepair = $_REQUEST['curbImpactRepair'];
    if($curbImpactRepair == 'limit') {
        $curbImpactRepairCharge = $_REQUEST['curbImpactRepairCharge'];
    }
}
$paintlessDentRepair = null;
$paintlessDentRepairCharge = null;
if(isset($_REQUEST['paintlessDentRepair'])) {
    $paintlessDentRepair = $_REQUEST['paintlessDentRepair'];
    if($paintlessDentRepair == 'limit') {
        $paintlessDentRepairCharge = $_REQUEST['paintlessDentRepairCharge'];
    }
}
$tierWheel = 'no';
$tierWheelCharge = null;
if(isset($_REQUEST['tierWheel'])) {
    $tierWheel = 'yes';
    $tierWheelCharge = $_REQUEST['tierWheelCharge'];
}
$windshildChip = 'no';
$windshildChipCharge = null;
if(isset($_REQUEST['windshildChip'])) {
    $windshildChip = 'yes';
    $windshildChipCharge = $_REQUEST['windshildChipCharge'];
}
$windshildProtection = 'no';
$windshildProtectionCharge = null;
if(isset($_REQUEST['windshildProtection'])) {
    $windshildProtection = 'yes';
    $windshildProtectionCharge = $_REQUEST['windshildProtectionCharge'];
}
$keyReplacement = 'no';
$keyReplacementCharge = null;
if(isset($_REQUEST['keyReplacement'])) {
    $keyReplacement = 'yes';
    $keyReplacementCharge = $_REQUEST['keyReplacementCharge'];
}
$leaseEndProtection = 'no';
$leaseEndProtectionCharge = null;
if(isset($_REQUEST['leaseEndProtection'])) {
    $leaseEndProtection = 'yes';
    $leaseEndProtectionCharge = $_REQUEST['leaseEndProtectionCharge'];
}
$otherHighlight = 'no';
$otherHighlightCharge = null;
if(isset($_REQUEST['otherHighlight'])) {
    $otherHighlight = 'yes';
    $otherHighlightCharge = $_REQUEST['otherHighlightCharge'];
}
$storeType = $_REQUEST['storeType'];
$effectiveDay = $_REQUEST['effectiveDay'];
$sql = "INSERT INTO highlight_coverage (appletoappledata_id, labourReady, labourReadyCharge, rental, rentalCharge, roadSideAssitance, roadSideAssitanceCharge, toing, toingCharge, otherHighlight, otherHighlightCharge, wheels, wheelsCharge, tiers, tiersCharge, roadHazard, roadHazardCharge, cosmeticWheelRepair, cosmeticWheelRepairCharge, curbImpactRepair, curbImpactRepairCharge, paintlessDentRepair, paintlessDentRepairCharge, tierWheel, tierWheelCharge, windshildChip, windshildChipCharge, windshildProtection, windshildProtectionCharge, keyReplacement, keyReplacementCharge, leaseEndProtection, leaseEndProtectionCharge, storeType, effectiveDay) VALUES ($appletoappledata_id, '$labourReady', '$labourReadyCharge', '$rental', '$rentalCharge', '$roadSideAssitance', '$roadSideAssitanceCharge', '$toing', '$toingCharge', '$otherHighlight', '$otherHighlightCharge', '$wheels', '$wheelsCharge', '$tiers', '$tiersCharge', '$roadHazard', '$roadHazardCharge', '$cosmeticWheelRepair', '$cosmeticWheelRepairCharge', '$curbImpactRepair', '$curbImpactRepairCharge', '$paintlessDentRepair', '$paintlessDentRepairCharge', '$tierWheel', '$tierWheelCharge', '$windshildChip', '$windshildChipCharge', '$windshildProtection', '$windshildProtectionCharge', '$keyReplacement', '$keyReplacementCharge', '$leaseEndProtection', '$leaseEndProtectionCharge', '$storeType', '$effectiveDay')";
mysqli_query($conn, $sql);
header('Location: add_apple_to_apple_list.php');
?>