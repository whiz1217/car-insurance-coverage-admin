<?php 
include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_title">
                <h2>Add Dealer Info</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <form class="form-horizontal form-label-left save_dealer" action="save_dealer.php" method="post" enctype="multipart/form-data">
					            <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-6">Dealer Type <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <select name="dealerType" class="form-control" required>
                            <option value="">Choose Type</option>
                            <option value="newVehicalsAndUsed">New Vehicals & Used</option>
                            <option value="usedOnly">Used Only</option>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-6">Business Name <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input class="form-control col-md-7 col-xs-12" name="businessName" placeholder="" required type="text">
                        </div>
                      </div>
					            <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Address <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="email" name="email" style="text-transform: none;" required class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 1 <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="streetAddres1" required class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 2</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="streetAddres2" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Zip/Postal Code <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="zipCode" onkeyup="get_city_business(event)" required class="zip_code form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12 city_vendor_add">
                          <input type="text" name="businessCity" placeholder="City" required class="city form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group col-md-6">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">State/Province <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12 state_vendor_add">
                          <input type="text" name="businessState" placeholder="State" required class="state form-control col-md-7 col-xs-12"> 
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="telephone" data-format="ddd-ddd-dddd" required class="form-control col-md-7 col-xs-12 bfh-phone">
                        </div>
                      </div>   

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="fax" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="password" name="password" placeholder="" class="form-control col-md-7 col-xs-12" required>
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">EIN/Tax ID</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="einTaxId" placeholder="" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" data-max-size="2048">Upload</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="file" name="businessDocFiles" class="form-control col-md-7 col-xs-12">
                        <!-- Max upload size 10 MB -->
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <label class="col-md-12">Personal details</label>
                      <div class="ln_solid"></div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="name" required class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Id <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="email" name="personalEmail" style="text-transform: none;" required class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 1 <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="personalStreetAddres1" required class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 2</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="personalStreetAddres2" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Zip/Postal Code <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="personalZipCode" required class="form-control col-md-7 col-xs-12" onkeyup="get_city_personal(event)">
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="personalCity" placeholder="City" required class="form-control col-md-7 col-xs-12 city_personal">
                        </div>
                      </div>
                      <div class="form-group col-md-6">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">State/Province <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="personalState" placeholder="State" required class="form-control col-md-7 col-xs-12 state_personal"> 
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="personalPhoneNo" data-format="ddd-ddd-dddd" required class="form-control col-md-7 col-xs-12 bfh-phone">
                        </div>
                      </div>
                    
                      <div class="clearfix"></div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <a class="btn btn-danger" href="vendor_list.php">Cancel</a>
                          <button id="send" type="submit" class="btn btn-success">Save</button>
                        </div>
                      </div>
                    </form>
              </div>
            </div>
        </div>

    
        <!-- /page content -->
        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
<script>
function get_city_business(e) {
	var zip = $(e.target).val();
	$.ajax({
		method: "POST",
		url: "get_city_state_by_zip.php",
		data: { zip: zip }
	}).done(function(data) {
		if(data != '') {
			var obj = JSON.parse(data);
			$('.city').val(obj.CITY);
			$('.state').val(obj.STATE);
		}
	});
}
function get_city_personal(e) {
	var zip = $(e.target).val();
	$.ajax({
		method: "POST",
		url: "get_city_state_by_zip.php",
		data: { zip: zip }
	}).done(function(data) {
		if(data != '') {
			var obj = JSON.parse(data);
			$('.city_personal').val(obj.CITY);
			$('.state_personal').val(obj.STATE);
		}
	});
}
	

function compIsType(t, s) { 
  for(z = 0; z < t.length; ++z) 
      if(t[z] == s)
        return true;

  return false;
}

function showIncentive(e) {
  var val = $(e.target).prop( "checked" );
  if(val) {
      $('.incentive').prop("disabled", false);
    } else {
      $('.incentive').prop("disabled", true);
    }
}

</script>