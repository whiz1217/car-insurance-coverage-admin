<?php 
        include_once('elements/db_connection.php');
        include_once('elements/left_menu.php');
        ?>
        <!-- top navigation -->
        <?php 
        include_once('elements/header.php');
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_title">
                <h2>Company Logo</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="class_list_table">
                <table id="restrict_class_list">
                    <thead>
                        <tr>
                            <th>Sl. No.</th>
                            <th>Logo</th>
                            <th>Date Modified</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT * FROM dealer_portal_settings";
                            $result = mysqli_query($conn, $sql);
                            $slNo = 1;
                            if (mysqli_num_rows($result) > 0) {
                                // output data of each row
                                while($row = mysqli_fetch_assoc($result)) {
                            ?>
                                <tr>
                                    <td><?php echo $slNo;?></td>
                                    <td><img src="user_documents/company_logo/<?php echo $row['logo'];?>" width="200"></td>
                                    <td><?php echo $row['logo_updated_date'];?></td>
                                    <td>
										<a class='btn btn-primary' onclick="edit_logo(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
                                    </td>
                                </tr>
                            <?php
                            $slNo++;
                                }
                            }
                        ?>
                    </tbody>
                    </table>
                </div>
                    
              </div>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content class-modal" >
                Loading...
            </div>

        </div>
        <!-- /page content -->
        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
<script>
var table;
$(document).ready(function(){
    table = $('#restrict_class_list').DataTable();
});  
function edit_logo(id) {
    $.ajax({
        method: "POST",
        url: "edit_logo.php",
        data: { id : id }
    }).done(function(data) {
        $('#myModal').html(data);
    });
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}
function close_popup() {
    $('#myModal').html('Loading...');
    var modal = document.getElementById('myModal');
    modal.style.display = "none";
}
</script>