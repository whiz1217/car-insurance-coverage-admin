<!-- Modal content -->
<div class="modal-content">
<?php
include_once('elements/db_connection.php');
$id = $_REQUEST['id'];
$sql = "SELECT * FROM dealer_portal_settings WHERE id = $id";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
?>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="update_logo.php" method="post" enctype="multipart/form-data">
				
				<div class="item form-group">
					<label class="col-md-2">Logo</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
						<input type="file" name="logo" class="form-control">
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="form-group">
				  <div class="col-md-6 col-md-offset-3">
					<a href="logo_list.php" class="btn btn-danger">Cancel</a>
					<button id="send" type="submit" class="btn btn-success">Save</button>
				  </div>
				</div>             				
            </form>
        </div>
    </div>
<?php } ?>   
</div>
