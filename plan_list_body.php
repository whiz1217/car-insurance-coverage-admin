                <table id="restrict_class_list">
                  <thead>
                      <tr>
                          <th>Sl. no.</th>
                          <th>Category</th>
                          <th>Vendor Name</th>
                          <th>Name</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                        include_once('elements/db_connection.php');
                        $cat = $_REQUEST['cat'];
                        if($cat == '') {
                          $sql = "SELECT id, name FROM plans WHERE isDeleted = 0 ORDER BY `id` DESC";
                        } else if(isset($_REQUEST['id']) && $_REQUEST['id'] != '') {
                          $user_id = $_REQUEST['id'];
                          $sql = "SELECT plans.id, plans.name 
                              FROM plans 
                              JOIN plan_terms ON plan_terms.plansId = plans.id 
                              JOIN user_detail ON user_detail.user_id = plan_terms.vendorId 
                              WHERE plans.isDeleted = 0 
                              AND user_detail.category='$cat' 
                              AND user_detail.user_id = '$user_id' 
                              GROUP BY `id` 
                              ORDER BY `id` DESC";
                        } else {
                          $sql = "SELECT plans.id, plans.name 
                              FROM plans 
                              JOIN plan_terms ON plan_terms.plansId = plans.id 
                              JOIN user_detail ON user_detail.user_id = plan_terms.vendorId 
                              WHERE plans.isDeleted = 0 AND user_detail.category='$cat'
                              GROUP BY `id`  
                              ORDER BY `id` DESC";
                        }
                        $result = mysqli_query($conn, $sql);
                        $slNo = 1;
                        if (mysqli_num_rows($result) > 0) {
                            // output data of each row
                            while($row = mysqli_fetch_assoc($result)) {
                              $planId = $row['id'];
                                $sqlTerm = "SELECT user_detail.company_name, user_detail.category FROM plan_terms LEFT JOIN user_detail ON user_detail.user_id = plan_terms.vendorId WHERE plan_terms.isDeleted = 0 AND plan_terms.plansId = $planId";
                                $resultTerm = mysqli_query($conn, $sqlTerm);
                                $slNoA = 1;
                                if (mysqli_num_rows($resultTerm) > 0) {
                                    // output data of each row
                                    while($rowTerm = mysqli_fetch_assoc($resultTerm)) {
                                        if ($slNoA== 1) { 
                                            $category = $rowTerm['category'];
                                            $vendorName = $rowTerm['company_name'];
                                        }
                                        $slNoA ++;
                                    }
                                }
                        ?>
                              <tr>
                                <td><?php echo $slNo;?></td>
                                <td><?php echo $category;?></td>
                                <td><?php echo $vendorName;?></td>
                                <td><?php echo $row['name'];?></td>
                                <td>
                                <a class='btn btn-danger' onclick="delete_plan(<?php echo $row['id'];?>)" style="cursor:pointer;">Delete</a>
                                <a class='btn btn-primary' onclick="edit_detail(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
                                <a class='btn btn-primary' onclick="show_detail(<?php echo $row['id'];?>)" style="cursor:pointer;">View</a>
                                </td>
                              </tr>
                        <?php
                              $slNo++;
                            }
                        } else {
                          $error_message = 'Wrong username or password';
                        }
                      ?>
                  </tbody>
                </table>