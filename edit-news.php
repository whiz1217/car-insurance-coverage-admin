<!-- Modal content -->
<div class="modal-content">
<?php
include_once('elements/db_connection.php');
$id = $_REQUEST['id'];
$sql = "SELECT * FROM news WHERE id = $id";
$result = mysqli_query($conn, $sql);
while($row = mysqli_fetch_assoc($result)) {
?>
    <div class="x_panel">
        <div class="x_content">
            <span class="close" onclick="close_popup()">&times;</span>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="update_news.php" method="post">
				
				<div class="item form-group">
					<label class="col-md-2">Promo Codes</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<input class="form-control" type="hidden" name="id" value="<?php echo $id;?>">
						<input type="text" name="promoCodes" class="form-control" value="<?php echo $row['promoCode'];?>" placeholder="promo code1, promo code 2">
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="item form-group">
					<label class="col-md-2">News</label>
					<div class="col-md-4 col-sm-4 col-xs-12">
					  <textarea type="text" name="news" class="form-control" placeholder=""><?php echo $row['news'];?></textarea>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="item form-group">
					<label class="col-md-2">Status</label>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<select class="form-control" name="status">
							<option value="Active" <?php if($row['status'] == 'Active'){ echo "selected";}?>>Active</option>
							<option value="Inactive"  <?php if($row['status'] == 'Inactive'){ echo "selected";}?>>Inactive</option>
						</select>
					</div>
				</div>
				<div class="form-group">
				  <div class="col-md-6 col-md-offset-3">
					<a href="news_list.php" class="btn btn-danger">Cancel</a>
					<button id="send" type="submit" class="btn btn-success">Save</button>
				  </div>
				</div>             				
            </form>
        </div>
    </div>
<?php } ?>   
</div>

<script>
function show_vendor_by_category(e) {
  var cat = $(e.target).val();
  $.ajax({
    method: "POST",
    url: "vendor_list_filter.php",
    data: { cat : cat }
  }).done(function(data) {
    //console.log(data);
    $('.vendor_list_filter').html(data);
  });
}
</script>
