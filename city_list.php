<?php
include_once('elements/db_connection.php');
$state_id = $_REQUEST['id'];
if($state_id != '') {
    ?>
    <select name="city_id" required class="form-control">
    <option value="">Please select city</option>
    <?php 
    $sql = "SELECT * FROM cities where `state_id` = $state_id";
    $result = mysqli_query($conn, $sql);
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
    ?>
        <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
    <?php       
        }
} else {
    ?>
    <select name="city_id" required class="form-control" disabled>
    <option value="">Please select city</option>
    </select>
    <?php
}
?>