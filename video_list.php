<?php 
include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_title">
                <h2>Video Category List</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="class_list_table">
                <table id="restrict_class_list">
                    <thead>
                        <tr>
                            <th>Sl. No.</th>
                            <th>Category Name</th>
                            <th>Sub Category Name</th>
                            <th>Video</th>
                            <th>Date Created</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT faq_video.id,faq_video.sub_category,faq_video.video,faq_video.created_at,master_video_category.name 
							FROM faq_video 
							LEFT JOIN master_video_category ON faq_video.video_category_id = master_video_category.id
							WHERE faq_video.is_deleted='0'";
                            $result = mysqli_query($conn, $sql);
                            $slNo = 1;
                            if (mysqli_num_rows($result) > 0) {
                                // output data of each row
                                while($row = mysqli_fetch_assoc($result)) {
                            ?>
                                <tr>
                                    <td><?php echo $slNo;?></td>
                                    <td><?php echo $row['name'];?></td>
                                    <td><?php echo $row['sub_category'];?></td>
                                    <td>
									<video width="200" height="200" controls>
										<source src="user_documents/video_faq/<?php echo $row['video'];?>" type="video/mp4">
										Your browser does not support the video tag.
									</video>
									</td>
                                    <td><?php echo $row['created_at'];?></td>
                                    <td>
										<a class='btn btn-primary' onclick="edit_video(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
										<a class='btn btn-danger' onclick="delete_video(<?php echo $row['id'];?>)" style="cursor:pointer;">Delete</a> 
                                    </td>
                                </tr>
                            <?php
                            $slNo++;
                                }
                            }
                        ?>
                    </tbody>
                    </table>
                </div>
                    
              </div>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content class-modal" >
                Loading...
            </div>

        </div>
        <!-- /page content -->
		
        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
<script>
var table;
$(document).ready(function(){
    table = $('#restrict_class_list').DataTable();
});  
function edit_video(id) {
    $.ajax({
        method: "POST",
        url: "edit_video.php",
        data: { id : id }
    }).done(function(data) {
        $('#myModal').html(data);
    });
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}
function delete_video(id) {
    $.ajax({
        method: "POST",
        url: "video-delete.php",
        data: { id : id }
    }).done(function(data) {
        $('#myModal').html(data);
    });
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}
function close_popup() {
    $('#myModal').html('Loading...');
    var modal = document.getElementById('myModal');
    modal.style.display = "none";
}
</script>