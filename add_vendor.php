<?php 
	include_once('elements/db_connection.php');
	include_once('elements/left_menu.php');
	include_once('elements/header.php');
?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_title">
                <h2>Add Vendor Info</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <form class="form-horizontal form-label-left save_vendor" action="vendor/save_vendor.php" method="post" enctype="multipart/form-data">
					            <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-6">Category</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select required name="category[]" class="vendor_category" multiple="multiple">
                            <option value="VSC">VSC</option>
                            <option value="GAP">GAP</option>
                            <option value="ANCILLARY">ANCILLARY</option>
                            <option value="INSURANCE">INSURANCE</option>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" name="company_name" placeholder="" type="text">
                        </div>
                      </div>

					            <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Website URL</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="website_url" placeholder="http://www.website.com" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

					            <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Address</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="email" style="text-transform: none;" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 1</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="street_address_1" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Street Address 2</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="street_address_2" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Zip/Postal Code</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="zip_code" onkeyup="get_city(event)" class="zip_code form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">City</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 city_vendor_add">
                          <input type="text" name="city" placeholder="City" class="city form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group col-md-6">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">State/Province</label>
                        <div class="col-md-6 col-sm-6 col-xs-12 state_vendor_add">
                          <input type="text" name="state" placeholder="State" class="state form-control col-md-7 col-xs-12"> 
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" data-format="ddd-ddd-dddd" name="telephone" class="bfh-phone form-control col-md-7 col-xs-12">
                        </div>
                      </div>   

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="fax_number" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                    
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" data-max-size="2048">Logo</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="file" name="logoDocument" class="form-control col-md-7 col-xs-12 vendorLogo" accept="image/*">
						<img id="showLogo" src="#" alt="Logo" style="display:none; width: 225px;"/>
                        <!-- Max upload size 10 MB -->
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" data-max-size="2048">Notes</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea name="adminNotes" class="form-control col-md-7 col-xs-12"></textarea>
                        <!-- Max upload size 10 MB -->
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <label class="col-md-12">Claim Info</label>
                      <div class="ln_solid"></div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="claim_name" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Id</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="claim_email" style="text-transform: none;" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="claim_telephone" data-format="ddd-ddd-dddd" class="form-control col-md-7 col-xs-12 bfh-phone">
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="claim_fax" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      <label class="col-md-12">Cancellation Info</label>
                      <div class="ln_solid"></div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="cancellation_name" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Email Id</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="cancellation_email" style="text-transform: none;" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="cancellation_telephone" data-format="ddd-ddd-dddd" class="form-control col-md-7 col-xs-12 bfh-phone">
                        </div>
                      </div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Fax</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="cancellation_fax_number" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="clearfix"></div>
                      <label class="col-md-12"></label>
                      <div class="ln_solid"></div>
                      <div class="item form-group col-md-6">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Intro</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea name="intro" class="form-control col-md-7 col-xs-12" placeholder="Intro"></textarea>
                        </div>
                      </div>
                      <div class="checkbox col-md-12" style="margin-bottom:20px">
                        <label class="checkbox-inline"><input type="checkbox" name="pushToNews">Push intro to news</label>
                      </div>
                      <div class="clearfix"></div>
                        <label class="col-md-12">Master Agreement</label>
                        <div class="ln_solid"></div>
                        <div class="item form-group col-md-6">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12">Upload Master Agreement</label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="file" id="masterAgrementDocument" name="masterAgrementDocument[]" class="form-control col-md-7 col-xs-12" multiple>
							<a class="form-control btn btn-primary" type="button" value="Preview" onclick="PreviewImage('masterAgrementDocument');">Preview</a>
                            <!-- Max upload size 10 MB -->
                          </div>
                      </div>

                      
                      <div class="clearfix"></div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <a class="btn btn-danger" href="restrict_class_list.php">Cancel</a>
                          <button id="send" type="submit" class="btn btn-success">Save</button>
                        </div>
                      </div>
                    </form>
              </div>
            </div>
        </div>
		<!-- The Modal -->
        <div id="myModal" class="modal">
            <div class="modal-content">
			<span class="close" onclick="close_popup()">×</span>
                <div style="clear:both">
				   <iframe id="viewer" frameborder="0" scrolling="no" style="width: 100%; height:1000px;"></iframe>
				</div>
            </div>
        </div>

    
        <!-- /page content -->
        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
        <script>
		function PreviewImage(prId) {
			var modal = document.getElementById('myModal');
			modal.style.display = "block";
			pdffile=document.getElementById(prId).files[0];
			pdffile_url=URL.createObjectURL(pdffile);
			$('#viewer').attr('src',pdffile_url);
		}
		function close_popup() {
			var modal = document.getElementById('myModal');
			modal.style.display = "none";
		}
		function readURL(input) {

		  if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function(e) {
			  $('#showLogo').attr('src', e.target.result);
			  $('#showLogo').show();
			}

			reader.readAsDataURL(input.files[0]);
		  }
		}

		$(".vendorLogo").change(function() {
		  readURL(this);
		});
		
        function get_city(e) {
			var zip = $(e.target).val();
			$.ajax({
				method: "POST",
				url: "get_city_state_by_zip.php",
				data: { zip: zip }
			}).done(function(data) {
				if(data != '') {
					var obj = JSON.parse(data);
					$('.city').val(obj.CITY);
					$('.state').val(obj.STATE);
				}
			});
        }
       
        function compIsType(t, s) { 
          for(z = 0; z < t.length; ++z) 
              if(t[z] == s)
                return true;

          return false;
        }
		$('.vendor_category').multipleSelect({
			width: '100%',
		});
        </script>