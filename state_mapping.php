<?php 
include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_content">
              <form class="form-horizontal form-label-left" action="save_state_mapping.php" method="post">
					<div class="item form-group">
						<label class="col-md-2">Category <span class="required">*</span></label>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<select onchange="show_vendor_by_category(event)" class="form-control cat_id" required>
									<option value="">PLEASE SELECT A CATEGORY</option>
									<option value="VSC">VSC</option>
									<option value="GAP">GAP</option>
									<option value="ANCILLARY">ANCILLARY</option>
									<option value="INSURANCE">INSURANCE</option>
								</select>
						</div>
					</div>

					<div class="item form-group">
						<label class="col-md-2">Choose vendor <span class="required">*</span></label>
						<div class="col-md-4 col-sm-6 col-xs-12 vendor_list_filter">
							<select class="form-control vendor_id" disabled required>
								<option value="">PLEASE SELECT A VENDOR</option>
							</select>
						</div>
					</div>

					<div class="item form-group">
						<label class="col-md-2">Choose state</label>
						<div class="col-md-4 col-sm-6 col-xs-12 state_select_div">
						  <?php 
							//$sql = "SELECT user_business_details.state FROM user_business_details LEFT JOIN users ON user_business_details.userId = users.id  WHERE users.isDeleted = 0 AND user_business_details.state != '' GROUP BY user_business_details.state ORDER BY user_business_details.state ASC";
							$sql = "SELECT * FROM states WHERE country_id=231 ORDER BY name ASC";
							$result = mysqli_query($conn, $sql);
						  ?>
						  <select name="state[]" class="state_list_select" multiple="multiple" disabled required>
							<?php 
								  while($row = mysqli_fetch_assoc($result)) {
								?>
									  <option value="<?php echo $row['name'];?>"><?php echo $row['name'];?></option>
								<?php       
									  }
								?>
						  </select>
						</div>
					</div>
				  
					<div class="clearfix"></div>
					<div class="x_title">
						<h2>State Mapping</h2>
						<div class="clearfix"></div>
					</div>
					<div class="col-xs-2"></div>
					<div class="col-xs-3">Permission</div>
					<div class="col-xs-4"></div>
					<div class="col-xs-3">Display</div>
					<div class="row state_data_div">
						<div class="col-xs-5 state_show_list" style="height:500px; border: 1px solid #cbcbcc; overflow-y: scroll;">
						
						</div>
						<div class="col-xs-2">
						</div>
						<div class="col-xs-5 dealer_selected_list" style="height:500px; border: 1px solid #cbcbcc; overflow-y: scroll;">
						
						</div>
					</div>
            </div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
                <a href="state_mapping_list.php" class="btn btn-danger">Cancel</a>
                <button id="send" type="submit" class="btn btn-success">Save</button>
              </div>
            </div>               
          </form>
        </div>
      </div>
    </div>
    <span class="addRow" style="display:none"></span>
    <!-- footer content -->
    <?php 
    include_once('elements/footer.php');
    ?>
    <!-- /footer content -->
<script>
function add_state() {  
  return !$('#features option:selected').remove().appendTo('#selected_features');  
};  
function remove_state() {    
  return !$('#selected_features option:selected').remove().appendTo('#features');  
};
function show_vendor_by_category(e) {
    var cat = $(e.target).val();
    $.ajax({
      method: "POST",
      url: "vendor_list_filter_state_mapping.php",
      data: { cat : cat }
    }).done(function(data) {
      //console.log(data);
      $('.vendor_list_filter').html(data);
    });
}
function show_vendor_state_map() {
    var vendor_id = $('.vendor_id').val();
	//console.log(val);
    $.ajax({
      method: "POST",
      url: "show_state.php",
      data: { vendor_id : vendor_id }
    }).done(function(data) {
      //console.log(data);
      $('.state_select_div').html(data);
	  $('.state_list_select').multipleSelect({
		width: '100%',
		  onClose: function() {
			var state = $('.state_list_select').val();
			//console.log(val);
			$.ajax({
				method: "POST",
				url: "show_states.php",
				data: { state : state, vendor_id : vendor_id }
			}).done(function(data) {
				$('.state_show_list').html(data);
			});
		  }
		});
		var state = $('.state_list_select').val();
		//console.log(val);
		$.ajax({
			method: "POST",
			url: "show_states.php",
			data: { state : state, vendor_id : vendor_id }
		}).done(function(data) {
			$('.state_show_list').html(data);
			show_dealer();
		});
    });
}
function show_dealer() {
	var vendor_id = $('.vendor_id').val();
	var checkedVals = $('.selected_states:checkbox:checked').map(function() {
		return this.value;
	}).get();
	//console.log(checkedVals);
    $.ajax({
      method: "POST",
      url: "show_dealer.php",
      data: { state : checkedVals, vendor_id : vendor_id }
    }).done(function(data) {
      $('.dealer_selected_list').html(data);
    });
}
$(function() {
  $('.state_list_select').multipleSelect({
    width: '100%',
      onClose: function() {
        var val = $('.state_list_select').val();
		var vendor_id = $('.vendor_id').val();
        //console.log(val);
        $.ajax({
            method: "POST",
            url: "show_states.php",
            data: { state : val, vendor_id : vendor_id }
        }).done(function(data) {
            $('.state_show_list').html(data);
        });
      }
  });
});
function select_all_state() {
	if ($('.selected_states_all').is(':checked')) {
		$(".selected_states").each(function (index, element) {
			$(this).prop("checked", true);
        });
	} else { 
		$(".selected_states").each(function (index, element) {
			$(this).prop("checked", false);
        });
	}
	show_dealer();
}
function select_all_dealer() {
	if ($('.selected_dealer_all').is(':checked')) {
		$(".selected_dealer").each(function (index, element) {
			$(this).prop("checked", true);
        });
	} else { 
		$(".selected_dealer").each(function (index, element) {
			$(this).prop("checked", false);
        });
	}
}
</script>
