<?php 
	include_once('elements/db_connection.php');
	include_once('elements/left_menu.php');
	include_once('elements/header.php');
?>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
                <div class="x_title">
                    <h2>Class Restriction</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?php
                    $classId = $_REQUEST['id'];
                    $sql = "SELECT restrict_class.name, restrict_class.general_range, user_detail.company_name FROM restrict_class LEFT JOIN user_detail ON restrict_class.vendor_id = user_detail.user_id WHERE restrict_class.id = $classId";
                    $result = mysqli_query($conn, $sql);
                    while($rowClass = mysqli_fetch_assoc($result)) {
                        $sqlCMap = "SELECT restrict_class_mapping_for_car_make.make_id, master_car_make.name, restrict_class_mapping_for_car_make.make_range FROM restrict_class_mapping_for_car_make LEFT JOIN master_car_make ON restrict_class_mapping_for_car_make.make_id = master_car_make.id WHERE restrict_class_mapping_for_car_make.restrict_class_id = $classId";
                        $resultCMap = mysqli_query($conn, $sqlCMap);
                        $arData = array();
                        while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
                            $make_id = $rowCMap['make_id'];
                            $arData[$make_id] = array();
                            $arData[$make_id]['name'] = $rowCMap['name'];
                            $arData[$make_id]['range'] = $rowCMap['make_range'];
                            $arData[$make_id]['model'] = array();
                        }
                        $sqlCMap = "SELECT restrict_class_mapping_for_car_model.model_id, master_car_model.model_name, master_car_model.model_make_id, restrict_class_mapping_for_car_model.model_range FROM restrict_class_mapping_for_car_model LEFT JOIN master_car_model ON restrict_class_mapping_for_car_model.model_id = master_car_model.id WHERE restrict_class_mapping_for_car_model.restrict_class_id = $classId";
                        $resultCMap = mysqli_query($conn, $sqlCMap);
                        while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
                            $model_make_id = $rowCMap['model_make_id'];
                            $sqlMake = "SELECT * FROM master_car_make WHERE name = '$model_make_id'";
                            $resultMak = mysqli_query($conn, $sqlMake);
                            while($rowMak = mysqli_fetch_assoc($resultMak)) {
                                $make_id = $rowMak['id'];
                                $model_id = $rowCMap['model_id'];
                                if(!isset($arData[$make_id])) {
                                    $arData[$make_id] = array();
                                    $arData[$make_id]['name'] = $rowMak['name'];
                                    $arData[$make_id]['model'] = array();
                                }
                                $arData[$make_id]['model'][$model_id] = array();
                                $arData[$make_id]['model'][$model_id]['name'] = $rowCMap['model_name'];
                                $arData[$make_id]['model'][$model_id]['range'] = $rowCMap['model_range'];
                                $arData[$make_id]['model'][$model_id]['trim'] = array();
                            }
                        }
                        $sqlCMap = "SELECT restrict_class_mapping_for_car_trim.trim_id, master_car_trim.model_trim, master_car_trim.model_make_id, master_car_trim.model_name, restrict_class_mapping_for_car_trim.trim_range FROM restrict_class_mapping_for_car_trim LEFT JOIN master_car_trim ON restrict_class_mapping_for_car_trim.trim_id = master_car_trim.id WHERE restrict_class_mapping_for_car_trim.restrict_class_id = $classId";
                        $resultCMap = mysqli_query($conn, $sqlCMap);
                        while($rowCMap = mysqli_fetch_assoc($resultCMap)) {
                            $model_make_id = $rowCMap['model_make_id'];
                            $sqlMake = "SELECT * FROM master_car_make WHERE name = '$model_make_id'";
                            $resultMak = mysqli_query($conn, $sqlMake);
                            while($rowMak = mysqli_fetch_assoc($resultMak)) {
                                $make_id = $rowMak['id'];
                                $model_name = $rowCMap['model_name'];
                                $make_name = $rowCMap['model_make_id'];
                                $trim_id = $rowCMap['trim_id'];
                                $sqlModel = "SELECT * FROM master_car_model WHERE model_name = '$model_name' AND model_make_id = '$make_name'";
                                $resultModel = mysqli_query($conn, $sqlModel);
                                while($rowModel = mysqli_fetch_assoc($resultModel)) {
                                    $model_id = $rowModel['id'];
                                    if(!isset($arData[$make_id])) {
                                        $arData[$make_id] = array();
                                        $arData[$make_id]['name'] = $rowMak['name'];
                                        $arData[$make_id]['model'] = array();
                                    }
                                    if(!isset($arData[$make_id]['model'][$model_id])) {
                                        $arData[$make_id]['model'][$model_id] = array();
                                        $arData[$make_id]['model'][$model_id]['name'] = $rowModel['model_name'];
                                        $arData[$make_id]['model'][$model_id]['trim'] = array();
                                    }
                                    $arData[$make_id]['model'][$model_id]['trim'][$trim_id] = array();
                                    $arData[$make_id]['model'][$model_id]['trim'][$trim_id]['name'] = $rowCMap['model_trim'];
                                    $arData[$make_id]['model'][$model_id]['trim'][$trim_id]['range'] = $rowCMap['trim_range'];
                                }
                            }
                        }
                        //echo '<pre>';
                        //print_r($arData);
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title restrict_class" style="cursor: pointer">Restricted vehicles: <?php echo $rowClass['name'];?> | Vendor Name: <?php echo $rowClass['company_name']?><span style="float: right;"><?php if($rowClass['general_range'] != null) { echo 'General Range: '.$rowClass['general_range'];}?></span></h3>
                    </div>
                    <table class="table table-bordered table-condensed toggle_restrict_class" style="display: none;">
                        <thead>
                        <tr>
                            <th class="text-center colfix">Make</th>
                            <th class="text-center colfix">Model</th>
                            <th class="text-center colfix">Trim</th>
                            <th class="text-center colfix">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $arAllowedVehicals = array();
                        foreach($arData as $key=>$val) {
                            $make_count = count($val['model']);
                            $make_name = $val['name'];
                            $sql = "SELECT * FROM master_car_model WHERE model_make_id = '$make_name'";
                            $result = mysqli_query($conn, $sql);
                            while($rowModel = mysqli_fetch_assoc($result)) {
                                $modelId = $rowModel['id'];
                                $model_name = $rowModel['model_name'];
                                if(isset($val['model'][$modelId])) {
                                    $sql = "SELECT * FROM master_car_trim WHERE model_make_id = '$make_name' AND model_name='$model_name'";
                                    $resultTrim = mysqli_query($conn, $sql);
                                    while($rowTrim = mysqli_fetch_assoc($resultTrim)) {
                                        $trimId = $rowTrim['id'];
                                        if(!isset($val['model'][$modelId]['trim'][$trimId])) {
                                            $arAllowedVehicals[$key]['name'] = $make_name;
                                            $arAllowedVehicals[$key]['model'][$modelId]['name'] = $model_name;
                                            $model_trim = $rowTrim['model_trim'];
                                            $arAllowedVehicals[$key]['model'][$modelId]['trim'][$trimId]['name'] = $model_trim;
                                        }
                                    }
                                } else {
                                    $arAllowedVehicals[$key]['name'] = $make_name;
                                    $arAllowedVehicals[$key]['model'][$modelId]['name'] = $model_name;
                                    $sql = "SELECT * FROM master_car_trim WHERE model_make_id = '$make_name' AND model_name='$model_name'";
                                    $resultTrim = mysqli_query($conn, $sql);
                                    while($rowTrim = mysqli_fetch_assoc($resultTrim)) {
                                        $trimId = $rowTrim['id'];
                                        $model_trim = $rowTrim['model_trim'];
                                        $arAllowedVehicals[$key]['model'][$modelId]['trim'][$trimId]['name'] = $model_trim;
                                    }
                                }
                            }
                            $i = 0;
                            if(!empty($val['model'])) {
                            foreach($val['model'] as $md) {
                                if($i != 0) {
                        ?>
                                <tr>
                                    <td>
                                        <?php echo $md['name'];?><?php if(isset($md['range']) && $md['range'] != '') echo ': '.$md['range'];?>
                                    </td>
                                    <td>
                                    <?php
                                        foreach($md['trim'] as $tr) {
                                    ?>
                                        <?php echo $tr['name'];?><?php if(isset($tr['range']) && $tr['range'] != '') echo ': '.$tr['range'];?>, 
                                    <?php } ?>
                                    </td>
									<td>
									Restricted
                                    </td>
                                </tr>
                        <?php } else {
                            ?>
                                <tr>
                                    <td rowspan="<?php echo $make_count;?>">
                                        <?php echo $val['name'];?><?php if(isset($val['range']) && $val['range'] != '') echo ': '.$val['range'];?>
                                    </td>
                                    <td>
                                        <?php echo $md['name'];?><?php if(isset($md['range']) && $md['range'] != '') echo ': '.$md['range'];?>
                                    </td>
                                    <td>
                                    <?php
                                        foreach($md['trim'] as $tr) {
                                    ?>
                                        <?php echo $tr['name'];?><?php if(isset($tr['range']) && $tr['range'] != '') echo ': '.$tr['range'];?>, 
                                    <?php } ?>
                                    </td>
									<td>
									Restricted
                                    </td>
                                </tr>
                        <?php
                        } $i++; } } else {
                        ?>
                            <tr>
                                <td rowspan="<?php echo $make_count;?>">
                                    <?php echo $val['name'];?><?php if(isset($val['range']) && $val['range'] != '') echo ': '.$val['range'];?>
                                </td>
                                <td></td>
                                <td></td>
								<td>
								Restricted
								</td>
                            </tr>
                        <?php
                        } } 
                        ?>
                        </tbody>
                    </table>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    $sql = "SELECT * FROM master_car_make";
                    $result = mysqli_query($conn, $sql);
                    while($rowMake = mysqli_fetch_assoc($result)) {
                        $makeId = $rowMake['id'];
                        $make_name = $rowMake['name'];
                        if(!isset($arData[$makeId])) {
                            $arAllowedVehicals[$makeId]['name'] = $make_name;
                            $sql = "SELECT * FROM master_car_model WHERE model_make_id = '$make_name'";
                            $resultModel = mysqli_query($conn, $sql);
                            while($rowModel = mysqli_fetch_assoc($resultModel)) {
                                $modelId = $rowModel['id'];
                                $model_name = $rowModel['model_name'];
                                $arAllowedVehicals[$makeId]['name'] = $make_name;
                                $arAllowedVehicals[$makeId]['model'][$modelId]['name'] = $model_name;
                                $sql = "SELECT * FROM master_car_trim WHERE model_make_id = '$make_name' AND model_name='$model_name'";
                                $resultTrim = mysqli_query($conn, $sql);
                                while($rowTrim = mysqli_fetch_assoc($resultTrim)) {
                                    $trimId = $rowTrim['id'];
                                    $model_trim = $rowTrim['model_trim'];
                                    $arAllowedVehicals[$makeId]['model'][$modelId]['trim'][$trimId]['name'] = $model_trim;
                                }
                            }
                        }
                    }
                    //echo '<pre>';
                    //print_r($arAllowedVehicals);
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title allowed_vehicals" style="cursor: pointer">Allowed Vehicles</h3>
                        </div>
                        <table class="table table-bordered table-condensed toggle_allowed_vehicals" style="display: none;">
                            <thead>
                                <tr>
                                    <th class="text-center colfix">Make</th>
                                    <th class="text-center colfix">Model</th>
                                    <th class="text-center colfix">Trim</th>
                                    <th class="text-center colfix">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                //echo '<pre>';
                                //print_r($arAllowedVehicals); exit;
                                foreach($arAllowedVehicals as $val) {
                                    $make_count = count($val['model']);
                                ?>
                                <?php
                                $i = 0;
                                    if(!empty($val['model'])) {
                                    foreach($val['model'] as $md) {
                                        if($i != 0) {
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $md['name'];?><?php if(isset($md['range']) && $md['range'] != '') echo ': '.$md['range'];?>
                                    </td>
                                    <td>
                                    <?php
                                    if(!empty($md['trim'])) {
                                        foreach($md['trim'] as $tr) {
                                    ?>
                                        <?php echo $tr['name'];?><?php if(isset($tr['range']) && $tr['range'] != '') echo ': '.$tr['range'];?>, 
                                    <?php } } ?>
                                    </td>
									<td>
									Allowed
									</td>
                                </tr>
                                <?php } else { ?>
                                <tr>
                                    <td rowspan="<?php echo $make_count;?>">
                                        <?php echo $val['name'];?><?php if(isset($val['range']) && $val['range'] != '') echo ': '.$val['range'];?>
                                    </td>
                                    <td>
                                        <?php echo $md['name'];?><?php if(isset($md['range']) && $md['range'] != '') echo ': '.$md['range'];?>
                                    </td>
                                    <td>
                                    <?php
                                    if(!empty($md['trim'])) {
                                        foreach($md['trim'] as $tr) {
                                    ?>
                                        <?php echo $tr['name'];?><?php if(isset($tr['range']) && $tr['range'] != '') echo ': '.$tr['range'];?>, 
                                    <?php } } ?>
                                    </td>
									<td>
									Allowed
									</td>
                                </tr>
                                <?php } $i++; } } else { ?>
                                <tr>
                                    <td rowspan="<?php echo $make_count;?>">
                                        <?php echo $val['name'];?><?php if(isset($val['range']) && $val['range'] != '') echo ': '.$val['range'];?>
                                    </td>
                                    <td></td>
                                    <td></td>
									<td>
									Allowed
									</td>
                                </tr>
                                <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="engine_type panel-title" onclick="toggleEngine('commercialRestriction')" style="cursor: pointer">Commercial Restriction</h3>
                        </div>
                        <div class="commercialRestriction" style="display: none;">
                        <?php
                        $sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_class_id = $classId AND master_engine_type.type='Commercial Restriction'";
                        $resultE = mysqli_query($conn, $sqlE);
                        // output data of each row
                        $dataFound = false;
                        while($rowE = mysqli_fetch_assoc($resultE)) { 
						if($rowE['engine_type_restricted'] == 'yes') {
						$dataFound = true;
						?>
                            <?php echo $rowE['name'];?> ,
                        <?php 
							} } 
							if(!$dataFound) {
								echo 'Nothing Found.';
							}
						?>
                        </div>
                    </div>
					<div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="engine_type panel-title" onclick="toggleEngine('engineTypeRestriction')" style="cursor: pointer">Engine Type Restriction</h3>
                        </div>
                        <div class="engineTypeRestriction" style="display: none;">
                        <?php
                        $sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_class_id = $classId AND master_engine_type.type='Engine Type Restriction'";
                        $resultE = mysqli_query($conn, $sqlE);
                        // output data of each row
						$dataFound = false;
                        while($rowE = mysqli_fetch_assoc($resultE)) {
                        if($rowE['engine_type_restricted'] == 'yes') {
							$dataFound = true;
						?>
                            <?php echo $rowE['name'];?> ,
                        <?php 
							} } 
							if(!$dataFound) {
								echo 'Nothing Found.';
							}
						?>
                        </div>
                    </div>
					<div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="engine_type panel-title" onclick="toggleEngine('driveTrein')" style="cursor: pointer">DriveTrein</h3>
                        </div>
                        <div class="driveTrein" style="display: none;">
                        <?php
                        $sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_class_id = $classId AND master_engine_type.type='Drive Trein'";
                        $resultE = mysqli_query($conn, $sqlE);
                        // output data of each row
                        $dataFound = false;
                        while($rowE = mysqli_fetch_assoc($resultE)) {
                        if($rowE['engine_type_restricted'] == 'yes') {
							$dataFound = true;
						?>
                            <?php echo $rowE['name'];?> ,
                        <?php 
							} } 
							if(!$dataFound) {
								echo 'Nothing Found.';
							}
						?>
                        </div>
                    </div>
					<div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="engine_type panel-title" onclick="toggleEngine('packagesRestriction')" style="cursor: pointer">Millage & Packages Restriction</h3>
                        </div>
                        <div class="packagesRestriction" style="display: none;">
                        <?php
                        $sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_class_id = $classId AND master_engine_type.type='Millage & Packages Restriction'";
                        $resultE = mysqli_query($conn, $sqlE);
                        // output data of each row
                        $dataFound = false;
                        while($rowE = mysqli_fetch_assoc($resultE)) {
                        if($rowE['engine_type_restricted'] == 'yes') {
							
						?>
                            <?php echo $rowE['name'];?> ,
                        <?php 
							} } 
							if(!$dataFound) {
								echo 'Nothing Found.';
							}
						?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 style="cursor: pointer" class="additional_restriction panel-title">Additional Restriction</h3>
                    </div>
                        <table class="toggle_additional_restriction table table-bordered table-condensed" style="display: none;">
                        <thead>
                        <tr>
                            <th class="text-center colfix">Engine type (Allowed)</th>
                            <th class="text-center colfix">Surcharge($)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_class_id = $classId AND master_engine_type.type = 'main'";
                        $resultE = mysqli_query($conn, $sqlE);
                        // output data of each row
                        while($rowE = mysqli_fetch_assoc($resultE)) {
                        ?>
                        <tr>
                        <?php if($rowE['engine_type_restricted'] != 'yes') {?>
                            <td><?php echo $rowE['name'];?></td>
                            <td><?php echo $rowE['surcharge'];?></td>
                        <?php } ?>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
                <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 style="cursor: pointer" class="additional_surcharge panel-title">Additional Surcharge</h3>
                </div>
                    <table class="toggle_additional_surcharge table table-bordered table-condensed" style="display: none;">
                        <thead>
                        <tr>
                            <th class="text-center colfix"></th>
                            <th class="text-center colfix">Surcharge($)</th>
                        </tr>
                        </thead>
                    <tbody>
                    <?php
                    $sqlE = "SELECT restrict_engine_type_mapping_for_vendor.engine_type_restricted, restrict_engine_type_mapping_for_vendor.surcharge, master_engine_type.name FROM restrict_engine_type_mapping_for_vendor LEFT JOIN master_engine_type ON restrict_engine_type_mapping_for_vendor.engine_type_id = master_engine_type.id WHERE restrict_engine_type_mapping_for_vendor.restrict_class_id = $classId AND master_engine_type.type = 'additional'";
                    $resultE = mysqli_query($conn, $sqlE);
                    // output data of each row
                    while($rowE = mysqli_fetch_assoc($resultE)) {
                    ?>
                    <tr>
                    <?php if($rowE['engine_type_restricted'] != 'yes') {?>
                        <td><?php echo $rowE['name'];?></td>
                        <td><?php echo $rowE['surcharge'];?></td>
                    <?php } ?>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <?php 
        } 
        ?>
    </div>
</div>
        <!-- /page content -->
        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
<script>
$(document).on('click', '.engine_type', function(e) {
    $('.toggle_engine_type').toggle();
});
$(document).on('click', '.additional_restriction', function(e) {
    $('.toggle_additional_restriction').toggle();
});
$(document).on('click', '.additional_surcharge', function(e) {
    $('.toggle_additional_surcharge').toggle();
});
$(document).on('click', '.allowed_vehicals', function(e) {
    $('.toggle_allowed_vehicals').toggle();
});
$(document).on('click', '.restrict_class', function(e) {
    $('.toggle_restrict_class').toggle();
});
function toggleEngine(name) {
	$('.' + name).toggle();
};
</script>