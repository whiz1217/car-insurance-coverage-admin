<?php
include_once('elements/db_connection.php');
$vendor_id = $_REQUEST['id'];
if($vendor_id != '') {
    ?>
    <select onchange="show_plan(event, <?php echo $vendor_id?>)" name="class_id" class="form-control" required>
    <option value="">Please select a class</option>
    <?php 
    $sql = "SELECT * FROM restrict_class where `vendor_id` = $vendor_id";
    $result = mysqli_query($conn, $sql);
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
    ?>
        <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
    <?php       
        }
} else {
    ?>
    <select name="class_id" class="form-control" required disabled>
    <option value="">Please select a class</option>
  </select>
    <?php
}
?>