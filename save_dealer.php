<?php
    include_once('elements/db_connection.php');
   
    $businessName = '';
    $businessStreetAddress1 = NULL;
    $businessStreetAddress2 = NULL;
    
    $businessState = '';
    $businessCity = '';
    $businessZipCode = '';
    $businessPhone = '';
    $businessFax = '';
    $businessEmail = '';
    $businessPassword = '';
    $businessEinTaxId = '';

    $userRole = 3;
    if(isset($_POST['businessName'])){
        $businessName = strtoupper($_POST['businessName']);
    }

    if(isset($_POST['streetAddres1'])){
        $businessStreetAddress1 = strtoupper($_POST['streetAddres1']);
    }
    $dealerType = $_POST['dealerType'];
    if(isset($_POST['streetAddres2'])){
        $businessStreetAddress2 = strtoupper($_POST['streetAddres2']);
    }

    
    if(isset($_POST['businessState'])){
        $businessState = strtoupper($_POST['businessState']);
    }

    if(isset($_POST['businessCity'])){
        $businessCity = strtoupper($_POST['businessCity']);
    }

    if(isset($_POST['zipCode'])){
        $businessZipCode = $_POST['zipCode'];
    }
    if(isset($_POST['telephone'])){
        $businessPhone = $_POST['telephone'];
    }

    if(isset($_POST['fax'])){
        $businessFax = $_POST['fax'];
    }
    
    if(isset($_POST['email'])){
        $businessEmail = $_POST['email'];
    }
    if(isset($_POST['password'])){
        $businessPassword = $_POST['password'];
    }
    if(isset($_POST['einTaxId'])){
        $businessEinTaxId = $_POST['einTaxId'];
    }
// =================================
    $personalName = '';
    $personalEmail = '';
    $personalPhoneNo = '';
    $personalStreetAddress1 = NULL;
    $personalStreetAddress2 = NULL;
    
    $personalState = '';
    $personalCity ='';
    $personalZipCode = '';
    $iAgreeTnC = '';

    if(isset($_POST['name'])){
        $personalName = strtoupper($_POST['name']);
    }
    if(isset($_POST['personalEmail'])){
        $personalEmail = $_POST['personalEmail'];
    }
    if(isset($_POST['personalPhoneNo'])){
        $personalPhoneNo = $_POST['personalPhoneNo'];
    }
    if(isset($_POST['personalStreetAddres1'])){
        $personalStreetAddress1 = strtoupper($_POST['personalStreetAddres1']);
    }
    if(isset($_POST['personalStreetAddres2'])){
        $personalStreetAddress2 = strtoupper($_POST['personalStreetAddres2']);
    }

    if(isset($_POST['personalState'])){
        $personalState = strtoupper($_POST['personalState']);
    }
    if(isset($_POST['personalCity'])){
        $personalCity = strtoupper($_POST['personalCity']);
    }
    if(isset($_POST['personalZipCode'])){
        $personalZipCode = $_POST['personalZipCode'];
    }
    if(isset($_POST['iAgreeTnC'])){
        $iAgreeTnC = $_POST['iAgreeTnC'];
    }
    $tbl_name="users";
    $sql="SELECT * FROM $tbl_name WHERE email = '".$personalEmail."';";
    $result = mysqli_query($conn,$sql);
    if (!mysqli_num_rows($result) > 0) {
        $sqlUsers = "
        INSERT INTO users (user_name, email, password, user_role_id)
        VALUES(
                '$personalName',
                '$personalEmail',
                '$businessPassword',
                 $userRole
        ); ";
        
        $result = mysqli_query($conn, $sqlUsers);
        $user_id = mysqli_insert_id($conn);
      
        if($user_id > 0)
        {
            $sqlUserDetails = "
            INSERT INTO user_detail(user_id, street_address_1, street_address_2, state, city, zip_code, telephone)
            VALUES(
                $user_id,
                '$personalStreetAddress1',
                '$personalStreetAddress2',
                '$personalState',
                '$personalCity',
                '$personalZipCode',
                '$personalPhoneNo'
            );";
           
            $result = mysqli_query($conn, $sqlUserDetails);
        }
        // =============================================
        // User uploaded files
        
        if($user_id > 0)
        {
            $fileUploadedPath = '';
            $check = file_exists($_FILES["businessDocFiles"]["tmp_name"]);
            if($check !== false) {
                $fileUploadedPath = '../dealer/db_validation/user_uploaded_documents/user_file_';
                $temp = explode(".", $_FILES["businessDocFiles"]["name"]);
                $fileUploadedPath .= date('m-d-Y_hia').'.'. end($temp);
                $fileUploadedPathSave = 'user_uploaded_documents/user_file_';
                $temp = explode(".", $_FILES["businessDocFiles"]["name"]);
                $fileUploadedPathSave .= date('m-d-Y_hia').'.'. end($temp);
                move_uploaded_file($_FILES["businessDocFiles"]["tmp_name"], $fileUploadedPath);
            }
            if(isset($_FILES['businessDocFiles']['name']))
            {
                $sqlBusinessDetails = "
                INSERT INTO user_business_details
                (userId, businessName, streetAddres1, streetAddres2, city, state,
                 zipCode, telephone, fax, email, password, einTaxId, documents, dealerType
                ) 
                VALUES(
                     $user_id,
                    '$businessName',
                    '$businessStreetAddress1',
                    '$businessStreetAddress2',
                    '$businessCity',
                    '$businessState',
                    '$businessZipCode',
                    '$businessPhone',
                    '$businessFax',
                    '$businessEmail',
                    '$businessPassword',
                    '$businessEinTaxId',
                    '$fileUploadedPathSave', 
                    '$dealerType' 
                );";
            $result = mysqli_query($conn, $sqlBusinessDetails);
            }
        }
        header( "Location: dealer_list.php" );
    }
    $conn->close(); // Connection Closed
?>