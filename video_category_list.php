<?php 
include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_title">
                <h2>Video Category List</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="class_list_table">
                <table id="restrict_class_list">
                    <thead>
                        <tr>
                            <th>Sl. No.</th>
                            <th>Name</th>
                            <th>Date Created</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $sql = "SELECT * FROM master_video_category WHERE is_deleted='0'";
                            $result = mysqli_query($conn, $sql);
                            $slNo = 1;
                            if (mysqli_num_rows($result) > 0) {
                                // output data of each row
                                while($row = mysqli_fetch_assoc($result)) {
                            ?>
                                <tr>
                                    <td><?php echo $slNo;?></td>
                                    <td><?php echo $row['name'];?></td>
                                    <td><?php echo $row['created_at'];?></td>
                                    <td>
										<a class='btn btn-primary' onclick="edit_video_category(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
										<a class='btn btn-danger' onclick="delete_video_category(<?php echo $row['id'];?>)" style="cursor:pointer;">Delete</a> 
                                    </td>
                                </tr>
                            <?php
                            $slNo++;
                                }
                            }
                        ?>
                    </tbody>
                    </table>
                </div>
                    
              </div>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content class-modal" >
                Loading...
            </div>

        </div>
        <!-- /page content -->
		
        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
<script>
var table;
$(document).ready(function(){
    table = $('#restrict_class_list').DataTable();
});  
function edit_video_category(id) {
    $.ajax({
        method: "POST",
        url: "edit_video_category.php",
        data: { id : id }
    }).done(function(data) {
        $('#myModal').html(data);
    });
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}
function delete_video_category(id) {
    $.ajax({
        method: "POST",
        url: "video-category-delete.php",
        data: { id : id }
    }).done(function(data) {
        $('#myModal').html(data);
    });
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}
function close_popup() {
    $('#myModal').html('Loading...');
    var modal = document.getElementById('myModal');
    modal.style.display = "none";
}
</script>