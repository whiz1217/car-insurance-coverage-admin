<?php 
	include_once('elements/db_connection.php');
	include_once('elements/left_menu.php');
	include_once('elements/header.php');
?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_content">
              <form class="form-horizontal form-label-left" action="save_A2A.php" method="post">
                <div class="item form-group">
                  <label class="col-md-2">Category <span class="required">*</span></label>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <select onchange="show_vendor_by_category(event)" class="form-control cat_id" required>
                          <option value="">PLEASE SELECT A CATEGORY</option>
                          <option value="VSC">VSC</option>
                          <option value="GAP">GAP</option>
                          <option value="ANCILLARY">ANCILLARY</option>
                          <option value="INSURANCE">INSURANCE</option>
                      </select>
                  </div>
                </div>
                <div class="item form-group">
                  <label class="col-md-2">Choose vendor <span class="required">*</span></label>
                  <div class="col-md-4 col-sm-6 col-xs-12 vendor_list_filter">
                      <select class="form-control" required disabled>
                          <option value="">PLEASE SELECT A VENDOR</option>
                      </select>
                  </div>
                </div>
                  <!-- <div class="item form-group">
                    <label class="col-md-2">Choose Class</label>
                    <div class="col-md-4 col-sm-6 col-xs-12 restrict_class_d">
                      <select name="class_id" class="form-control" required disabled>
                        <option value="">Please select a class</option>
                      </select>
                    </div>
                  </div> -->
                <div class="item form-group">
                    <label class="col-md-2">Choose Plan *</label>
                    <div class="col-md-4 col-sm-6 col-xs-12 plan_d">
                      <select name="plan_id" class="form-control" required disabled>
                        <option value="">PLEASE SELECT A PLAN</option>
                      </select>
                    </div>
                  </div>
                  <div class="x_title">
                    <h2>Apples To Apples</h2>
                    <div class="clearfix"></div>
                  </div>
                  <ul class="tree_new">
                  <?php 
                    $sql = "SELECT * FROM appletoappletype";
                    $result = mysqli_query($conn, $sql);
                        // output data of each row
                        while($row = mysqli_fetch_assoc($result)) {
                  ?>
                    <li class="has">
                      <input type="checkbox" name="A2AType[]" value="<?php echo $row['id'];?>">
                      <label>
                      <?php
                        if(file_exists('images/a2a/' . $row['name'] . '.png')) {
                          ?>
                          <img src="<?php echo 'images/a2a/' . $row['name'] . '.png';?>">
                          <?php
                        } else {
                          echo $row['name'];
                        }
                      ?>
                      </label>
                      <ul>
                  <?php 
                    $appleToAppleType = $row['id'];
                    $sqlType = "SELECT * FROM appletoapplevalue WHERE appleToAppleType= $appleToAppleType";
                    $resultType = mysqli_query($conn, $sqlType);
                        // output data of each row
                        while($rowType = mysqli_fetch_assoc($resultType)) {
                  ?>
                      <li class="row">
                        <span class="col-md-7">
                          <input type="checkbox" name="A2ATypeValue[<?php echo $row['id'];?>][]" value="<?php echo $rowType['id'];?>">
                          <label><?php echo $rowType['name'];?></label>
                        </span>
                        <input class="col-md-3" name="A2ATypeComment[<?php echo $rowType['id'];?>]" placeholder="Type comments" type="text">
                      </li>
              <?php } ?>
                      </ul> 
                    </li>
            <?php } ?>
                  </ul>  
                  <div class="clearfix"></div>
                  <h2>Highlight Coverage</h2>
                  <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="item form-group">
                      <label class="col-md-2">Labour Ready</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="labourReady" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('labourReady')">
                      Unlimited <input type="radio" name="labourReady" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('labourReady')"> 
                          <input type="text" style="display: none" name="labourReadyCharge" class="labourReadyCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Wheels</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                          Limited <input type="radio" name="wheels" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('wheels')">
                          Unlimited <input type="radio" name="wheels" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('wheels')"> 
                          <input type="text" style="display: none" name="wheelsCharge" class="wheelsCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Tiers</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="tiers" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('tiers')">
                      Unlimited <input type="radio" name="tiers" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('tiers')"> 
                          <input type="text" style="display: none" name="tiersCharge" class="tiersCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Road Hazard</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="roadHazard" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('roadHazard')">
                      Unlimited <input type="radio" name="roadHazard" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('roadHazard')"> 
                          <input type="text" style="display: none" name="roadHazardCharge" class="roadHazardCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Cosmetic Wheel Repair</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="cosmeticWheelRepair" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('cosmeticWheelRepair')">
                      Unlimited <input type="radio" name="cosmeticWheelRepair" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('cosmeticWheelRepair')"> 
                          <input type="text" style="display: none" name="cosmeticWheelRepairCharge" class="cosmeticWheelRepairCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Curb Impact Repair & Replacement</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="curbImpactRepair" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('curbImpactRepair')">
                      Unlimited <input type="radio" name="curbImpactRepair" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('curbImpactRepair')"> 
                          <input type="text" style="display: none" name="curbImpactRepairCharge" class="curbImpactRepairCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Paintless Dent Repair & Ding</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="paintlessDentRepair" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('paintlessDentRepair')">
                      Unlimited <input type="radio" name="paintlessDentRepair" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('paintlessDentRepair')"> 
                          <input type="text" style="display: none" name="paintlessDentRepairCharge" class="paintlessDentRepairCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Rental</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="rental" onclick="fnShowHideInput(event, 'rentalCharge')" style="cursor: pointer;">
                        <input type="text" style="display: none" name="rentalCharge" class="rentalCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Road Side Assitance</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="roadSideAssitance" onclick="fnShowHideInput(event, 'roadSideAssitanceCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="roadSideAssitanceCharge" class="roadSideAssitanceCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Toing</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="toing" onclick="fnShowHideInput(event, 'toingCharge')" style="cursor: pointer;">
                        <input type="text" style="display: none" name="toingCharge" class="toingCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Other Highlights</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="otherHighlight" onclick="fnShowHideInput(event, 'otherHighlightCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="otherHighlightCharge" class="otherHighlightCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Tier & Wheel</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="tierWheel" onclick="fnShowHideInput(event, 'tierWheelCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="tierWheelCharge" class="tierWheelCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Windshild Chip</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="windshildChip" onclick="fnShowHideInput(event, 'windshildChipCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="windshildChipCharge" class="windshildChipCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Windshild Protection</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="windshildProtection" onclick="fnShowHideInput(event, 'windshildProtectionCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="windshildProtectionCharge" class="windshildProtectionCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Key Replacement</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="keyReplacement" onclick="fnShowHideInput(event, 'keyReplacementCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="keyReplacementCharge" class="keyReplacementCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Lease End Protection</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="leaseEndProtection" onclick="fnShowHideInput(event, 'leaseEndProtectionCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="leaseEndProtectionCharge" class="leaseEndProtectionCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Effective Day</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      <input type="text" name="effectiveDay" class="effectiveDay form-control">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Store Type</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <select name="storeType" style="cursor: pointer;">
                          <option value="">--select--</option>
                          <option value="independent">Independent</option>
                          <option value="franchise">Franchise</option>
                          <option value="both">Both</option>
                        </select>
                      </div>
                    </div>
                  </div>      
                  <div class="clearfix"></div>
                  <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                      <a class="btn btn-danger">Cancel</a>
                      <button id="send" type="submit" class="btn btn-success">Save</button>
                    </div>
                  </div>               
                </form>
              </div>
            </div>
        </div>
        <span class="addRow" style="display:none"></span>
        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
<script>
function show_class(e) {
  var val = $(e.target).val();
  $.ajax({
    method: "POST",
    url: "class_list.php",
    data: { id : val }
  }).done(function(data) {
    $('.restrict_class_d').html(data);
  });
}
function show_plan(e) {
  var val = $(e.target).val();
  $.ajax({
    method: "POST",
    url: "plan_d_list.php",
    data: { vendor_id : val }
  }).done(function(data) {
    $('.plan_d').html(data);
  });
}
$(document).on('click', '.tree_new label', function(e) {
  $(this).next('ul').fadeToggle();
  e.stopPropagation();
});
$(document).on('change', '.tree_new input[type=checkbox]', function(e) {
  $(this).siblings('ul').find("input[type='checkbox']").prop('checked', this.checked);
  //$(this).parentsUntil('.tree_new').children("input[type='checkbox']").prop('checked', this.checked);
  e.stopPropagation();
});
function fnShowLimitBox(prName) { 
  $('.'+prName+'Charge').show();
}
function fnShowHideInput(e, name) {
  if($(e.target).prop('checked') == true) {
    $('.'+name).show();
  } else {
    $('.'+name).hide();
  }
}
function fnHideLimitBox(prName) { 
  $('.'+prName+'Charge').hide();
}
function show_vendor_by_category(e) {
  var cat = $(e.target).val();
  $.ajax({
    method: "POST",
    url: "vendor_list_filter_a2a.php",
    data: { cat : cat }
  }).done(function(data) {
    //console.log(data);
    $('.vendor_list_filter').html(data);
  });
}
</script>
