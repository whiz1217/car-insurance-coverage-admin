<?php
include_once('elements/db_connection.php');
if(isset($_GET['api']) && $_GET['api'] == 'getPlanListData') {
    $vendor_id = ($_GET['vendor_id'])?$_GET['vendor_id']:null;
    $tbl_name="restrict_class";
    $sql="SELECT id,name as class FROM $tbl_name WHERE vendor_id=$vendor_id";
    $result = mysqli_query($conn,$sql);
    $getData = mysqli_fetch_all($result,MYSQLI_ASSOC);
    $returnData = array('classList'=>$getData);
    echo json_encode($returnData);
    exit;
} else if(isset($_POST['api']) && $_POST['api'] == 'insertPlanData') {
	//echo "<pre>";
	//print_r($_POST);
	//exit;
    $planName = strtoupper($_POST['PlanName']);
    $vendor_id = $_POST['vendor_id'];
	$TermSelect = null;
	if(isset($_POST['TermSelect'])) {
         $TermSelect = $_POST['TermSelect'];
    }
	$TermSelectAnciallary = null;
	if(isset($_POST['TermSelectAnciallary'])) {
         $TermSelectAnciallary = $_POST['TermSelectAnciallary'];
    }
	$TermSelectInsurance = null;
	if(isset($_POST['TermSelectInsurance'])) {
         $TermSelectInsurance = $_POST['TermSelectInsurance'];
    }
	$TermFromOption = null;
	if(isset($_POST['TermFromOption'])) {
         $TermFromOption = $_POST['TermFromOption'];
    }
	$TermToOption = null;
	if(isset($_POST['TermToOption'])) {
         $TermToOption = $_POST['TermToOption'];
    }
    $term = [];
	if(isset($_POST['term'])) {
         $term = $_POST['term'];
    }
    $class = [];
	if(isset($_POST['class'])) {
         $class = $_POST['class'];
    }
    $coverMailage = [];
	if(isset($_POST['coverMailage'])) {
         $coverMailage = $_POST['coverMailage'];
    }
    $upToMailage = [];
	if(isset($_POST['upToMailage'])) {
         $upToMailage = $_POST['upToMailage'];
    }
    $effectiveDay = [];
	if(isset($_POST['effectiveDay'])) {
         $effectiveDay = $_POST['effectiveDay'];
    }
    $mailageFrom = [];
	if(isset($_POST['mailageFrom'])) {
         $mailageFrom = $_POST['mailageFrom'];
    }
    $mailageTo = [];
	if(isset($_POST['mailageTo'])) {
         $mailageTo = $_POST['mailageTo'];
    }
    $price = [];
	if(isset($_POST['price'])) {
         $price = $_POST['price'];
    }
    $deductable = [];
	if(isset($_POST['deductable'])) {
         $deductable = $_POST['deductable'];
    }
    $manufacturerWarranty = [];
	if(isset($_POST['manufacturerWarranty'])) {
         $manufacturerWarranty = $_POST['manufacturerWarranty'];
    }
    $termFrom = [];
	if(isset($_POST['termFrom'])) {
         $termFrom = $_POST['termFrom'];
    }
    $termTo = [];
	if(isset($_POST['termTo'])) {
         $termTo = $_POST['termTo'];
    }
    $coverageLimitNone = [];
	if(isset($_POST['coverageLimitNone'])) {
         $coverageLimitNone = $_POST['coverageLimitNone'];
    }
    $coverageLimit = [];
	if(isset($_POST['coverageLimit'])) {
         $coverageLimit = $_POST['coverageLimit'];
    }
    $storeType = [];
	if(isset($_POST['storeType'])) {
         $storeType = $_POST['storeType'];
    }
    $payment = [];
	if(isset($_POST['payment'])) {
         $payment = $_POST['payment'];
    }
    $options = [];
	if(isset($_POST['options'])) {
         $options = $_POST['options'];
    }
    $sub_category = [];
	if(isset($_POST['sub_category'])) {
         $sub_category = $_POST['sub_category'];
    }
	
	$insertPlaneData = [];

    $sqlQuery = "INSERT INTO plans (name) VALUES ('$planName')";
    if ($conn->query($sqlQuery) === TRUE) {
        $insertID = $conn->insert_id;

        if(count($term)){
            foreach($term as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['TermSelect'] = $TermSelect;
					$insertPlaneData[$index]['termNo'] = $data;
				}
            }
        }
        if(count($class)){
            foreach($class as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['classId'] = $data;
				}
            }
        }
        if(count($coverMailage)){
            foreach($coverMailage as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['coverMailage'] = $data;
				}
            }
        }
        if(count($upToMailage)){
            foreach($upToMailage as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['upToMailage'] = $data;
				}
            }
        }
        if(count($manufacturerWarranty)){
            foreach($manufacturerWarranty as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['manufacturerWarranty'] = 'yes';
				}
            }
        }
        if(count($mailageFrom)){
            foreach($mailageFrom as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['milage_from'] = $data;
				}
            }
        }
        if(count($mailageTo)){
            foreach($mailageTo as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['milage_to'] = $data;
				}
            }
        }
        if(count($price)){
            foreach($price as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['price'] = $data;
					$insertPlaneData[$index]['price_unit'] = '$';
				}
            }
        }
        if(count($deductable)){
            foreach($deductable as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['deductable'] = $data;
				}
            }
        }
		if(count($termFrom)){
            foreach($termFrom as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['termFrom'] = $data;
				}
            }
        }
		if(count($termTo)){
            foreach($termTo as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['termTo'] = $data;
				}
            }
        }
		if(count($coverageLimitNone)){
            foreach($coverageLimitNone as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['coverageLimitNone'] = $data;
				}
            }
        }
		if(count($coverageLimit)){
            foreach($coverageLimit as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['coverageLimit'] = $data;
				}
            }
        }
		if(count($storeType)){
            foreach($storeType as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['storeType'] = $data;
				}
            }
        }
		if(count($payment)){
            foreach($payment as $index=>$data){
				if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['payment'] = $data;
				}
            }
        }
		if(count($options)){
            foreach($options as $index=>$data){
				//if($data != '' && $data != null) {
					$insertPlaneData[$index]['plansId'] = $insertID;
					$insertPlaneData[$index]['vendorId'] = $vendor_id;
					$insertPlaneData[$index]['options'] = $data;
					$insertPlaneData[$index]['sub_category'] = $sub_category;
				//}
            }
        }
    }
    if(count($insertPlaneData)) {
        foreach($insertPlaneData as $data){
            $insertID = insertDataGetID($conn, 'plan_terms', $data);
            if($insertID) echo "Success";
        }
    }
    // for users documents
    // ==============
    //echo '<pre>';
    //print_r($_FILES);
    //exit;
    $uploaddir = 'user_documents/user_doc/user_';
    $check = file_exists($_FILES["broacherDocument"]["tmp_name"][0]);
    if($check !== false) {
        foreach($_FILES["broacherDocument"]["name"] as $key=>$val) {
            $temp = explode(".", $_FILES["broacherDocument"]["name"][$key]);
            $uploaddir .= date('m-d-Y_hia').'.'. end($temp);
            //print_r($uploaddir);exit;
            move_uploaded_file($_FILES["broacherDocument"]["tmp_name"][$key], '../'.$uploaddir);
            $sqlUserDetail = "INSERT INTO plan_brouchure_info (planId, documentPath, isDeleted) VALUES ('$insertID', '$uploaddir', 0)";
            mysqli_query($conn, $sqlUserDetail);
        }
    }
    header('Location: plan_list.php');
    exit;
} else if(isset($_POST['api']) && $_POST['api'] == 'editPlanData') {
    $insertPlaneData = [];
    $PlanID = $_POST['PlanID'];
    $planName = strtoupper($_POST['PlanName']);
    $vendor_id = $_POST['vendor_id'];
    if($vendor_id == '' || $vendor_id == null) {
        echo "<script>alert('Please select a vendor!');</script>";
        header('Location: plan_list.php');
        exit;
    }
    $TermSelect = 'days';
    $planTermsID = isset($_POST['planTermsID'])?$_POST['planTermsID']:array();
    $term = isset($_POST['term'])?$_POST['term']:array();
    $class = isset($_POST['class'])?$_POST['class']:array();
    $coverMailage = isset($_POST['coverMailage'])?$_POST['coverMailage']:array();
    $upToMailage = isset($_POST['upToMailage'])?$_POST['upToMailage']:array();
    $effectiveDay = isset($_POST['effectiveDay'])?$_POST['effectiveDay']:array();
    $mailageFrom = isset($_POST['mailageFrom'])?$_POST['mailageFrom']:array();
    $mailageTo = isset($_POST['mailageTo'])?$_POST['mailageTo']:array();
    $price = isset($_POST['price'])?$_POST['price']:array();
    $deductable = isset($_POST['deductable'])?$_POST['deductable']:array();
    $manufacturerWarranty = isset($_POST['manufacturerWarranty'])?$_POST['manufacturerWarranty']:array();
    if(count($price)==0 && count($price)==0 && count($mailageFrom)==0){
        header('Location: plan_list.php');
        exit;
    }
    $sqlClaim = "UPDATE plans SET name= '$planName' WHERE id = ".$PlanID;
    if ($conn->query($sqlClaim) === TRUE) {
        $insertID = $PlanID;
        if(count($term)){
            foreach($planTermsID as $index=>$data){
                $insertPlaneData[$index]['planTermsID'] = $data;
            }
            foreach($term as $index=>$data){
                $insertPlaneData[$index]['plansId'] = $insertID;
                $insertPlaneData[$index]['vendorId'] = $vendor_id;
                $insertPlaneData[$index]['TermSelect'] = $TermSelect;
                $insertPlaneData[$index]['termNo'] = $data;
            }
        }
        if(count($class)){
            foreach($class as $index=>$data){
                $insertPlaneData[$index]['plansId'] = $insertID;
                $insertPlaneData[$index]['vendorId'] = $vendor_id;
                $insertPlaneData[$index]['classId'] = $data;
            }
        }
        if(count($coverMailage)){
            foreach($coverMailage as $index=>$data){
                $insertPlaneData[$index]['plansId'] = $insertID;
                $insertPlaneData[$index]['vendorId'] = $vendor_id;
                $insertPlaneData[$index]['coverMailage'] = $data;
            }
        }
        if(count($upToMailage)){
            foreach($upToMailage as $index=>$data){
                $insertPlaneData[$index]['plansId'] = $insertID;
                $insertPlaneData[$index]['vendorId'] = $vendor_id;
                $insertPlaneData[$index]['upToMailage'] = $data;
            }
        }
        if(count($manufacturerWarranty)){
            foreach($manufacturerWarranty as $index=>$data){
                $insertPlaneData[$index]['plansId'] = $insertID;
                $insertPlaneData[$index]['vendorId'] = $vendor_id;
                $insertPlaneData[$index]['manufacturerWarranty'] = 'yes';
            }
        }
        if(count($effectiveDay)){
            foreach($effectiveDay as $index=>$data){
                $insertPlaneData[$index]['plansId'] = $insertID;
                $insertPlaneData[$index]['vendorId'] = $vendor_id;
                $insertPlaneData[$index]['effectiveDay'] = $data;
            }
        }
        if(count($mailageFrom)){
            foreach($mailageFrom as $index=>$data){
                $insertPlaneData[$index]['plansId'] = $insertID;
                $insertPlaneData[$index]['vendorId'] = $vendor_id;
                $insertPlaneData[$index]['milage_from'] = $data;
            }
        }
        if(count($mailageTo)){
            foreach($mailageTo as $index=>$data){
                $insertPlaneData[$index]['plansId'] = $insertID;
                $insertPlaneData[$index]['vendorId'] = $vendor_id;
                $insertPlaneData[$index]['milage_to'] = $data;
            }
        }
        if(count($price)){
            foreach($price as $index=>$data){
                $insertPlaneData[$index]['plansId'] = $insertID;
                $insertPlaneData[$index]['vendorId'] = $vendor_id;
                $insertPlaneData[$index]['price'] = $data;
                $insertPlaneData[$index]['price_unit'] = '$';
            }
        }
        if(count($deductable)){
            foreach($deductable as $index=>$data){
                $insertPlaneData[$index]['plansId'] = $insertID;
                $insertPlaneData[$index]['vendorId'] = $vendor_id;
                $insertPlaneData[$index]['deductable'] = $data;
            }
        }
    }
    if(count($insertPlaneData)) {
        $sql = "SELECT id FROM `plan_terms` WHERE `plansId`=$PlanID AND isDeleted=0";
        $result = mysqli_query($conn,$sql);
        $getOldPlanTermsIds = mysqli_fetch_all($result,MYSQLI_ASSOC);
        $setOldPlanTermsIds = array();
        if(count($getOldPlanTermsIds)) {
            foreach ($getOldPlanTermsIds as $key => $value) {
                $setOldPlanTermsIds[$value['id']] = true;
            }
        }
        foreach($insertPlaneData as $data){
            if(isset($data['planTermsID']) && !empty($data['planTermsID'])) {
                $setOldPlanTermsIds[$data['planTermsID']] = false;
                $sqlClaim = "UPDATE plan_terms SET 
                    plansId= '".$data['plansId']."', 
                    vendorId= '".$data['vendorId']."', 
                    TermSelect= '".$data['TermSelect']."', 
                    termNo= '".$data['termNo']."', 
                    classId= '".$data['classId']."', 
                    coverMailage= '".$data['coverMailage']."', 
                    upToMailage= '".$data['upToMailage']."', 
                    manufacturerWarranty= '".$data['manufacturerWarranty']."', 
                    effectiveDay= '".$data['effectiveDay']."', 
                    milage_from= '".$data['milage_from']."', 
                    milage_to= '".$data['milage_to']."', 
                    price= '".$data['price']."', 
                    price_unit= '".$data['price_unit']."', 
                    deductable= '".$data['deductable']."'
                    WHERE id = ".$data['planTermsID'];
                if ($conn->query($sqlClaim) === TRUE) echo "Update Successfully";
            } else {
                $insertID = insertDataGetID($conn, 'plan_terms', $data);
                if($insertID) echo "Success to Added";
            }
        }
        foreach($setOldPlanTermsIds as $planTermsId=>$value){
            if($value == true) {
                $sqlClaim = "UPDATE plan_terms SET isDeleted = 1 WHERE id = ".$planTermsId;
                if ($conn->query($sqlClaim) === TRUE)  echo "Delete plan Terms";
            }
        }
    } else {
        $sqlClaim = "UPDATE plan_terms SET isDeleted= 1 WHERE plansId = ".$PlanID;
        if ($conn->query($sqlClaim) === TRUE) echo "Deleted Plan";
    }
    
    header('Location: plan_list.php');
    exit;
}

function insertDataGetID($conn, $tableName, $arInsertData) {
    $insertID = 0;
    if(!empty($arInsertData)) {
        $arInsertData = removeSqlNullValue($arInsertData);
        $columns = implode(", ", array_keys($arInsertData));
        $escaped_values_array = makeMysqlEscapeValueByArray($arInsertData);
        $values  = implode(", ", $escaped_values_array);
        $sqlQuery = "INSERT INTO $tableName ($columns) VALUES ($values)";
        if ($conn->query($sqlQuery) === TRUE) $insertID = $conn->insert_id;
    }
    return $insertID;
}
function removeSqlNullValue($prArray) {
    $returnArray = array();
    foreach($prArray as $key=>$value) {
        if($value !== null) {
            $returnArray[$key] = $value;
        }
    }
    return $returnArray;
}
function makeMysqlEscapeValueByArray($prArray) {
    $returnArray = array();
    foreach($prArray as $value) {
        if(is_string($value)) $value = "'".addslashes($value)."'";
        $returnArray[] = $value;
    }
    return $returnArray;
}
echo json_encode(['status'=>'error']);