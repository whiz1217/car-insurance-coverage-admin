<?php
include_once('elements/db_connection.php');
$country_id = $_REQUEST['id'];
if($country_id != '') {
    ?>
    <select name="state_id" onclick="show_city(event)" required class="form-control">
    <option value="">Please select state or province</option>
    <?php 
    $sql = "SELECT * FROM states where `country_id` = $country_id";
    $result = mysqli_query($conn, $sql);
        // output data of each row
        while($row = mysqli_fetch_assoc($result)) {
    ?>
        <option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
    <?php       
        }
} else {
    ?>
    <select name="state_id" required class="form-control" disabled>
    <option value="">Please select state or province</option>
    </select>
    <?php
}
?>