<?php
include_once('elements/db_connection.php');
?>

		  <?php
		  if($_REQUEST['type']='to_apply'){
		  $sql = "SELECT * FROM master_car_make WHERE restrictedByDefault = 'no' ORDER BY name ASC";
		  $result = mysqli_query($conn, $sql);
		  // output data of each row
		  $arCarData = array(); 
		  $i = 0;
		  while($row = mysqli_fetch_assoc($result)) {
			$makeId = strtolower($row['name']);
			$arCarData[$makeId]['make'] = $row;
			if($i == 0) {
				$arMakeIds = '(\''.$makeId.'\'';
			} else {
				$arMakeIds = $arMakeIds.',\''.$makeId.'\'';
			}
			$i++;
		  } 
		  $arMakeIds = $arMakeIds.')';
		  //$sqlMake = "SELECT * FROM master_car_model WHERE model_make_id = '$make_id' ORDER BY model_name ASC";
		 $sqlModel = "SELECT * FROM master_car_model WHERE model_make_id IN $arMakeIds";
		  $resultModel = mysqli_query($conn, $sqlModel);
		  // output data of each row
		  while($rowModel = mysqli_fetch_assoc($resultModel)) {
			  $makeId = strtolower($rowModel['model_make_id']);
			  $arCarData[$makeId]['model'][] = $rowModel;
		  }
		  //echo "<pre>";
		  //print_r($arClassData);
		  //echo "</pre>";
		  foreach($arCarData as $mId=>$make) {
		  ?>
		  <li class="has model row">
			<span class="col-md-2 forSearch" id="make_<?php  echo $make['make']['id'];?>">
			  <input type="checkbox" class="model_checkbox" name="make[]" value="<?php echo $make['make']['id'];?>">
			  <label class="model_label"><?php echo ucwords($make['make']['name']);?></label>
			</span>
			<span class="col-md-10" style="padding-bottom: 20px;">
			  <input class="form-control" style="width:30%" placeholder="Year range (eg. 2009, 2010-2017)" name="make_range[<?php echo $make['make']['id'];?>]" type="text">
			</span>
			<br />
			<ul class="tree_new tree_make">
			<?php 
			  //$make_id = $row['make_id'];
			  //$sqlMake = "SELECT * FROM master_car_model WHERE model_make_id = '$make_id' ORDER BY model_name ASC";
			  //$resultMake = mysqli_query($conn, $sqlMake);
			  // output data of each row
			  foreach($make['model'] as $model) {
			  ?>
			  <li class="has make_li row">
				<span class="col-md-2 forSearch" id="model_<?php  echo $model['id'];?>">
				  <input type="checkbox" class="make_checkbox" name="model[]" value="<?php echo $model['id'];?>">
				  <label class="make_label"><?php echo ucwords($model['model_name']);?></label>
				</span>
				<span class="col-md-10" style="padding-bottom: 20px;">
				  <input class="form-control" style="width:30%" placeholder="Year range (eg. 2009, 2010-2017)" name="model_range[<?php echo $model['id'];?>]" type="text">
				</span>
				<ul class="make">
				  <?php 
				  $model_name = $model['model_name'];
				  //$make_id = $make['make']['id'];
				  $sqlTrim = "SELECT * FROM master_car_trim WHERE model_make_id = '$mId' AND model_name = '$model_name' ORDER BY model_trim ASC";
				  $resultTrim = mysqli_query($conn, $sqlTrim);
				  // output data of each row
				  while($rowTrim = mysqli_fetch_assoc($resultTrim)) {
				  ?>
				  <li class="row">
					<span class="col-md-6 forSearch" id="trim_<?php  echo $rowTrim['id'];?>">
					  <input type="checkbox" name="trim[]" value="<?php echo $rowTrim['id'];?>">
					  <label><?php echo ucwords($rowTrim['model_trim']);?></label>
					</span>
					<span class="col-md-4" style="padding-bottom: 20px;">
					  <input id="name" class="form-control" placeholder="Year range (eg. 2009, 2010-2017)" name="trim_range[<?php echo $rowTrim['id'];?>]" type="text">
					</span>
				  </li>
				  <?php } ?>
				</ul>
			  </li>
			  <?php } ?>
			</ul>
		  </li>
<?php } 
}
	?>
		
		<?php
		if($_REQUEST['type']='to_default'){ 
				//$sql = "SELECT * FROM master_car_make WHERE restrictedByDefault = 'yes' ORDER BY name ASC";
				//$result = mysqli_query($conn, $sql);
				// output data of each row
				$sql = "SELECT * FROM master_car_make WHERE restrictedByDefault = 'yes' ORDER BY name ASC";
				$result = mysqli_query($conn, $sql);
				// output data of each row
				$arCarData = array(); 
				$i = 0;
				while($row = mysqli_fetch_assoc($result)) {
					$makeId = strtolower($row['name']);
					$arCarData[$makeId]['make'] = $row;
					if($i == 0) {
						$arMakeIds = '(\''.$makeId.'\'';
					} else {
						$arMakeIds = $arMakeIds.',\''.$makeId.'\'';
					}
					$i++;
				} 
				$arMakeIds = $arMakeIds.')';
				//$sqlMake = "SELECT * FROM master_car_model WHERE model_make_id = '$make_id' ORDER BY model_name ASC";
				$sqlModel = "SELECT * FROM master_car_model WHERE model_make_id IN $arMakeIds";
				$resultModel = mysqli_query($conn, $sqlModel);
				// output data of each row
				while($rowModel = mysqli_fetch_assoc($resultModel)) {
					$makeId = strtolower($rowModel['model_make_id']);
					$arCarData[$makeId]['model'][] = $rowModel;
				}
			  
				//print_r($arClassData);
				//exit;
				foreach($arCarData as $mId=>$make) {
					  ?>
					  <li class="has model row">
						<span class="col-md-2 forSearch" id="make_<?php  echo $make['make']['id'];?>">
						  <input type="checkbox" class="model_checkbox" name="make[]" value="<?php echo $make['make']['id'];?>">
						  <label class="model_label"><?php echo ucwords($make['make']['name']);?></label>
						</span>
						<span class="col-md-10" style="padding-bottom: 20px;">
						  <input class="form-control" style="width:30%" placeholder="Year range (eg. 2009, 2010-2017)" name="make_range[<?php echo $make['make']['id'];?>]" type="text">
						</span>
						<br />
						<ul class="tree_new tree_make">
						<?php 
						  //$make_id = $row['make_id'];
						  //$sqlMake = "SELECT * FROM master_car_model WHERE model_make_id = '$make_id' ORDER BY model_name ASC";
						  //$resultMake = mysqli_query($conn, $sqlMake);
						  // output data of each row
						  foreach($make['model'] as $model) {
						  ?>
						  <li class="has make_li row">
							<span class="col-md-2 forSearch" id="model_<?php  echo $model['id'];?>">
							  <input type="checkbox" class="make_checkbox" name="model[]" value="<?php echo $model['id'];?>">
							  <label class="make_label"><?php echo ucwords($model['model_name']);?></label>
							</span>
							<span class="col-md-10" style="padding-bottom: 20px;">
							  <input class="form-control" style="width:30%" placeholder="Year range (eg. 2009, 2010-2017)" name="model_range[<?php echo $model['id'];?>]" type="text">
							</span>
							<ul class="make">
							  <?php 
							  $model_name = $model['model_name'];
							  //$make_id = $make['make']['id'];
							  $sqlTrim = "SELECT * FROM master_car_trim WHERE model_make_id = '$mId' AND model_name = '$model_name' ORDER BY model_trim ASC";
							  $resultTrim = mysqli_query($conn, $sqlTrim);
							  // output data of each row
							  while($rowTrim = mysqli_fetch_assoc($resultTrim)) {
							  ?>
							  <li class="row">
								<span class="col-md-6 forSearch" id="trim_<?php  echo $rowTrim['id'];?>">
								  <input type="checkbox" name="trim[]" value="<?php echo $rowTrim['id'];?>">
								  <label><?php echo ucwords($rowTrim['model_trim']);?></label>
								</span>
								<span class="col-md-4" style="padding-bottom: 20px;">
								  <input id="name" class="form-control" placeholder="Year range (eg. 2009, 2010-2017)" name="trim_range[<?php echo $rowTrim['id'];?>]" type="text">
								</span>
							  </li>
							  <?php } ?>
							</ul>
						  </li>
						  <?php } ?>
						</ul>
					  </li>
						  <?php } 
}
				?>
							