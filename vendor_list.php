<?php 
include_once('elements/db_connection.php');
include_once('elements/left_menu.php');
include_once('elements/header.php');
?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
              <div class="x_title">
                <h2>Vendors List</h2>
                <ul class="nav navbar-right panel_toolbox">
                  <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                  </li>
                </ul>
                <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <div class="item form-group pull-right">
                    <select onchange="show_vendor_by_category(event)" class="form-control" style="width: 177px;">
                        <option value="">All Category</option>
                        <option value="VSC">VSC</option>
                        <option value="GAP">GAP</option>
                        <option value="ANCILLARY">ANCILLARY</option>
                        <option value="INSURANCE">INSURANCE</option>
                    </select>
                </div> 
                <div class="clearfix"></div>
                <div class="item form-group pull-right">
                    <select onchange="show_vendor_by_approveDisapprove(event)" class="form-control" style="width: 177px;">
                        <option value="">All Vendors</option>
                        <option value="yes">Approve Vendors</option>
                        <option value="no">Disapprove Vendors</option>
                    </select>
                </div> 
                <div class="clearfix"></div>
                <div class="vendor_list_table">
                    <table id="vendor_list">
                    <thead>
                        <tr>
                            <th>Sl. no.</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class"vendor_list_body">
                        <?php 
                            $sql = "SELECT users.id, user_detail.company_name, user_detail.category ,users.isEnable FROM users LEFT JOIN user_detail ON users.id = user_detail.user_id WHERE users.user_role_id=2 AND users.isDeleted = 0  ORDER BY users.id DESC";
                            $result = mysqli_query($conn, $sql);
                            $slNo = 1;
                            if (mysqli_num_rows($result) > 0) {
                                // output data of each row
                                while($row = mysqli_fetch_assoc($result)) {
                            ?>
                                <tr>
                                    <td><?php echo $slNo;?></td>
                                    <td><?php echo $row['company_name'];?></td>
                                    <td><?php echo $row['category'];?></td>
                                    <td>
                                    <a class='btn btn-danger' onclick="delete_vendor(<?php echo $row['id'];?>)" style="cursor:pointer;">Delete</a>
                                    <a class='btn btn-primary' onclick="edit_detail(<?php echo $row['id'];?>)" style="cursor:pointer;">Edit</a>
                                    <a class='btn btn-primary' onclick="show_detail(<?php echo $row['id'];?>)" style="cursor:pointer;">View</a>
                                  <a class='btn btn-primary' onclick="approve_disapprove_vendor(<?php echo $row['id'];?>, '<?php echo $row['isEnable'];?>')" style="cursor:pointer;">
                                  <?php
                                  if($row['isEnable'] == 'yes') {
                                      echo ' Click to disapprove';
                                  } else {
                                    echo ' Click to approve';
                                  }
                                  ?>
                                  </a>
                                    </td>
                                </tr>
                            <?php
                            $slNo++;
                                }
                            } else {
                            $error_message = 'Wrong username or password';
                            }
                        ?>
                    </tbody>
                    </table>
                </div>
              </div>
            </div>
        </div>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                Loading...
            </div>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <?php 
        include_once('elements/footer.php');
        ?>
        <!-- /footer content -->
<script>
var table;
$(document).ready(function(){
    table = $('#vendor_list').DataTable();
});
function show_detail(user_id) {
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
    $.ajax({
        method: "POST",
        url: "vendor/vendor-detail.php",
        data: { user_id : user_id }
    }).done(function(data) {
        $('#myModal').html(data);
    });
}

function edit_detail(user_id) {
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
    $.ajax({
        method: "POST",
        url: "vendor/vendor-edit.php",
        data: { user_id : user_id }
    }).done(function(data) {
        $('#myModal').html(data);
    });
}
function delete_vendor(user_id) {
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
    $.ajax({
        method: "POST",
        url: "vendor/vendor-delete.php",
        data: { user_id : user_id }
    }).done(function(data) {
        $('#myModal').html(data);
    });
}
function close_popup() {
    $('#myModal').html('<div class="modal-content" >Loading...</div>');
    var modal = document.getElementById('myModal');
    modal.style.display = "none";
}

 function approve_disapprove_vendor(user_id, is_approve) {
    $.ajax({
        method: "POST",
        url: "vendor/vendor-approve.php",
        data: { user_id : user_id, is_approve : is_approve }
    }).done(function(data) {
        window.location.reload();
    });
}
function show_vendor_by_category(e) {
    var val = $(e.target).val();
    $.ajax({
        method: "POST",
        url: "vendor/vendor_list_body.php",
        data: { cat : val }
    }).done(function(data) {
        //console.log(data);
        table.clear();
        table.destroy();
        $('.vendor_list_table').html(data);
        table = $("#vendor_list").DataTable();
    });
}

function show_vendor_by_approveDisapprove(e) {
    var val = $(e.target).val();
    $.ajax({
        method: "POST",
        url: "vendor/vendor_list_body_of_ApproveDisapprove.php",
        data: { cat : val }
    }).done(function(data) {
        //console.log(data);
        table.clear();
        table.destroy();
        $('.vendor_list_table').html(data);
        table = $("#vendor_list").DataTable();
    });
}

function get_city(e) {
	var zip = $(e.target).val();
	$.ajax({
		method: "POST",
		url: "get_city_state_by_zip.php",
		data: { zip: zip }
	}).done(function(data) {
		if(data != '') {
			var obj = JSON.parse(data);
			$('.city').val(obj.CITY);
			$('.state').val(obj.STATE);
		}
	});
}
function compIsType(t, s) { 
    for(z = 0; z < t.length; ++z) 
        if(t[z] == s)
        return true;

    return false;
}
function generatePdf() {
    var val = $(".vendor_detail").html();
    $.ajax({
        method: "POST",
        url: "vendor/generate_pdf.php",
        data: { html : val }
    }).done(function(data) {
        //console.log(data);
    });
}
$(document).on('click', '.engine_type', function(e) {
    $('.toggle_engine_type').toggle();
});
$(document).on('click', '.additional_restriction', function(e) {
    $('.toggle_additional_restriction').toggle();
});
$(document).on('click', '.additional_surcharge', function(e) {
    $('.toggle_additional_surcharge').toggle();
});
$(document).on('click', '.allowed_vehicals', function(e) {
    $('.toggle_allowed_vehicals').toggle();
});
$(document).on('click', '.restrict_class', function(e) {
    $('.toggle_restrict_class').toggle();
});
function fnShowHidePlan(prtoggleClass) {
    $('.' + prtoggleClass).toggle();
}
function toggleEngine(name) {
	$('.' + name).toggle();
};
</script>