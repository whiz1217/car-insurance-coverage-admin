<?php
include_once('elements/db_connection.php');
$id = $_REQUEST['id'];
$sqlM = "SELECT appletoappledata.vendor_id, appletoappledata.plan_id, user_detail.category FROM appletoappledata LEFT JOIN user_detail ON user_detail.user_id = appletoappledata.vendor_id WHERE appletoappledata.id = $id";
$resultM = mysqli_query($conn, $sqlM);
while($rowM = mysqli_fetch_assoc($resultM)) {
$cat = $rowM['category'];
?>
        <div class="x_panel">
        <span class="close" onclick="close_popup()">&times;</span>
        <div class="clearfix"></div>
              <div class="x_content">
              <form class="form-horizontal form-label-left" action="edit_A2A.php" method="post">
                <input type="hidden" name="appletoappledata_id" value="<?php echo $id;?>">
                <div class="item form-group">
                  <label class="col-md-2">Category <span class="required">*</span></label>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                  <select onchange="show_vendor_by_category(event)" class="form-control cat_id">
                      <option value="">PLEASE SELECT A CATEGORY</option>
                      <option value="VSC" <?php if($cat == 'VSC'){ echo "selected";}?>>VSC</option>
                      <option value="GAP" <?php if($cat == 'GAP'){ echo "selected";}?>>GAP</option>
                      <option value="ANCILLARY" <?php if($cat == 'ANCILLARY'){ echo "selected";}?>>ANCILLARY</option>
                      <option value="INSURANCE" <?php if($cat == 'INSURANCE'){ echo "selected";}?>>INSURANCE</option>
                  </select>
                  </div>
                </div>
                <div class="item form-group">
                    <label class="col-md-2">Choose Vendor *</label>
                    <div class="col-md-4 col-sm-6 col-xs-12 vendor_list_filter">
                         <?php 
                              $sql = "SELECT users.id, user_detail.company_name FROM users LEFT JOIN user_detail ON users.id = user_detail.user_id WHERE users.user_role_id='2' AND  user_detail.category = '$cat' ORDER BY users.id DESC";
                              $result = mysqli_query($conn, $sql);
                            ?>
                      <select onchange="show_plan(event)" name="vendor_id" class="form-control" required>
                        <option value="">PLEASE SELECT A VENDOR</option>
                        <?php 
                              while($row = mysqli_fetch_assoc($result)) {
                            ?>
                                  <option value="<?php echo $row['id'];?>" <?php if($rowM['vendor_id'] == $row['id']) echo 'selected'; ?>><?php echo $row['company_name'];?></option>
                            <?php       
                                  }
                            ?>
                      </select>
                    </div>
				</div>
                <div class="item form-group">
                    <label class="col-md-2">Choose Plan *</label>
                    <div class="col-md-4 col-sm-6 col-xs-12 plan_d">
                      <select name="plan_id" class="form-control" required>
                      <option value="">PLEASE SELECT A PLAN</option>
                      <?php 
                      $vendor_id = $rowM['vendor_id'];
                      $sql = "SELECT plans.id FROM plan_terms LEFT JOIN plans ON plan_terms.plansId = plans.id where plan_terms.vendorId = $vendor_id GROUP BY plan_terms.plansId";
                      $result = mysqli_query($conn, $sql);
                          // output data of each row
                      while($row = mysqli_fetch_assoc($result)) {
                          $p_id = $row['id'];
                          $sqlP = "SELECT * FROM plans where id = $p_id";
                          $resultP = mysqli_query($conn, $sqlP);
                              // output data of each row
                          while($rowP = mysqli_fetch_assoc($resultP)) {
                      ?>
                          <option value="<?php echo $row['id'];?>" <?php if($rowM['plan_id'] == $row['id']) echo 'selected'; ?>><?php echo $rowP['name'];?></option>
                      <?php   
                          }
                        }    
                      ?>
                      </select>
                    </div>
				</div>
				<div class="x_title">
					<h2>Apples To Apples</h2>
					<div class="clearfix"></div>
				</div>
				<ul class="tree_new">
                  <?php 
                    $sql = "SELECT * FROM appletoappletype";
                    $result = mysqli_query($conn, $sql);
                        // output data of each row
                        while($row = mysqli_fetch_assoc($result)) {
                          $sqlE = "SELECT * FROM a2a_type_data WHERE appletoappledata_id = $id";
                          $resultE = mysqli_query($conn, $sqlE);
                          // output data of each row
                          $typeSelected = false;
                          while($rowE = mysqli_fetch_assoc($resultE)) {
                            if($rowE['a2a_type_id'] == $row['id']) {
                              $typeSelected = true;
                            }
                          }
                  ?>
                    <li class="has">
						<input type="checkbox" name="A2AType[]" value="<?php echo $row['id'];?>" <?php if($typeSelected) echo 'checked';?>>
						<label>
							<?php
							if(file_exists('images/a2a/' . $row['name'] . '.png')) {
							?>
								<img src="<?php echo 'images/a2a/' . $row['name'] . '.png';?>">
							<?php
							} else {
								echo $row['name'];
							}
							?>
						</label>
						<ul>
							<?php 
							$appleToAppleType = $row['id'];
							$sqlType = "SELECT * FROM appletoapplevalue WHERE appleToAppleType= $appleToAppleType";
							$resultType = mysqli_query($conn, $sqlType);
							// output data of each row
							while($rowType = mysqli_fetch_assoc($resultType)) {
							  $sqlE = "SELECT * FROM a2a_value_data WHERE appletoappledata_id = $id";
							  $resultE = mysqli_query($conn, $sqlE);
							  // output data of each row
							  $valSelected = false;
							  $valComment = '';
							  while($rowE = mysqli_fetch_assoc($resultE)) {
								if($rowE['a2a_value_id'] == $rowType['id']) {
								  $valSelected = true;
								  $valComment = $rowE['value_comment'];
								}
							  }
							?>
							<li class="row">
								<span class="col-md-7">
									<input type="checkbox" name="A2ATypeValue[<?php echo $row['id'];?>][]" value="<?php echo $rowType['id'];?>" <?php if($valSelected) echo 'checked';?>>
									<label><?php echo $rowType['name'];?></label>
								</span>
								<input class="col-md-3" name="A2ATypeComment[<?php echo $rowType['id'];?>]" placeholder="Type comments" value="<?php echo $valComment;?>" type="text">
							</li>
							<?php } ?>
						</ul> 
                    </li>
					<?php } ?>
				</ul>  
				<?php 
				$sqlH = "SELECT * FROM highlight_coverage WHERE appletoappledata_id = $id";
				$resultH = mysqli_query($conn, $sqlH);
				// output data of each row
				if (mysqli_num_rows($resultH) > 0) {
					while($rowH = mysqli_fetch_assoc($resultH)) {
				?>
                  <div class="clearfix"></div>
                  <h2>Highlight Coverage</h2>
                  <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="item form-group">
                      <label class="col-md-2">Labour Ready</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                          Limited <input type="radio" name="labourReady" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('labourReady')" <?php if($rowH['labourReady'] == 'limit') { echo 'checked'; } ?>>
                          Unlimited <input type="radio" name="labourReady" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('labourReady')" <?php if($rowH['labourReady'] == 'noLimit') { echo 'checked'; } ?>> 
                          <?php if($rowH['labourReady'] == 'limit') { ?>
                          <input type="text" name="labourReadyCharge" class="labourReadyCharge form-control" autofocus value="<?php echo $rowH['labourReadyCharge']?>">
                          <?php } else { ?>
                            <input type="text" style="display: none" name="labourReadyCharge" class="labourReadyCharge form-control" autofocus>
                          <?php } ?>
                      </div>
                    </div>
					<div class="item form-group">
                      <label class="col-md-2">Wheels</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                          Limited <input type="radio" name="wheels" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('wheels')" <?php if($rowH['wheels'] == 'limit') { echo 'checked'; } ?>>
                          Unlimited <input type="radio" name="wheels" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('wheels')" <?php if($rowH['wheels'] == 'noLimit') { echo 'checked'; } ?>>
						  <?php if($rowH['wheels'] == 'limit') { ?>
							<input type="text" name="wheelsCharge" class="wheelsCharge form-control" autofocus value="<?php echo $rowH['wheelsCharge']?>">
                          <?php } else { ?>
                            <input type="text" style="display: none" name="wheelsCharge" class="wheelsCharge form-control" autofocus>
                          <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Tiers</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="tiers" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('tiers')" <?php if($rowH['tiers'] == 'limit') { echo 'checked'; } ?>>
                      Unlimited <input type="radio" name="tiers" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('tiers')" <?php if($rowH['tiers'] == 'noLimit') { echo 'checked'; } ?>> 
						  <?php if($rowH['tiers'] == 'limit') { ?>
							<input type="text" name="tiersCharge" class="tiersCharge form-control" autofocus value="<?php echo $rowH['tiersCharge']?>">
                          <?php } else { ?>
                            <input type="text" style="display: none" name="tiersCharge" class="tiersCharge form-control" autofocus>
                          <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Road Hazard</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="roadHazard" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('roadHazard')" <?php if($rowH['roadHazard'] == 'limit') { echo 'checked'; } ?>>
                      Unlimited <input type="radio" name="roadHazard" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('roadHazard')" <?php if($rowH['roadHazard'] == 'noLimit') { echo 'checked'; } ?>> 
						  <?php if($rowH['roadHazard'] == 'limit') { ?>
							<input type="text" name="roadHazardCharge" class="roadHazardCharge form-control" autofocus value="<?php echo $rowH['roadHazardCharge']?>">
                          <?php } else { ?>
                            <input type="text" style="display: none" name="roadHazardCharge" class="roadHazardCharge form-control" autofocus>
                          <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Cosmetic Wheel Repair</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="cosmeticWheelRepair" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('cosmeticWheelRepair')" <?php if($rowH['cosmeticWheelRepair'] == 'limit') { echo 'checked'; } ?>>
                      Unlimited <input type="radio" name="cosmeticWheelRepair" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('cosmeticWheelRepair')" <?php if($rowH['cosmeticWheelRepair'] == 'noLimit') { echo 'checked'; } ?>> 
						  <?php if($rowH['cosmeticWheelRepair'] == 'limit') { ?>
							<input type="text" name="cosmeticWheelRepairCharge" class="cosmeticWheelRepairCharge form-control" autofocus value="<?php echo $rowH['cosmeticWheelRepairCharge']?>">
                          <?php } else { ?>
                            <input type="text" style="display: none" name="cosmeticWheelRepairCharge" class="cosmeticWheelRepairCharge form-control" autofocus>
                          <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Curb Impact Repair & Replacement</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="curbImpactRepair" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('curbImpactRepair')" <?php if($rowH['curbImpactRepair'] == 'limit') { echo 'checked'; } ?>>
                      Unlimited <input type="radio" name="curbImpactRepair" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('curbImpactRepair')" <?php if($rowH['curbImpactRepair'] == 'noLimit') { echo 'checked'; } ?>> 
					      <?php if($rowH['curbImpactRepair'] == 'limit') { ?>
							<input type="text" name="curbImpactRepairCharge" class="curbImpactRepairCharge form-control" autofocus value="<?php echo $rowH['curbImpactRepairCharge']?>">
                          <?php } else { ?>
                            <input type="text" style="display: none" name="curbImpactRepairCharge" class="curbImpactRepairCharge form-control" autofocus>
                          <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Paintless Dent Repair & Ding</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="paintlessDentRepair" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('paintlessDentRepair')" <?php if($rowH['paintlessDentRepair'] == 'limit') { echo 'checked'; } ?>>
                      Unlimited <input type="radio" name="paintlessDentRepair" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('paintlessDentRepair')" <?php if($rowH['paintlessDentRepair'] == 'noLimit') { echo 'checked'; } ?>> 
					      <?php if($rowH['paintlessDentRepair'] == 'limit') { ?>
							<input type="text" name="paintlessDentRepairCharge" class="paintlessDentRepairCharge form-control" autofocus value="<?php echo $rowH['paintlessDentRepairCharge']?>">
                          <?php } else { ?>
                            <input type="text" style="display: none" name="paintlessDentRepairCharge" class="paintlessDentRepairCharge form-control" autofocus>
                          <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Rental</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="rental" onclick="fnShowHideInput(event, 'rentalCharge')" style="cursor: pointer;" <?php if($rowH['rental'] == 'yes') { echo 'checked'; } ?>>
                        <?php if($rowH['rental'] == 'yes') { ?>
                          <input type="text" name="rentalCharge" class="rentalCharge form-control" value="<?php echo $rowH['rentalCharge']?>" autofocus>
                        <?php } else { ?>
                          <input type="text" style="display: none" name="rentalCharge" class="rentalCharge form-control" autofocus>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Road Side Assistance</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="roadSideAssitance" onclick="fnShowHideInput(event, 'roadSideAssitanceCharge')" style="cursor: pointer;" <?php if($rowH['roadSideAssitance'] == 'yes') { echo 'checked'; } ?>>
                        <?php if($rowH['roadSideAssitance'] == 'yes') { ?>
                          <input type="text" name="roadSideAssitanceCharge" class="roadSideAssitanceCharge form-control" value="<?php echo $rowH['roadSideAssitanceCharge']?>" autofocus>
                        <?php } else { ?>
                          <input type="text" style="display: none" name="roadSideAssitanceCharge" class="roadSideAssitanceCharge form-control" autofocus>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Toing</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="toing" onclick="fnShowHideInput(event, 'toingCharge')" style="cursor: pointer;" <?php if($rowH['toing'] == 'yes') { echo 'checked'; } ?>>
                        <?php if($rowH['toing'] == 'yes') { ?>
                          <input type="text" name="toingCharge" class="toingCharge form-control" value="<?php echo $rowH['toingCharge']?>" autofocus>
                        <?php } else { ?>
                          <input type="text" style="display: none" name="toingCharge" class="toingCharge form-control" autofocus>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Other Highlights</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="otherHighlight" onclick="fnShowHideInput(event, 'otherHighlightCharge')" style="cursor: pointer;" <?php if($rowH['otherHighlight'] == 'yes') { echo 'checked'; } ?>>
                        <?php if($rowH['otherHighlight'] == 'yes') { ?>
                          <input type="text" name="otherHighlightCharge" class="otherHighlightCharge form-control" value="<?php echo $rowH['otherHighlightCharge']?>" autofocus>
                        <?php } else { ?>
                          <input type="text" style="display: none" name="otherHighlightCharge" class="otherHighlightCharge form-control" autofocus>
                        <?php } ?>
                      </div>
                    </div>
					<div class="item form-group">
                      <label class="col-md-2">Tier & Wheel</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="tierWheel" onclick="fnShowHideInput(event, 'tierWheelCharge')" style="cursor: pointer;" <?php if($rowH['tierWheel'] == 'yes') { echo 'checked'; } ?>>
						<?php if($rowH['tierWheel'] == 'yes') { ?>
                          <input type="text" name="tierWheelCharge" class="tierWheelCharge form-control" value="<?php echo $rowH['tierWheelCharge']?>" autofocus>
                        <?php } else { ?>
                          <input type="text" style="display: none" name="tierWheelCharge" class="tierWheelCharge form-control" autofocus>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Windshield Chip</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="windshildChip" onclick="fnShowHideInput(event, 'windshildChipCharge')" style="cursor: pointer;" <?php if($rowH['windshildChip'] == 'yes') { echo 'checked'; } ?>>
						<?php if($rowH['windshildChip'] == 'yes') { ?>
                          <input type="text" name="windshildChipCharge" class="windshildChipCharge form-control" value="<?php echo $rowH['windshildChipCharge']?>" autofocus>
                        <?php } else { ?>
                          <input type="text" style="display: none" name="windshildChipCharge" class="windshildChipCharge form-control" autofocus>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Windshield Protection</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="windshildProtection" onclick="fnShowHideInput(event, 'windshildProtectionCharge')" style="cursor: pointer;" <?php if($rowH['windshildProtection'] == 'yes') { echo 'checked'; } ?>>
                        <?php if($rowH['windshildProtection'] == 'yes') { ?>
                          <input type="text" name="windshildProtectionCharge" class="windshildProtectionCharge form-control" value="<?php echo $rowH['windshildProtectionCharge']?>" autofocus>
                        <?php } else { ?>
                          <input type="text" style="display: none" name="windshildProtectionCharge" class="windshildProtectionCharge form-control" autofocus>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Key Replacement</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="keyReplacement" onclick="fnShowHideInput(event, 'keyReplacementCharge')" style="cursor: pointer;" <?php if($rowH['keyReplacement'] == 'yes') { echo 'checked'; } ?>>
						<?php if($rowH['keyReplacement'] == 'yes') { ?>
                          <input type="text" name="keyReplacementCharge" class="keyReplacementCharge form-control" value="<?php echo $rowH['keyReplacementCharge']?>" autofocus>
                        <?php } else { ?>
                          <input type="text" style="display: none" name="keyReplacementCharge" class="keyReplacementCharge form-control" autofocus>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Lease End Protection</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="leaseEndProtection" onclick="fnShowHideInput(event, 'leaseEndProtectionCharge')" style="cursor: pointer;" <?php if($rowH['leaseEndProtection'] == 'yes') { echo 'checked'; } ?>>
						<?php if($rowH['leaseEndProtection'] == 'yes') { ?>
                          <input type="text" name="leaseEndProtectionCharge" class="leaseEndProtectionCharge form-control" value="<?php echo $rowH['leaseEndProtectionCharge']?>" autofocus>
                        <?php } else { ?>
                          <input type="text" style="display: none" name="leaseEndProtectionCharge" class="leaseEndProtectionCharge form-control" autofocus>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Effective Day</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      <input type="text" name="effectiveDay" class="effectiveDay form-control" value="<?php echo $rowH['effectiveDay'];?>">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Store Type</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <select name="storeType" style="cursor: pointer;">
                          <option value="">--select--</option>
                          <option value="independent" <?php if($rowH['storeType'] == 'independent') { echo 'selected'; } ?>>Independent</option>
                          <option value="franchise" <?php if($rowH['storeType'] == 'franchise') { echo 'selected'; } ?>>Franchise</option>
                          <option value="both" <?php if($rowH['storeType'] == 'both') { echo 'selected'; } ?>>Both</option>
                        </select>
                      </div>
                    </div>
                  </div>   
                  <?php } } else {?>   
                  <div class="clearfix"></div>
                  <h2>Highlight Coverage</h2>
                  <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="item form-group">
                      <label class="col-md-2">Labour Ready</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="labourReady" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('labourReady')">
                      Unlimited <input type="radio" name="labourReady" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('labourReady')"> 
                          <input type="text" style="display: none" name="labourReadyCharge" class="labourReadyCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Wheels</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                          Limited <input type="radio" name="wheels" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('wheels')">
                          Unlimited <input type="radio" name="wheels" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('wheels')"> 
                          <input type="text" style="display: none" name="wheelsCharge" class="wheelsCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Tiers</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="tiers" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('tiers')">
                      Unlimited <input type="radio" name="tiers" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('tiers')"> 
                          <input type="text" style="display: none" name="tiersCharge" class="tiersCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Road Hazard</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="roadHazard" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('roadHazard')">
                      Unlimited <input type="radio" name="roadHazard" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('roadHazard')"> 
                          <input type="text" style="display: none" name="roadHazardCharge" class="roadHazardCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Cosmetic Wheel Repair</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name=">cosmeticWheelRepair" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('cosmeticWheelRepair')">
                      Unlimited <input type="radio" name="cosmeticWheelRepair" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('cosmeticWheelRepair')"> 
                          <input type="text" style="display: none" name="cosmeticWheelRepairCharge" class="cosmeticWheelRepairCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Curb Impact Repair & Replacement</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="curbImpactRepair" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('curbImpactRepair')">
                      Unlimited <input type="radio" name="curbImpactRepair" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('curbImpactRepair')"> 
                          <input type="text" style="display: none" name="curbImpactRepairCharge" class="curbImpactRepairCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Paintless Dent Repair & Ding</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      Limited <input type="radio" name="paintlessDentRepair" value="limit" style="cursor: pointer;" onclick="fnShowLimitBox('paintlessDentRepair')">
                      Unlimited <input type="radio" name="paintlessDentRepair" value="noLimit" style="cursor: pointer;" onclick="fnHideLimitBox('paintlessDentRepair')"> 
                          <input type="text" style="display: none" name="paintlessDentRepairCharge" class="paintlessDentRepairCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Rental</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="rental" onclick="fnShowHideInput(event, 'rentalCharge')" style="cursor: pointer;">
                        <input type="text" style="display: none" name="rentalCharge" class="rentalCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Road Side Assistance</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="roadSideAssitance" onclick="fnShowHideInput(event, 'roadSideAssitanceCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="roadSideAssitanceCharge" class="roadSideAssitanceCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Toing</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="toing" onclick="fnShowHideInput(event, 'toingCharge')" style="cursor: pointer;">
                        <input type="text" style="display: none" name="toingCharge" class="toingCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Other Highlights</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="otherHighlight" onclick="fnShowHideInput(event, 'otherHighlightCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="otherHighlightCharge" class="otherHighlightCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Tier & Wheel</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="tierWheel" onclick="fnShowHideInput(event, 'tierWheelCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="tierWheelCharge" class="tierWheelCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Windshild Chip</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="windshildChip" onclick="fnShowHideInput(event, 'windshildChipCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="windshildChipCharge" class="windshildChipCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Windshild Protection</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="windshildProtection" onclick="fnShowHideInput(event, 'windshildProtectionCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="windshildProtectionCharge" class="windshildProtectionCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Key Replacement</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="keyReplacement" onclick="fnShowHideInput(event, 'keyReplacementCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="keyReplacementCharge" class="keyReplacementCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Lease End Protection</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <input type="checkbox" name="leaseEndProtection" onclick="fnShowHideInput(event, 'leaseEndProtectionCharge')" style="cursor: pointer;">
                          <input type="text" style="display: none" name="leaseEndProtectionCharge" class="leaseEndProtectionCharge form-control" autofocus>
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Effective Day</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                      <input type="text" name="effectiveDay" class="effectiveDay form-control">
                      </div>
                    </div>
                    <div class="item form-group">
                      <label class="col-md-2">Store Type</label>
                      <div class="col-md-4 col-sm-6 col-xs-12">
                        <select name="storeType" style="cursor: pointer;">
                          <option value="">--select--</option>
                          <option value="independent">Independent</option>
                          <option value="franchise">Franchise</option>
                          <option value="both">Both</option>
                        </select>
                      </div>
                    </div>
                  </div>      
                  <?php } ?>   
                  <div class="clearfix"></div>
                  <div class="ln_solid"></div>
                  <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                      <a class="btn btn-danger" href="add_apple_to_apple_list.php">Cancel</a>
                      <button id="send" type="submit" class="btn btn-success">Save</button>
                    </div>
                  </div>               
                </form>
              </div>
            </div>
<?php } ?>